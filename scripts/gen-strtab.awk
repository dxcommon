#!/bin/awk -f
#
# Generate a C string table based on an input string specification file.
#
# Copyright © 2021, 2023-2024 Nick Bowler
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# A string table is a single large char single array containing all of
# the specified (0-terminated) strings, which is then offset to obtain
# the desired string.  By storing these offsets instead of string pointers
# into read-only data structures, this can reduce the need for relocation
# processing at startup when programs are built in PIC mode.
#
# The string specification file is processed line by line.  Comment
# lines may be included by beginning the line with a # character, which
# must be the very first character on the line.  If a comment is encountered,
# processing immediately moves on to the next line and the result is as if
# the comment line were omitted from the input.
#
# Options may be used to alter the normal behaviour.  An option is placed
# on a line by itself beginning with an @ character, and may appear anywhere
# in the input file.  The following options are defined:
#
#   @nozero
#     All strings will have a non-zero offset in the strtab.
#
#   @macro
#     Instead of a variable declaration, the generated header will define an
#     object-like macro that can be used as the initializer for a char array.
#
# A string is defined by beginning a line with one or two & characters, which
# must be immediately followed by a C identifier.  Two & characters indicates
# a string that should not be translated, as described below.  A nonempty
# sequence of whitespace (with at most one newline) separates the identifier
# from the beginning of the string itself.  This whitespace is never included
# in the output.
#
# The string is then interpreted as follows:
#
#   - Leading blanks on each line are ignored.
#   - The sequences \\, \a, \b, \t, \n, \v, \f and \r can be entered and
#     mean the same as they do in C string literals.  The "\\" sequence
#     prevents any special interpretation of the second backslash.
#   - Octal and hexadecimal escape sequences like \33 or \x1e are also
#     supported.
#   - Newlines in the input are included in the output, except when the
#     entire string (including its identifier) is on one line.
#   - If this is not desired, a newline which is immediately preceded by an
#     unescaped backslash will deleted, along with the backslash.
#   - All other backslashes are deleted.  This can be used to prevent special
#     handling of whitespace, # or & characters at the beginning of a line.
#
# Unless the @macro option is specified, the output defines a variable,
# strtab, which contains all of the strings, and each identifier in the input
# is declared as an emumeration constant whose value is the offset of the
# associated string within strtab.  Otherwise, if the @macro option is
# specified, no variables are defined and STRTAB_INITIALIZER object-like macro
# may be used to initialize a char array with static storage duration.
#
# Normally, the generated source code wraps strings using the identity macro
# N_(x), which has no effect on the resulting data structures but enables tools
# such as xgettext to extract translatable strings from the source code.  An
# identifier preceded by two ampersands (&&) suppresses this output to allow
# a single string table to also contain both translateable strings as well as
# ones that should not be translated.
#
# The object-like macro STRTAB_MAX_OFFSET is defined and expands to the
# greatest string offset, suitable for use in #if preprocessing directives.

END {
  print "/*"
  if (FILENAME) {
    print " * Automatically generated by gen-strtab.awk from " FILENAME
  } else {
    print " * Automatically generated by gen-strtab.awk"
  }
  print " * Do not edit."
  print " */"
}

BEGIN {
  # Check if "\\\\" in substitutions gives just one backslash.
  bs = "x"; sub(/x/, "\\\\", bs);
  bs = (length(bs) == 1 ? "\\\\" : "\\");

  opts["zero"] = 1
  opts["macro"] = 0
  collected = ident = ""
  startline = endline = 0
  num_vars = 0
}

# Comments
NF == 0 || $0 ~ /^[#]/ { next }

# Options
sub(/^@/, "", $0) {
  if (NF == 1) {
    orig=$1
    gsub(/-/, "_", $1);
    val = !sub(/^no_?/, "", $1);
    if ($1 in opts) {
      opts[$1] = val;
    } else {
      print "error: unrecognized option: @" orig | "cat 1>&2"
      exit 1
    }
  }
  next
}

sub(/^[&]/, "") {
  if (ident != "") {
    finish_string_input(strings, ident, collected);
    vars[num_vars++] = ident;
  }

  current_l10n = !sub(/^[&]/, "");
  startline = NR;
  ident = $1;

  collected = "";
  sub(/^[^ \t]*/, "");
}

ident != "" {
  sub(/^[ \t]*/, "");

  sep = collected != "" ? "\n" : "";
  collected = collected sep $0;
  endline = NR;
}

END {
  if (ident != "") {
    finish_string_input(strings, ident, collected)
    vars[num_vars++] = ident
  }
}

END {
  strtab = "";
  count = bucketsort(sorted_strings, strings);
  for (i = 0; i < count; i++) {
    gsub(/\\\\/, "\2", sorted_strings[i]);
    strtab = add_to_strtab(strtab, sorted_strings[i], offsets);
  }

  if (!opts["zero"]) {
    strtab = "\1" strtab;
  }

  # Format string table into C syntax
  tmp_strtab = "";
  while (match(strtab, "\1") > 0) {
    nul = (substr(strtab, RSTART+1, 1) ~ /[0-7]/ ? "\\000" : "\\0");
    tmp_strtab = tmp_strtab substr(strtab, 1, RSTART-1) nul "\\\n";
    strtab = substr(strtab, RSTART+1);
  }
  strtab = tmp_strtab strtab;
  gsub("\2", bs bs, strtab);

  if (opts["macro"]) {
    print "\n#define STRTAB_INITIALIZER \"\\\n" strtab "\"";
  } else {
    print "\nstatic const char strtab[] = \"\\\n" strtab "\";";
  }

  prefix = "\n#if 0\n";
  for (i in l10n) {
    print prefix "\tgettext(\"" l10n[i] "\");";
    prefix = "";
  }
  if (prefix == "") print "#endif";

  max = 0;
  sep="\t";
  print "\nenum {";
  for (i in strings) {
    gsub(/\\\\/, "\2", strings[i]);
    count = offsets[strings[i]] + (!opts["zero"]);
    if (count > max)
      max = count;

    printf "%s%s = %s", sep, i, count;
    sep = ",\n\t";
  }
  print "\n};"

  print "\n#define STRTAB_MAX_OFFSET " max
}

# finish_string_input(strings, ident, val)
#
# Deal with backslash-escapes and special characters in val, then set
# strings[ident] = val.
function finish_string_input(strings, ident, val, tmpval, s)
{
  gsub(/\\\\/, "\2", val);
  if (endline > startline)
    val = val "\n";
  gsub("\\\\\n", "", val);

  # Convert all numeric escapes to 3-digit octal escapes
  tmpval = "";
  while (match(val, /\\[0-7][0-7]?[0-7]?|\\x[0-9abcdefABCDEF]+/) > 0) {
    if (substr(val, RSTART+1, 1) == "x") {
      s = hex_to_octal(substr(val, RSTART+2, RLENGTH-2));
    } else {
      s = substr("00" substr(val, RSTART+1, RLENGTH-1), RLENGTH-1);
    }
    tmpval = tmpval substr(val, 1, RSTART-1) "\\" s;
    val = substr(val, RSTART+RLENGTH);
  }
  val = tmpval val;

  # Delete remaining backslashes
  tmpval = ""
  while (match(val, /\\[^abtnvfr0-7]/) > 0) {
    tmpval = tmpval substr(val, 1, RSTART-1);
    val = substr(val, RSTART+1);
  }
  tmpval = tmpval val;

  # Escape special characters
  gsub(/"/,  bs"\"", tmpval);
  gsub(/\t/, bs"t", tmpval);
  gsub("\n", bs"n", tmpval);
  gsub("\2", bs bs, tmpval);

  strings[ident] = tmpval;
  if (current_l10n) {
    l10n[ident] = tmpval;
  }
}

# bucketsort(dst, src)
#
# Sort the elements of src by descending string length,
# placing them into dst[0] ... dst[n].
#
# Returns the number of elements.
function bucketsort(dst, src, max, count, i, t)
{
  # Note: ULTRIX 4.5 nawk does not support local array parameters
  split("", bucketsort_buckets);

  for (t in src) {
    i = length(src[t]);
    if (i > max) { max = i; }
    bucketsort_buckets[i]++;
  }

  for (i = max; i >= 0; i--) {
    if (i in bucketsort_buckets) {
      t = bucketsort_buckets[i];
      bucketsort_buckets[i] = count;
      count += t;
    }
  }

  for (t in src) {
    i = length(t = src[t]);
    dst[bucketsort_buckets[i]++] = t;
  }

  return count;
}

function real_length(s, t)
{
  t = length(s);
  t -= 2*gsub(/\\[0-7]/, "&", s)
  return t - gsub(/\\./, "&", s);
}

# add_to_strtab(strtab, str, offsets)
#
# Append string to strtab if there is not already a matching string present
# in the table.  Newly-added strings are separated by "\1", which must be
# translated into null bytes afterwards.  The updated strtab is returned, and
# the offsets[str] array member is updated with the position (counting from 0)
# of str in the strtab.
#
# For optimal results, strings should be added in descending length order.
function add_to_strtab(strtab, str, offsets, pos)
{
  if ( (pos = index(strtab "\1", str"\1") - 1) < 0) {
    pos = length(strtab);
    if (pos) {
      strtab = strtab "\1" str;
      pos++;
    } else {
      strtab = strtab str;
    }
  }

  offsets[str] = real_length(substr(strtab, 1, pos));
  return strtab
}

function hex_to_octal(s, n, i)
{
  for (n = 0; length(s) > 0; s = substr(s, 2)) {
    n = n * 16;
    if ((i = index("0123456789abcdef", substr(s, 1, 1))) > 0) {
      n += i-1;
    } else if ((i = index("0123456789ABCDEF", substr(s, 1, 1))) > 0) {
      n += i-1;
    }
  }

  return sprintf("%.3o", n % 512);
}
