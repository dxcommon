#!/usr/bin/env perl
#
# Prepare the Gnulib tree for inclusion into a non-recursive automake build.
#
# Copyright © 2011-2014, 2020-2024 Nick Bowler
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# While the output of gnulib-tool is "include"-able if the --makefile-name
# option is used, it is still not suitable for non-recursive use for a couple
# reasons; chief among them is that filenames are not relative to the top
# source directory.
#
# This script postprocesses the gnulib-tool output to produce something
# that is intended to be suitable for inclusion into such non-recursive
# build environments.  Since the integration involves both configure.ac
# and Makefile.am, the output must be included into _both_.  Supposing
# the output is written to lib/gnulib.mk, you would add:
#
#   m4_include([lib/gnulib.mk]) # to configure.ac, after any call to gl_INIT
#   include $(top_srcdir)/lib/gnulib.mk # to Makefile.am
#
# You must also arrange for the Gnulib-generated header files to be built
# before the object files which depend on them; the most robust way to do
# this is by explicit prerequisites, for example:
#
#   bin_PROGRAMS = foo
#   $(foo_OBJECTS): $(gnulib_headers)
#
# The $(gnulib_headers) variable will expand to GNU-make order-only
# prerequisites when available, avoiding spurious incremental rebuilds when
# unused headers are changed.  If this feature is not available, it will
# expand to ordinary prerequisites.  It is therefore only appropriate for
# use in target prerequisites; the $(gnulib_raw_headers) variable may be
# used in other contexts when only the list of header files is required.
#
# This script also provides machinery for Gnulib symbol renaming via the
# glconfig.mk Makefile.am snippet; use of this feature is optional.
#
# Most of the specific transformations are documented below.

use strict;
use Getopt::Long;

my $output   = undef;
my $input    = undef;

my $use_libtool = undef;
my $for_library = undef;

my $line     = 0;

Getopt::Long::Configure("gnu_getopt", "no_auto_abbrev");
GetOptions(
	"o|output=s"   => \$output,
	"i|input=s"    => \$input,
	"library"      => sub { $for_library = 1; },
	"program"      => sub { $for_library = 0; },
);

open STDOUT, ">", $output or die "$output: $!\n" if (defined $output);
open STDIN,  "<", $input  or die "$input: $!\n"  if (defined $input);

my $printed_header = 0;
my @cleanfiles;

# Hashes to record make variables used in the automake source.  The allvars
# hash contains variables actually assigned in the Makefile, sourcevars
# contains variables used as filenames.  Keys are the variable name, and
# the value is always set to 1.
my (%allvars, %sourcevars);

# Collected names of subdirectories that may need to be created at build time.
# The keys are directory names, the values are targets.
my %gl_dirstamps;

# State to drop MKDIR_P lines that have been replaced by dirstamps.
my ($have_dirstamp) = (0);

sub drop {
	undef $_;
	next;
}

sub basename {
	my $file = shift;
	$file =~ m|(?:.+/)?([^/]+)/?|;
	return $1;
}

# Convert special characters in a filename to Automake mumble_FOO format.
sub tr_mumble {
	my $s = shift;
	$s =~ y|-/|_|;
	return $s;
}

sub mangle_file {
	my $word = shift;

	if ($word =~ /^\$\(([[:word:].]+)\)$/) {
		# Don't touch variables now, but record them for later.
		$sourcevars{$1} = 1;
	} elsif ($word =~ /^\$\((?:top_)?(?:srcdir|builddir)\)/) {
		# Do nothing.  Generic transformation will take care of
		# $(srcdir) and $(builddir).
	} elsif ($word =~ /^[[:word:].+\/-]+$/) {
		# Mangle the filename.  Assume that all relevant files start
		# with a non-dot (so we don't transform suffix rules) and
		# contain at least one dot (so we don't transform phony rules).
		$word = "lib/$word" if ($word =~ /^[^.]+\./);
	} else {
		print STDERR "$0:$line: warning: unrecognized source file: $word\n";
	}

	return "$word";
}

sub mangle_variable {
	my $raw = shift;

	$raw =~ /([^=]+=)[[:space:]]*(.*)/s;
	my ($left, @right) = ($1, split(/[[:space:]]+/, $2));

	return join(" ", ($left, map(mangle_file($_), @right))) . "\n";
}

sub mangle_target {
	my $raw = shift;

	$raw =~ /([^:]+):[[:space:]]*(.*)/s;
	my @left  = split(/[[:space:]]+/, $1);
	my @right = split(/[[:space:]]+/, $2);

	@left  = map(mangle_file($_), @left);
	@right = map(mangle_file($_), @right);

	my @dirstamps = get_dirstamps(@left);

	return join(" ", @left) . ": " . join(" ", @dirstamps, @right) . "\n";
}

sub get_dirstamps {
	my %h;

	foreach (@_) {
		next unless $_[0] =~ m|^(lib(/.*)?)/[^/]*$|;

		$h{$gl_dirstamps{$1} = "$1/\$(am__dirstamp)"} = 1;
	}

	return keys %h;
}

while (<STDIN>) {
	$line++;

	# Combine line splices.
	while (s/\\$//) {
		$line++;
		$_ = $_ . <STDIN>
	}

	next if (/^#/);

	if (!$printed_header) {
		print "# Postprocessed by ", basename($0), "\n\n";
		print <<'EOF';
# BEGIN AUTOMAKE/M4 POLYGLOT \
m4_unquote(m4_argn([2], [
.PHONY: # Automake code follows

# This trick should define gnulib_orderonly to | iff we're using GNU make.
gnulib_make_features = $(.FEATURES)
gnulib_have_orderonly = $(findstring order-only,${gnulib_make_features})
gnulib_orderonly = $(gnulib_have_orderonly:order-only=|)
gnulib_core_headers =
gnulib_raw_headers = $(gnulib_core_headers)
gnulib_headers = $(gnulib_orderonly) $(gnulib_raw_headers)

# Oddly, gnulib tries to add to MOSTLYCLEANDIRS (which is *not* an automake
# variable) without defining it.
MOSTLYCLEANDIRS =
EOF

		$printed_header = 1;
		drop;
	}

	# Locate the libgnu definition to determine whether or not the user is
	# using libtool mode in gnulib-tool.  Default to program mode if they
	# are not, which will avoid pulling in the glsym dependencies.
	#
	# Convert noinst to EXTRA, that way libgnu will not be built unless
	# something actually depends on it (which is typically the case).
	if (/^noinst_LIBRARIES.*libgnu.a/) {
		s/^noinst/EXTRA/;
		$for_library = 0 unless defined $for_library;
		$use_libtool = 0;
	}
	if (/^noinst_LTLIBRARIES.*libgnu.la/) {
		s/^noinst/EXTRA/;
		$for_library = 1 unless defined $for_library;
		$use_libtool = 1;
	}

	# For some reason, gnulib-tool adds core dumps to "make mostlyclean".
	# Since these files are (hopefully!) not created by make, they should
	# not be cleaned.
	drop if (/^MOSTLYCLEANFILES.*core/);

	# Some modules set AM_CPPFLAGS/AM_CFLAGS/etc. in a manner that is not
	# useful for non-recursive builds.  Strip them out.
	drop if (/^(AM_CPPFLAGS|AM_CFLAGS)/);

	# We don't care about upstream warning flags that just result in adding
	# massive amounts of additional build rules for no reason.
	if (/_CFLAGS/) {
		s/ *\$\(GL_CFLAG_GNULIB_WARNINGS\)// if /_CFLAGS\s*=/;
	}

	# Drop superfluous CFLAGS assignments (which may be created by above
	# transformation).
	drop if /_CFLAGS\s*=\s*\$\(AM_CFLAGS\)\s*$/;

	# Library dependencies are added automatically to libgnu.la by
	# gnulib-tool.  Unfortunately, this means that everything linking
	# against libgnu.la is forced to pull in the same deps, even if they're
	# unneeded.  Furthermore, a libtool linker flag reordering bug prevents
	# --as-needed from stripping out the useless deps, so it's better to
	# handle them all manually.
	drop if (/LDFLAGS/);

	# Current uses of SUFFIXES in gnulib are pointless since Automake will
	# figure it out all on its own.  Strip it out.
	drop if (/SUFFIXES/);

	# Rewrite automake hook targets to be more generic.
	if (s/^(.*)-local:/\1-gnulib:/) {
		print ".PHONY: $1-gnulib\n";
		print "$1-local: $1-gnulib\n";
		s/$1-generic//;
	}

	# We need to mangle filenames in make variables; prepending a lib/ on
	# relative paths.  The following should catch all variable assignments
	# that need mangling.
	if (/^([[:word:]]+)[[:space:]]*\+?=/) {
		$allvars{$1} = 1;

		if ($1 =~ /(_SOURCES|CLEANFILES|EXTRA_DIST|[[:upper:]]+_H)$/) {
			$_ = mangle_variable($_);
		}
	}

	# BUILT_SOURCES has similar problems to recursive make: inadequate
	# dependencies lead to incorrect builds.  Collect them into an
	# ordinary variable so we can deal with them later.
	s/BUILT_SOURCES/gnulib_core_headers/;

	# Targets are similar to variables: the target and its dependencies
	# need to be mangled.
	if (/^([^\t:]*):/) {
		$_ = mangle_target($_);
		$have_dirstamp = /am__dirstamp/;
	}

	# MKDIR_P commands need to be fixed up; in principle Gnulib could also
	# be patched here to use $(@D) instead (and thus automatically benefit
	# from the target being fixed up), but this will do for now.
	s/^(\t.*\$\(MKDIR_P\)) ([[:alnum:]]+)$/\1 lib\/\2/;

	# When using conditional-dependencies, *CLEANFILES can end up
	# depending on the configuration.  This means that "make distclean"
	# may not actually delete everything if the configuration changes
	# after building the package.  Stash all the variables for later so
	# they can be moved outside of any conditional.
	if (/(CLEANFILES|CLEANDIRS)[[:space:]]*\+=/) {
		push(@cleanfiles, $_);
		drop;
	}

	# Change t-$@ to $@-t as the former will break if $@ has a directory
	# component.
	s/t-\$@/\$\@-t/g;

	# $(srcdir), $(builddir) and %reldir% need to be fixed up.
	s:\$\(srcdir\):\$\(top_srcdir\)/lib:g;
	s:\$\(builddir\):\$\(top_builddir\)/lib:g;
	s:%reldir%:lib:g;

	# If we installed a dirstamp prerequisite for this target, don't
	# emit the mkdir line which creates the output directory.
	if ($have_dirstamp && m|\$[({]MKDIR_P[})][ '"]*lib/|) {
		drop unless s/^(\t\$[({](AM_V_GEN|gl_V_at)[})]).*/\1:/;
	}
	undef $have_dirstamp if /^\t/;
} continue { s/(\n.)/\\\1/g; print; };

# Define a bunch of fake programs which will ensure Automake produces the
# necessary dirstamp rules, as unfortunately we cannot know in advance which
# will be generated, and the usual Automake behaviour where generated rules
# are suppressed by rules in Makefile.am doesn't actaully work for these.
print <<EOF foreach (keys %gl_dirstamps);
EXTRA_PROGRAMS += $_/gl-dirstamp
${\(tr_mumble "$_/gl-dirstamp")}_SOURCES =
EOF

print <<'EOF' if ($use_libtool);
gnulib_lt_objects = $(libgnu_la_OBJECTS) $(gl_LTLIBOBJS)
gnulib_objects = $(gnulib_lt_objects)
gnulib_all_symfiles = $(gnulib_lt_objects:.lo=.glsym)
$(gnulib_objects): $(gnulib_headers)
EOF
print <<'EOF' if (!$use_libtool);
gnulib_objects = $(libgnu_a_OBJECTS) $(gl_LIBOBJS)
gnulib_all_symfiles = $(gnulib_objects:.@OBJEXT@=.glsym)
$(gnulib_objects): $(gnulib_headers)
EOF

print @cleanfiles;

print <<'EOF';
if FALSE
], [dnl M4 code follows

AC_SUBST([GLSRC], [lib])
AC_CONFIG_LIBOBJ_DIR([lib])

AC_DEFUN_ONCE([DX_GLSYM_PREFIX],
[AC_REQUIRE([DX_AUTOMAKE_COMPAT])AC_REQUIRE([DX_EXPORTED_SH])dnl
AC_SUBST([GLSYM_PREFIX], [$1])dnl
AC_SUBST([gnulib_symfiles], ['$(gnulib_all_symfiles)'])])
EOF

print <<'EOF' if ($for_library);
AC_CONFIG_COMMANDS_PRE([DX_GLSYM_PREFIX([${PACKAGE}__])])
EOF

print <<'EOF';
m4_foreach([gl_objvar], [[gl_LIBOBJS], [gl_LTLIBOBJS]], [dnl
set x $gl_objvar; shift
gl_objvar=
while test ${#} -gt 0; do
	gl_objvar="$gl_objvar lib/${1}"; shift
done
])
EOF

# Some filenames are AC_SUBSTed by the Gnulib macros, and thus we need to
# prepend lib/ if and only if they're not empty.  Unfortunately, make is not
# powerful to do this, so we need to put this transformation into configure
# itself by defining a new autoconf macro.

foreach (keys %sourcevars) {
	print "$_=\${$_:+lib/\$$_}\n" unless $allvars{$_};
}

print <<'EOF';
], [
endif
# ]))dnl
# END AUTOMAKE/M4 POLYGLOT
EOF
