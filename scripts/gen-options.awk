#!/bin/awk -f
#
# Generate definitions helpful when using getopt_long from an options
# specification file.
#
# Copyright © 2021, 2023-2024 Nick Bowler
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# The options specification file is processed line by line.  Any line
# beginning with a - character introduces a new option definition.  Each
# option definition specifies any or all of a short option name, a long
# option name, an argument specification, and an action specification.
#
# Only the long option name is mandatory.  It is not possible to define
# short options without a corresponding long option.
#
# The optional short option name is first, and consists of a hyphen (which
# must be the first character on the line) followed by the one character
# short option name, followed by a comma.
#
# The long option name is next on the line, which consists of two hyphens
# followed by the desired option name.  If the short option name was omitted,
# then the first hyphen of the long option name must be the first character
# on the line.
#
# The argument specification is next, consisting of an equals sign followed by
# the argument name.  The argument name can be any sequence of non-whitespace
# characters and only relevant for --help text.
#
# If the argument specification is surrounded by square brackets, this
# indicates an optional argument.  If the argument specification is omitted
# completely, this option has no argument.  Otherwise, the option has a
# mandatory argument.
#
# Finally, the optional action specification defines how the "flag" and
# "val" members are set in the option structure for this option.  An action
# specification may only be provided for options with no short name.
#
# If the action specification is omitted, then flag will be set to a null
# pointer and val is set to the short option character, if any, otherwise the
# unique enumeration constant LOPT_xxx for this option (described below).
#
# The action specification can be of the form (val) or (flag, val), where flag
# and val are C expressions suitable for use in an initializer for objects
# with static storage duration.  Neither flag nor val may contain commas or
# whitespace.  In the first form, the option's flag is set to a null pointer.
#
# Any amount of whitespace may follow the short option name, the argument
# specification, the action specification, or the comma within an action
# specification.  Whitespace is not permitted between a long option name
# and a flag specification.
#
# Examples of option specifications:
#
#   -h, --help
#   --do-nothing (0)
#   -o, --output=FILE
#   --pad[=VAL]
#   --parse-only (&parse_only, 1)
#
# Each option is assigned an enumeration constant of the form LOPT_xxx,
# where xxx is the long option name with all letters in uppercase and
# all non-alphanumeric characters replaced with underscores.  The value
# of the constants is unspecified, except that they will be unique across
# all defined options and distinct from the integer value of any short
# option character.
#
# The object-like macro SOPT_STRING expands to a string literal suitable
# for use as the optstring argument to getopt et al.
#
# The object-like macro LOPTS_INITIALIZER expands to a comma-separated
# sequence of struct option initializers, suitable for use in a declaration
# of an array of struct option elements with static storage duration.  The
# all-zero terminating element required by getopt_long must be added by the
# user.  For example:
#
#   static const struct option lopts[] = { LOPTS_INITIALIZER, {0} };
#
# If none of the options have action specifications, then an alternate
# set of macros is also defined, which encode the struct option array
# into a more compact format that can be used to generate the full
# 'struct option' array at runtime:
#
#   * the object-like macro LOPT_PACK_BITS expands to an integer constant
#     expression, suitable for use in #if directives, that specifies the
#     minimum number of bits required by the encoding.  LOPT_PACK_BITS2
#     is the same, but rounded up to the next power of two greater than
#     or equal to 8.
#
#   * the object-like macro LOPTS_PACKED_INITIALIZER expands to a
#     comma-separated sequence of integer constant expressions, suitable
#     for initializing an array of integers.  All values are less than
#     2^LOPT_PACK_BITS.
#
#   * the function-like macro LOPT_UNPACK(opt, x), where opt is an
#     lvalue of type 'struct option', and x is one of the array
#     elements initialized by LOPTS_PACKED_INITIALIZER.  This expands
#     the encoded value and sets the name, has_arg and val members of
#     opt appopriately.  The caller should ensure that the flag member
#     is set to zero.
#
# The help text for an individual struct option element may be obtained by
# the function
#
#   struct lopt_help { const char *desc, *arg; }
#   *lopt_get_help(const struct option *opt);
#
# The returned desc and arg pointers point to the argument name and help text
# for the argument, respectively, as written in the options specification file.

END {
  print "/*"
  if (FILENAME) {
    print " * Automatically generated by gen-options.awk from " FILENAME
  } else {
    print " * Automatically generated by gen-options.awk"
  }
  print " * Do not edit."
  print " */"
}

BEGIN {
  # Check if "\\\\" in substitutions gives just one backslash.
  bs = "x"; sub(/x/, "\\\\", bs);
  bs = (length(bs) == 1 ? "\\\\" : "\\");

  has_actions = 0
  sopt_string = ""
  num_options = 0
  lopt = ""
  err = 0
}

# Parse option specifier lines
$0 ~ /^-/ {
  work = $0
  arg = lopt = sopt = ""
  has_arg = 0

  # Extract short option name
  if (work ~ /^-[^-]/) {
    sopt = substr(work, 2, 1)
    sub(/^-.,[ \t]*/, "", work)
  }

  # Extract long option name
  if (work ~ /^--/) {
    if (n = match(work, /[= \t[]/)) {
      lopt = substr(work, 3, n-3)
      work = substr(work, n)
    } else {
      lopt = substr(work, 3)
      work = ""
    }
  }

  # Extract argument name
  if (work ~ /^\[=[^ \t]+\]/ && sub(/\]/, "&", work) == 1) {
    if (n = index(work, "]")) {
      arg = substr(work, 3, n-3)
      work = substr(work, n+1)
    }
    has_arg = 2
  } else if (work ~ /^=/) {
    if (n = match(work, /[ \t]/)) {
      arg  = substr(work, 2, n-2)
      work = substr(work, n)
    } else {
      arg  = substr(work, 2)
      work = ""
    }
    has_arg = 1
  }

  # Extract action
  sub(/^[ \t]*/, "", work)
  if (!sopt && work ~ /^\([^, \t]+(,[ \t]*[^, \t]+)?\)/) {
    # packed form is not possible w/ actions
    has_actions = 1;

    n = split(work, a, ",[ \t]*")
    if (n == 2) {
      flag = substr(a[1], 2) ", " substr(a[2], 1, length(a[2])-1)
    } else if (n == 1) {
      flag = "0, " substr(a[1], 2, length(a[1])-2)
    }
    sub(/^\([^, \t]+(,[ \t]*[^, \t]+)?/, "", work)
  } else if (sopt) {
    flag = "0, '" sopt "'"
  } else {
    flag = "0, " to_enum(lopt)
  }

  if (work) {
    print "invalid option specification:", $0 > "/dev/stderr"
    err = 1
    exit
  }

  if (sopt) {
    sopt_string = sopt_string sopt substr("::", 1, has_arg)
  }
  options[num_options++] = lopt
  optionspec[lopt] = has_arg ", " flag
  if (arg) {
    optionarg[lopt] = arg
  }

  next
}

# Ignore any line beginning with a #
$0 ~ /^#/ { next; }

NF && lopt != "" {
  sub(/^[ \t]*/, "");

  if (lopt in optionhelp)
    $0 = "\n" $0;
  optionhelp[lopt] = optionhelp[lopt] $0;
}

END {
  # Exit immediately on error
  if (err) exit err;

  print "#include <limits.h>\n";
  print "#define SOPT_STRING \"" sopt_string "\"";

  lopt_strings = "";
  count = bucketsort(sorted_options, options);
  for (i = 0; i < count; i++) {
    lopt_strings = add_to_strtab(lopt_strings, sorted_options[i], offsets);
  }
  gsub("\1", bs"0"bs"\n", lopt_strings);
  print "static const char lopt_strings[] = \"\\\n" lopt_strings "\";";

  print "\nenum {"
  for (i = 0; i < count; i++) {
    opt = options[i]
    sep = (i+1 == count ? "" : ",")

    print "\t" to_enum(opt), "= UCHAR_MAX+1 +", offsets[opt] sep
  }
  print "};"
  print "#define lopt_str(x) (lopt_strings + (LOPT_ ## x - UCHAR_MAX - 1))"

  if (!has_actions) {
    output_packed_macros()
  }

  print "\n#define LOPTS_INITIALIZER \\"
  for (i = 0; i < count; i++) {
    opt = options[i]
    sep = (i+1 == count ? "" : ", \\")

    print "\t/* --" opt, "*/ \\"
    print "\t{ lopt_strings+" offsets[opt] ",", optionspec[opt] " }" sep
  }

  output_help_function();
}

# Emit the lopt_get_help function, for generating --help output from the
# options description.
function output_help_function(strings, count, i)
{
  print "\nstatic struct lopt_help { const char *desc, *arg; }";
  print "*lopt_get_help(const struct option *opt, struct lopt_help *out)";
  print "{";

  count = bucketsort(help_sorted, optionhelp);
  for (i = 0; i < count; i++) {
    strings = add_to_strtab(strings, help_sorted[i], help_offsets);
  }

  count = bucketsort(help_sorted, optionarg);
  for (i = 0; i < count; i++) {
    strings = add_to_strtab(strings, help_sorted[i], help_arg_offsets);
  }

  # ensure empty string is offsets table
  strings = add_to_strtab(strings, "", help_offsets);

  gsub("\"", bs"\"", strings);
  gsub("\n", bs"n"bs"\n", strings);
  gsub("\1", bs"0"bs"\n", strings);
  print "\tstatic const char help_strings[] = \"\\\n" strings "\";";
  print "#if 0";
  for (i in options) {
    i = options[i];
    if (i in optionhelp) {
      i = optionhelp[i];
      gsub("\"", bs"\"", i);
      gsub("\n", bs"n\"\n\t        \"", i);
      print "\tgettext(\"" i "\");";
    } else {
      print "\tpgettext(\"" i "\", \"\");";
    }
  }
  for (i in optionarg) {
    print "\tpgettext(\"" i "\", \"" optionarg[i] "\");";
  }
  print "#endif";

  print "\n\tswitch((opt->name - lopt_strings) + UCHAR_MAX + 1) {";
  for (i in optionhelp) {
    print "\tcase " to_enum(i) ":";
    print "\t\tout->desc = help_strings+" help_offsets[optionhelp[i]] ";";
    if (i in optionarg) {
      print "\t\tout->arg  = help_strings+" help_arg_offsets[optionarg[i]] ";";
    }
    print "\t\treturn out;";
  }
  for (i in optionarg) {
    if (!(i in optionhelp)) {
      print "\tcase " to_enum(i) ":";
      print "\t\tout->arg = help_strings+" help_arg_offsets[optionarg[i]] ";";
      print "\t\tbreak;";
    }
  }
  print "\t}";

  for (i in options) {
    if (!(options[i] in optionhelp)) {
      print "\n\tout->desc = help_strings+" help_offsets[""] ";";
      print "\treturn out;";
      break;
    }
  }

  print "}";
}

# Emit the packed initializer macros.  This is used as an array initializer
# that encodes the following information:
#
#   - short option character offset
#   - arg value (0, 1 or 2), and
#   - long option string offset
#
# as a single integer value for each option, in as few bits as practical.
#
# Currently, this only works if none of the options use action specifications
# (as these would require encoding user-specified pointer expressions and
# arbitrary int values).
function output_packed_macros(i, tmp, accum, max, totalbits)
{
  print "";

  # determine number of bits to encode offsets in SOPT_STRING
  max = length(sopt_string);
  totalbits = accum = 0;
  for (i = 1; i <= max; i *= 2) {
    accum++;
  }
  print "#define LOPT_SC_BITS " accum;
  totalbits += accum;

  # determine number of bits to encode has_arg values
  max = 0;
  for (i in optionspec) {
    tmp = optionspec[i]; sub(/,.*/, "", tmp);
    if (tmp > max)
      max = tmp;
  }
  accum = (max > 1 ? 2 : max > 0 ? 1 : 0);
  print "#define LOPT_HA_BITS " accum;
  totalbits += accum;

  # determine number of bits to encode offsets in lopt_strings
  max = 0;
  for (i in offsets) {
    if (offsets[i] > max)
      max = offsets[i];
  }

  accum = 0;
  for (i = 1; i <= max; i *= 2) {
    accum++;
  }
  print "#define LOPT_LS_BITS " accum;
  totalbits += accum;

  print "#define LOPT_PACK_BITS " totalbits;
  for (i = 8; i < totalbits; i *= 2)
    ;
  print "#define LOPT_PACK_BITS2 " i;
  print "#define LOPT_PACK_TYPE uint_least" i "_t"

  # Now emit the packed initializer macro
  print "\n#define LOPTS_PACKED_INITIALIZER \\";
  accum = "";
  for (i = 0; i < count; i++) {
    if (accum)
      print "\t" accum ", \\";

    tmp = options[i];
    accum = "("offsets[tmp] "ul" "<<LOPT_HA_BITS)";
    max = tmp = optionspec[tmp];
    sub(/,.*/, "", max)
    accum = "((" accum "|" max ")<<LOPT_SC_BITS)";

    sub(/.*[, ]/, "", tmp);
    if (tmp ~ /^[']/) {
      tmp = index(sopt_string, substr(tmp, 2, 1)) - 1;
    } else {
      tmp = length(sopt_string);
    }
    accum = accum "|" tmp;
  }

  if (accum)
    print "\t" accum;

  # Finally, the unpack helper macros
  tmp = "(x) & ((1ul<<LOPT_SC_BITS)-1)";
  print "\n#define LOPT_UNPACK_VAL(x) \\"
  print "\t( SOPT_STRING[" tmp "] \\";
  print "\t? SOPT_STRING[" tmp "] \\";
  print "\t: 1u + UCHAR_MAX + ((x)>>(LOPT_SC_BITS+LOPT_HA_BITS)))";

  print "\n#define LOPT_UNPACK_ARG(x) \\";
  print "\t(((x)>>LOPT_SC_BITS)&((1ul<<LOPT_HA_BITS)-1))";

  print "\n#define LOPT_UNPACK_NAME(x) \\"
  print "\t(lopt_strings+((x)>>(LOPT_SC_BITS+LOPT_HA_BITS)))";

  print "\n#define LOPT_UNPACK(opt, x) do { \\";
  print "\t(opt).name = LOPT_UNPACK_NAME(x); \\"
  print "\t(opt).has_arg = LOPT_UNPACK_ARG(x); \\"
  print "\t(opt).val = LOPT_UNPACK_VAL(x); \\"
  print "} while (0)";
}

# bucketsort(dst, src)
#
# Sort the elements of src by descending string length,
# placing them into dst[0] ... dst[n].
#
# Returns the number of elements.
function bucketsort(dst, src, max, count, i, t)
{
  # Note: ULTRIX 4.5 nawk does not support local array parameters
  split("", bucketsort_buckets);

  for (t in src) {
    i = length(src[t])
    if (i > max) { max = i }
    bucketsort_buckets[i]++
  }

  for (i = max; i > 0; i--) {
    if (i in bucketsort_buckets) {
      t = bucketsort_buckets[i]
      bucketsort_buckets[i] = count
      count += t
    }
  }

  for (t in src) {
    i = length(t = src[t])
    dst[bucketsort_buckets[i]++] = t
  }

  return count
}

# to_enum(lopt)
#
# Return the string LOPT_xxx, where xxx is the argument with all lowercase
# letters converted to uppercase, and all non-alphanumeric characters replaced
# with underscores.
function to_enum(lopt)
{
  lopt = toupper(lopt)
  gsub(/[^ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789]/, "_", lopt)
  return "LOPT_" lopt
}

# add_to_strtab(strtab, str, offsets)
#
# Append string to strtab if there is not already a matching string present
# in the table.  Newly-added strings are separated by "\1", which must be
# translated into null bytes afterwards.  The updated strtab is returned, and
# the offsets[str] array member is updated with the position (counting from 0)
# of str in the strtab.
#
# For optimal results, strings should be added in descending length order.
function add_to_strtab(strtab, str, offsets, pos)
{
  if ( (pos = index(strtab, str "\1") - 1) < 0) {
    pos = length(strtab);
    if (pos) {
      strtab = strtab "\1" str;
      pos++
    } else {
      strtab = strtab str;
    }
  }

  offsets[str] = pos;
  return strtab;
}
