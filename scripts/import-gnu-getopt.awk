#!/bin/awk -f
#
# Patcher script to import getopt.c from Gnulib in order to remove
# bits and make it directly usable as a fallback getopt_long(_only)
# implementation in programs.
#
# Copyright © 2024 Nick Bowler
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Exercising section 3 of the LGPLv2.1 to the modified library:
#
#   You may opt to apply the terms of the ordinary GNU General Public
#   License instead of this License to a given copy of the Library.  To
#   do this, you must alter all the notices that refer to this License,
#   so that they refer to the ordinary GNU General Public License, version 2,
#   instead of this License.  (If a newer version than version 2 of the
#   ordinary GNU General Public License has appeared, then you can specify
#   that version instead if you wish.)  Do not make any other change in
#   these notices.
{
  # "GNU Lesser General Public License" -> "GNU General Public License"
  sub(/Lesser General/, "General");

  # "version 2.1 of the License" -> "version 3 of the License"
  sub(/version 2[.]1/, "version 3");
}

# Remove all conditionally compiled blocks
BEGIN { cond_depth = 0; }
/^#[\t ]*if/ { cond_depth++; }
/^#[\t ]*endif/ { cond_depth--; next; }
cond_depth > 0 { next; }

# Replace references to getopt_data with direct variable names
{ gsub(/d->__/, ""); gsub(/d->opt/, "opt"); }

# Rename stuff out of the implementation-reserved namespace
{ gsub(/_getopt_internal_r/, "gnu_getopt"); }
{ gsub(/_getopt_initialize/, "initialize"); }

# Remove comment and macro about removed plain "getopt"
/LSB-compliant getopt/ { while (!/[*][/]/) getline; next; }
/^#[\t ]*define +GETOPT_ENTRY/ { while (/\\$/) getline; next; }

# Avoid further touches to block comments
BEGIN { first_comment = in_comment = 0; }
/^[\t ]*[/][*]/ { in_comment = 1; }
in_comment == 1 {
  if (/[*][/]/) {
    if (first_comment == 0) first_comment = 1;
    in_comment = 0;
  }

  print deferred_blank (last_line = $0);
  deferred_blank = "";
  next;
}

# Add our stuff right after the copyright notice
first_comment == 1 {
  first_comment = -1;

  date_cmd="date +%Y-%m-%d";
  date_cmd | getline date;
  close(date_cmd);

  print "\n/*";
  print " * This file has been modified from its original version,";
  if (date) date = " on " date;
  print " * postprocessed by import-gnu-getopt.awk" date;
  print " */";

  if (!test_mode) {
    print "\n#if HAVE_CONFIG_H";
    print "#\tinclude <config.h>";
    print "#endif\n";

    print "#include <stdio.h>";
    print "#include <stdlib.h>";
    print "#include <string.h>";
    print "#include \"gnu_getopt.h\"\n";

    print "#if ENABLE_NLS"
    print "#\tinclude <libintl.h>"
    print "#else"
    print "#\tdefine gettext(x) (x)";
    print "#endif";
  }
}

# Remove all includes from the input.
/^#[\t ]*include/ { next; }

# Remove linter nonsense
{ gsub(/_GL_UNUSED */, ""); }

# Remove alloca
{ gsub(/(__libc_use_alloca|alloca) *[(][^)]*[)]/, "0"); }

# Convert _() to a direct gettext call
{ gsub(/_[(]"/, "gettext(\""); }

# Remove stdio locking
/f(un)?lockfile/ { next; }

# Replace global getopt_data struct with separate global variables ...
/^static.*getopt_data/ {
  $0 = "static int first_nonopt, last_nonopt;\n" \
       "static char *nextchar;\n" \
       "static unsigned char ordering, initialized;\n" \
       "enum { REQUIRE_ORDER, PERMUTE, RETURN_IN_ORDER };";
}

# ... and remove parameter declarations of it everywhere ...
{ sub(/, *struct +_getopt_data *[*] *d/, ""); }
/^[\t ]*struct +_getopt_data *[*] *d,/ {
  sub(/struct +_getopt_data *[*] *d, */, "");
}

# ... and remove it from function calls
{ gsub(/, *d *,/, ","); gsub(/, *d *[)]/, ")"); }

# Also remove posixly_correct parameter from functions
{
  sub(/, *int +posixly_correct *[)]/, ")");
  sub(/int +posixly_correct *[)]/, "int unused)");
  gsub(/posixly_correct/, "0");
}

# Remove _getopt_internal function completely
BEGIN { func_delete = 0; func_block = 0; func_tmp = ""; }
/^int$/ { func_tmp = $0; next; }
func_tmp != "" {
  if ($1 == "_getopt_internal") {
    func_delete = 1;
  } else {
    $0 = func_tmp ORS $0;
  }
  func_tmp = "";
}

func_delete != 0 {
  if ((func_block += gsub(/{/, "{")) > 0) {
    if ((func_block -= gsub(/}/, "}")) == 0) {
      func_delete = 0;
    }
  }
  $0 = "";
}

# For the tests, move optind/opterr assignments into do_getopt_long_(only), as
# all callers explicitly set these to 1 and 0, respectivly, prior to the call.
# In addition to reducing the complexity of the test program, this also works
# around a code generation bug in old versions of MIPSpro C.
BEGIN { in_wrapper = 0; in_test = 0; }
/^do_getopt_long/ { in_wrapper = 1; }
in_wrapper && /{/ {
  $0 = $0 "\n  optind = 1, opterr = 0;";
  in_wrapper = 0;
}

# Drop no longer needed optind/opterr assignments
in_test && (/optind *= *1/ || /opterr *= *0/) { next; }
/^test_getopt/ { in_test = 1; }
in_test && /^}/ { in_test = 0; }

# Consolidate redundant declarations in the test functions
in_test == 2 && /char *[*]argv\[/ { next; }
in_test == 2 && /int *c;/ { next; }
in_test == 1 && /{/ {
  $0 = $0 "\n  const char *argv[20];\n  int c;";
  in_test = 2;
}

# Remove consecutive blank lines which may be generated by the above
BEGIN { last_line = ""; deferred_blank = ""; }
{
  empty = sub(/^[\f\t ]*$/, "")

  if (!empty && deferred_blank) {
    print (deferred_blank = "");
  }

  if (!empty || last_line != "") {
    if ($0 == "") {
      deferred_blank = ORS;
    } else {
      print $0;
    }
  }

  last_line = $0;
}
