/*
 * Copyright © 2009 Nick Bowler
 *
 * Portable binary (de-)serialisation of integral types.
 *
 * License WTFPL2: Do What The Fuck You Want To Public License, version 2.
 * This is free software: you are free to do what the fuck you want to.
 * There is NO WARRANTY, to the extent permitted by law.
 */

#include "pack.h"

/* Unsigned integer packing. */
#define DEFPACK_BE(bits, type) void pack_ ## bits ## _be ( \
	unsigned char *out, type v \
) { \
	unsigned i; \
	for (i = 1; i <= bits/8; i++) { \
		out[bits/8 - i] = v % 256; \
		v /= 256; \
	} \
}

#define DEFPACK_LE(bits, type) void pack_ ## bits ## _le ( \
	unsigned char *out, type v \
) { \
	unsigned i; \
	for (i = 0; i < bits/8; i++) { \
		out[i] = v % 256; \
		v /= 256; \
	} \
}

DEFPACK_BE(16, unsigned short)
DEFPACK_BE(32, unsigned long)
#if PACK_HAVE_64BIT
DEFPACK_BE(64, unsigned long long)
#endif

DEFPACK_LE(16, unsigned short)
DEFPACK_LE(32, unsigned long)
#if PACK_HAVE_64BIT
DEFPACK_LE(64, unsigned long long)
#endif

#define DEFUNPACK_BE(bits, type) type unpack_ ## bits ## _be ( \
	const unsigned char *in \
) { \
	type v = 0; \
	unsigned i; \
	for (i = 0; i < bits/8; i++) { \
		v *= 256; \
		v += in[i]; \
	} \
	return v; \
}

#define DEFUNPACK_LE(bits, type) type unpack_ ## bits ## _le ( \
	const unsigned char *in \
) { \
	type v = 0; \
	unsigned i; \
	for (i = 1; i <= bits/8; i++) { \
		v *= 256; \
		v += in[bits/8 - i]; \
	} \
	return v; \
}

DEFUNPACK_BE(16, unsigned short)
DEFUNPACK_BE(32, unsigned long)
#if PACK_HAVE_64BIT
DEFUNPACK_BE(64, unsigned long long)
#endif

DEFUNPACK_LE(16, unsigned short)
DEFUNPACK_LE(32, unsigned long)
#if PACK_HAVE_64BIT
DEFUNPACK_LE(64, unsigned long long)
#endif

/*
 * Two's complement signed integer packing.  This is unlikely to work on
 * systems that don't themselves use two's complement.
 */

#define DEFUNPACK_SBE(bits, max, type) type unpack_s ## bits ## _be ( \
	const unsigned char *in \
) { \
	type v = 0; \
	unsigned i; \
	int sign = (in[0] & 0x80) ? 1 : 0; \
	for (i = 0; i < bits/8; i++) { \
		v *= 256; \
		v += in[i] & (i == 0 ? 0x7f : 0xff); \
	} \
	return sign*(-max-1) + v; \
}

#define DEFUNPACK_SLE(bits, max, type) type unpack_s ## bits ## _le ( \
	const unsigned char *in \
) { \
	type v = 0; \
	unsigned i; \
	int sign = (in[bits/8 - 1] & 0x80) ? 1 : 0; \
	for (i = 1; i <= bits/8; i++) { \
		v *= 256; \
		v += in[bits/8 - i] & (i == 1 ? 0x7f : 0xff); \
	} \
	return sign*(-max-1) + v; \
}

DEFUNPACK_SBE(16, 32767, short)
DEFUNPACK_SBE(32, 2147483647l, long)
#if PACK_HAVE_64BIT
DEFUNPACK_SBE(64, 9223372036854775807ll, long long)
#endif

DEFUNPACK_SLE(16, 32767, short)
DEFUNPACK_SLE(32, 2147483647l, long)
#if PACK_HAVE_64BIT
DEFUNPACK_SLE(64, 9223372036854775807ll, long long)
#endif
