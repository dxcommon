/*
 * Copyright © 2023 Nick Bowler
 *
 * Helper function to output the copyright symbol in a specified encoding.
 *
 * License WTFPL2: Do What The Fuck You Want To Public License, version 2.
 * This is free software: you are free to do what the fuck you want to.
 * There is NO WARRANTY, to the extent permitted by law.
 */

#ifndef DX_COPYSYM_H_
#define DX_COPYSYM_H_

#if ENABLE_NLS
const char *copyright_symbol(const char *charset);
#else
#define copyright_symbol(x) "(C)"
#endif

#endif
