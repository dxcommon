/*
 * Copyright © 2015 Nick Bowler
 *
 * Simple TAP output library for C programs.
 *
 * License WTFPL2: Do What The Fuck You Want To Public License, version 2.
 * This is free software: you are free to do what the fuck you want to.
 * There is NO WARRANTY, to the extent permitted by law.
 */

#ifndef DX_TAP_H_
#define DX_TAP_H_

#include <stdarg.h>

void tap_plan(unsigned expected_tests);
void tap_done(void);

void tap_vbail_out(const char *fmt, va_list ap);
void tap_bail_out(const char *fmt, ...);

void tap_vskip_all(const char *fmt, va_list ap);
void tap_skip_all(const char *fmt, ...);

void tap_vdiag(const char *fmt, va_list ap);
void tap_diag(const char *fmt, ...);

int tap_vresult(int ok, const char *fmt, va_list ap);
int tap_result(int ok, const char *fmt, ...);

#endif
