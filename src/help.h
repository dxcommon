/*
 * Helper functions for formatting --help program output.
 *
 * Copyright © 2021, 2023-2024 Nick Bowler
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DX_HELP_H_
#define DX_HELP_H_

/*
 * On some very old preprocessors (e.g., VAX C) -Dinline= defines inline to 1,
 * so when not using a config header detect and work around the problem here.
 */
#if !HAVE_CONFIG_H && (inline - 1 == 0)
#  undef inline
#  define inline
#endif

struct option;

/*
 * Print an option string describing the short option character (if any),
 * the long option name, and the argument name (if applicable).  The argument
 * name is localized (if NLS is enabled).  If the string width is more than
 * l columns, a newline is printed and 0 is returned.  Otherwise, a newline
 * is not printed and the string width (in columns) is returned.
 */
int help_print_optstring(const struct option *opt, const char *argname, int l);

/*
 * Print an option description with each line indented.  If opt is not NULL,
 * then the string is first localized (if NLS is enabled) via pgettext_expr
 * with the context set to opt->name.  The first line will be indented by
 * i-w spaces (to account for the cursor being in some other column), all
 * other lines are indented by i spaces.
 *
 * The output always ends with a newline, regardless of whether or not the
 * input string ends with a newline.
 */
void help_print_desc(const struct option *opt, const char *desc, int i, int w);

static inline void help_print_option(const struct option *opt,
                                     const char *argname, const char *desc,
				     int w)
{
	if (w < 2)
		w = 2;

	help_print_desc(opt, desc, w,
	                help_print_optstring(opt, argname, w-2));
}

#endif
