/*
 * Copyright © 2009, 2023 Nick Bowler
 *
 * Portable binary (de-)serialisation of integral types.
 *
 * License WTFPL2: Do What The Fuck You Want To Public License, version 2.
 * This is free software: you are free to do what the fuck you want to.
 * There is NO WARRANTY, to the extent permitted by law.
 */

#ifndef DX_PACK_H_
#define DX_PACK_H_

#ifndef PACK_HAVE_64BIT
#  if HAVE_LONG_LONG_INT
#    define PACK_HAVE_64BIT 1
#  else
#    include <limits.h>
#    if defined(LLONG_MAX) || defined(LONG_LONG_MAX)
#      define PACK_HAVE_64BIT 1
#    endif
#  endif
#endif

short unpack_s16_be(const unsigned char *);
short unpack_s16_le(const unsigned char *);
unsigned short unpack_16_be(const unsigned char *);
unsigned short unpack_16_le(const unsigned char *);
void pack_16_be(unsigned char *, unsigned short);
void pack_16_le(unsigned char *, unsigned short);

long unpack_s32_be(const unsigned char *);
long unpack_s32_le(const unsigned char *);
unsigned long unpack_32_be(const unsigned char *);
unsigned long unpack_32_le(const unsigned char *);
void pack_32_be(unsigned char *, unsigned long);
void pack_32_le(unsigned char *, unsigned long);

#if PACK_HAVE_64BIT
long long unpack_s64_be(const unsigned char *);
long long unpack_s64_le(const unsigned char *);
unsigned long long unpack_64_be(const unsigned char *);
unsigned long long unpack_64_le(const unsigned char *);
void pack_64_be(unsigned char *, unsigned long long);
void pack_64_le(unsigned char *, unsigned long long);
#endif

#endif
