/*
 * Copyright © 2015, 2023 Nick Bowler
 *
 * Simple TAP output library for C programs.
 *
 * License WTFPL2: Do What The Fuck You Want To Public License, version 2.
 * This is free software: you are free to do what the fuck you want to.
 * There is NO WARRANTY, to the extent permitted by law.
 */

#if HAVE_CONFIG_H
#	include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "tap.h"

static unsigned plan, total, passed, xpassed, failed, xfailed;

void tap_plan(unsigned expected_tests)
{
	if (plan)
		tap_bail_out("tap_plan: tests already planned!");
	if (!expected_tests)
		tap_bail_out("tap_plan: no tests to run!");

	plan = expected_tests;
	printf("1..%u\n", plan);
}

void tap_done(void)
{
	if (!plan) {
		tap_plan(total);
	} else if (total != plan) {
		tap_bail_out("tap_done: planned %u tests, ran %u", plan, total);
	}

	assert(total == passed + xpassed + failed + xfailed);
	exit(failed || xpassed);
}

void tap_vbail_out(const char *fmt, va_list ap)
{
	printf("Bail out!%*s", fmt != NULL, "");
	if (fmt)
		vprintf(fmt, ap);
	putchar('\n');
	exit(99);
}

void tap_bail_out(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	tap_vbail_out(fmt, ap);
	va_end(ap);
}

void tap_vskip_all(const char *fmt, va_list ap)
{
	if (plan || total)
		tap_bail_out("tap_skip_all: already started tests");

	printf("1..0 # skip%*s", fmt != NULL, "");
	if (fmt)
		vprintf(fmt, ap);
	putchar('\n');
	exit(77);
}

void tap_skip_all(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	tap_vskip_all(fmt, ap);
	va_end(ap);
}

void tap_vdiag(const char *fmt, va_list ap)
{
	printf("#%*s", fmt != NULL, "");
	if (fmt)
		vprintf(fmt, ap);
	putchar('\n');
}

void tap_diag(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	tap_vdiag(fmt, ap);
	va_end(ap);
}

int tap_vresult(int ok, const char *fmt, va_list ap)
{
	if (!ok)
		printf("not ");
	printf("ok %u%*s", ++total, fmt != NULL, "");
	if (fmt)
		vprintf(fmt, ap);
	putchar('\n');

	if (!total)
		tap_bail_out("cannot handle so many tests");

	passed += !!ok;
	failed += !ok;

	return ok;
}

int tap_result(int ok, const char *fmt, ...)
{
	va_list ap;
	int ret;

	va_start(ap, fmt);
	ret = tap_vresult(ok, fmt, ap);
	va_end(ap);

	return ret;
}
