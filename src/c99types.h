/*
 * Simple header to include headers for C99 types, otherwise provide fallback
 * macros for a subset of these in order to support older implementations.
 *
 * Copyright © 2024 Nick Bowler
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/*
 * If HAVE_STDBOOL_H is defined to 1, then <stdbool.h> is included to provide
 * bool, true and false macros.  Otherwise, if HAVE__BOOL is defined to 1 then
 * bool is defined as an object-like macro which expands to _Bool, otherwise
 * bool expands to signed char.  These configuration macros may be set by
 * using the Autoconf AC_HEADER_STDBOOL macro.
 *
 * Note that the signed char fallback does not provide full C99 semantics
 * but it is usually not tricky to write code that works either way.
 *
 * If HAVE_INTTYPES_H is defined to 1, then <inttypes.h> is included to provide
 * C99 integer type definitions.  If HAVE_STDINT_H is defined to 1, <stdint.h>
 * is included.
 *
 * If missing, fallback definitions of (u)int_least8_t, (u)int_least16_t,
 * (u)int_least32_t and their corresponding limit macros are provided by
 * using the <limits.h> to select the least-rank standard integer type
 * with a suitable range.
 *
 * For uint_least64_t, a fallback is defined to "unsigned long", if it is
 * sufficient large, otherwise "unsigned long long", if the configuration macro
 * HAVE_UNSIGNED_LONG_LONG_INT is defined to 1, otherwise "unsigned __int64",
 * if the configuration macro HAVE_UNSIGNED___INT64 is defined to 1.
 *
 * If (u)int_fast32_t are missing, then they are defined as macros expanding to
 * the corresponding (u)int_least32_t type along with their corresponding limit
 * macros.
 */
#ifndef DX_C99TYPES_H_
#define DX_C99TYPES_H_

#include <limits.h>
#if HAVE_INTTYPES_H
#  include <inttypes.h>
#endif
#if HAVE_STDINT_H
#  include <stdint.h>
#endif

#if HAVE_STDBOOL_H
#  include <stdbool.h>
#else
#  if HAVE__BOOL
#    define bool _Bool
#  else
#    define bool signed char
#  endif
#  define false 0
#  define true 1
#endif

#ifndef INT_LEAST8_MAX
#  define int_least8_t signed char
#  define INT_LEAST8_MAX SCHAR_MAX
#  define INT_LEAST8_MIN SCHAR_MIN
#endif

#ifndef UINT_LEAST8_MAX
#  define uint_least8_t unsigned char
#  define UINT_LEAST8_MAX UCHAR_MAX
#endif

#ifndef INT_LEAST16_MAX
#  if SCHAR_MAX < 32767
#    define int_least16_t signed short
#    define INT_LEAST16_MAX SHRT_MAX
#    define INT_LEAST16_MIN SHRT_MIN
#  else
#    define int_least16_t signed char
#    define INT_LEAST16_MAX SCHAR_MAX
#    define INT_LEAST16_MIN SCHAR_MIN
#  endif
#endif

#ifndef UINT_LEAST16_MAX
#  if UCHAR_MAX < 65535
#    define uint_least16_t unsigned short
#    define UINT_LEAST16_MAX USHRT_MAX
#  else
#    define uint_least16_t unsigned char
#    define UINT_LEAST16_MAX UCHAR_MAX
#  endif
#endif

#ifndef INT_LEAST32_MAX
#  if INT_MAX < 2147483647
#    define int_least32_t signed long
#    define INT_LEAST32_MAX LONG_MAX
#    define INT_LEAST32_MIN LONG_MIN
#  elif SHRT_MAX < 2147483647
#    define int_least32_t signed int
#    define INT_LEAST32_MAX INT_MAX
#    define INT_LEAST32_MIN INT_MIN
#  elif SCHAR_MAX < 2147483647
#    define int_least32_t signed short
#    define INT_LEAST32_MAX SHRT_MAX
#    define INT_LEAST32_MIN SHRT_MIN
#  else
#    define int_least32_t signed char
#    define INT_LEAST32_MAX SCHAR_MAX
#    define INT_LEAST32_MIN SCHAR_MIN
#  endif
#endif

#ifndef UINT_LEAST32_MAX
#  if UINT_MAX < 4294967295
#    define uint_least32_t unsigned long
#    define UINT_LEAST32_MAX ULONG_MAX
#  elif USHRT_MAX < 4294967295
#    define uint_least32_t unsigned int
#    define UINT_LEAST32_MAX UINT_MAX
#  elif UCHAR_MAX < 4294967295
#    define uint_least32_t unsigned short
#    define UINT_LEAST32_MAX USHRT_MAX
#  else
#    define uint_least32_t unsigned char
#    define UINT_LEAST32_MAX UCHAR_MAX
#  endif
#endif

#ifndef UINT_LEAST64_MAX
#  if !(((ULONG_MAX >> 16) >> 16) >> 31)
#    if HAVE_UNSIGNED_LONG_LONG_INT
#      define uint_least64_t unsigned long long
#      if defined ULLONG_MAX
#        define UINT_LEAST64_MAX ULLONG_MAX
#      elif defined ULONG_LONG_MAX
#        define UINT_LEAST64_MAX ULONG_LONG_MAX
#      else
#        define UINT_LEAST64_MAX ((uint_least64_t)-1)
#      endif
#    elif HAVE_UNSIGNED___INT64
#      define uint_least64_t unsigned __int64
#      ifdef _UI64_MAX
#        define UINT_LEAST64_MAX _UI64_MAX
#      else
#        define UINT_LEAST64_MAX ((uint_least64_t)-1)
#      endif
#    endif
#  else
#    define uint_least64_t unsigned long
#    define UINT_LEAST64_MAX ULONG_MAX
#  endif
#endif

#ifndef INT_FAST32_MAX
#  define int_fast32_t int_least32_t
#  define INT_FAST32_MAX INT_LEAST32_MAX
#  define INT_FAST32_MIN INT_LEAST32_MIN
#endif

#ifndef UINT_FAST32_MAX
#  define uint_fast32_t uint_least32_t
#  define UINT_FAST32_MAX UINT_LEAST32_MAX
#endif

#endif
