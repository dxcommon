/* Getopt for GNU.
   Copyright (C) 1987-2024 Free Software Foundation, Inc.
   This file is part of the GNU C Library and is also part of gnulib.
   Patches to this file should be submitted to both projects.

   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.

   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public
   License along with the GNU C Library; if not, see
   <https://www.gnu.org/licenses/>.  */

/*
 * This file has been modified from its original version,
 * postprocessed by import-gnu-getopt.awk on 2024-07-30
 */

#if HAVE_CONFIG_H
#	include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "gnu_getopt.h"

#if ENABLE_NLS
#	include <libintl.h>
#else
#	define gettext(x) (x)
#endif

/* This implementation of 'getopt' has three modes for handling
   options interspersed with non-option arguments.  It can stop
   scanning for options at the first non-option argument encountered,
   as POSIX specifies.  It can continue scanning for options after the
   first non-option argument, but permute 'argv' as it goes so that,
   after 'getopt' is done, all the options precede all the non-option
   arguments and 'optind' points to the first non-option argument.
   Or, it can report non-option arguments as if they were arguments to
   the option character '\x01'.

   The default behavior of 'getopt_long' is to permute the argument list.
   When this implementation is used standalone, the default behavior of
   'getopt' is to stop at the first non-option argument, but when it is
   used as part of GNU libc it also permutes the argument list.  In both
   cases, setting the environment variable POSIXLY_CORRECT to any value
   disables permutation.

   If the first character of the OPTSTRING argument to 'getopt' or
   'getopt_long' is '+', both functions will stop at the first
   non-option argument.  If it is '-', both functions will report
   non-option arguments as arguments to the option character '\x01'.  */

/* For communication from 'getopt' to the caller.
   When 'getopt' finds an option that takes an argument,
   the argument value is returned here.
   Also, when 'ordering' is RETURN_IN_ORDER,
   each non-option ARGV-element is returned here.  */

char *optarg;

/* Index in ARGV of the next element to be scanned.
   This is used for communication to and from the caller
   and for communication between successive calls to 'getopt'.

   On entry to 'getopt', zero means this is the first call; initialize.

   When 'getopt' returns -1, this is the index of the first of the
   non-option elements that the caller should itself scan.

   Otherwise, 'optind' communicates from one call to the next
   how much of ARGV has been scanned so far.  */

/* 1003.2 says this must be 1 before any call.  */
int optind = 1;

/* Callers store zero here to inhibit the error message
   for unrecognized options.  */

int opterr = 1;

/* Set to an option character which was unrecognized.
   This must be initialized on some systems to avoid linking in the
   system's own getopt implementation.  */

int optopt = '?';

/* Keep a global copy of all internal members of getopt_data.  */

static int first_nonopt, last_nonopt;
static char *nextchar;
static unsigned char ordering, initialized;
enum { REQUIRE_ORDER, PERMUTE, RETURN_IN_ORDER };

/* Exchange two adjacent subsequences of ARGV.
   One subsequence is elements [first_nonopt,last_nonopt)
   which contains all the non-options that have been skipped so far.
   The other is elements [last_nonopt,optind), which contains all
   the options processed since those non-options were skipped.

   'first_nonopt' and 'last_nonopt' are relocated so that they describe
   the new indices of the non-options in ARGV after they are moved.  */

static void
exchange (char **argv)
{
  int bottom = first_nonopt;
  int middle = last_nonopt;
  int top = optind;
  char *tem;

  /* Exchange the shorter segment with the far end of the longer segment.
     That puts the shorter segment into the right place.
     It leaves the longer segment in the right place overall,
     but it consists of two parts that need to be swapped next.  */

  while (top > middle && middle > bottom)
    {
      if (top - middle > middle - bottom)
	{
	  /* Bottom segment is the short one.  */
	  int len = middle - bottom;
	  int i;

	  /* Swap it with the top part of the top segment.  */
	  for (i = 0; i < len; i++)
	    {
	      tem = argv[bottom + i];
	      argv[bottom + i] = argv[top - (middle - bottom) + i];
	      argv[top - (middle - bottom) + i] = tem;
	    }
	  /* Exclude the moved bottom segment from further swapping.  */
	  top -= len;
	}
      else
	{
	  /* Top segment is the short one.  */
	  int len = top - middle;
	  int i;

	  /* Swap it with the bottom part of the bottom segment.  */
	  for (i = 0; i < len; i++)
	    {
	      tem = argv[bottom + i];
	      argv[bottom + i] = argv[middle + i];
	      argv[middle + i] = tem;
	    }
	  /* Exclude the moved top segment from further swapping.  */
	  bottom += len;
	}
    }

  /* Update records for the slots the non-options now occupy.  */

  first_nonopt += (optind - last_nonopt);
  last_nonopt = optind;
}

/* Process the argument starting with nextchar as a long option.
   optind should *not* have been advanced over this argument.

   If the value returned is -1, it was not actually a long option, the
   state is unchanged, and the argument should be processed as a set
   of short options (this can only happen when long_only is true).
   Otherwise, the option (and its argument, if any) have been consumed
   and the return value is the value to return from gnu_getopt.  */
static int
process_long_option (int argc, char **argv, const char *optstring,
		     const struct option *longopts, int *longind,
		     int long_only,
		     int print_errors, const char *prefix)
{
  char *nameend;
  size_t namelen;
  const struct option *p;
  const struct option *pfound = NULL;
  int n_options;
  int option_index;

  for (nameend = nextchar; *nameend && *nameend != '='; nameend++)
    /* Do nothing.  */ ;
  namelen = nameend - nextchar;

  /* First look for an exact match, counting the options as a side
     effect.  */
  for (p = longopts, n_options = 0; p->name; p++, n_options++)
    if (!strncmp (p->name, nextchar, namelen)
	&& namelen == strlen (p->name))
      {
	/* Exact match found.  */
	pfound = p;
	option_index = n_options;
	break;
      }

  if (pfound == NULL)
    {
      /* Didn't find an exact match, so look for abbreviations.  */
      unsigned char *ambig_set = NULL;
      /* Use simpler fallback diagnostic if ambig_set == &ambig_fallback.  */
      unsigned char ambig_fallback;
      void *ambig_malloced = NULL;
      int indfound = -1;

      for (p = longopts, option_index = 0; p->name; p++, option_index++)
	if (!strncmp (p->name, nextchar, namelen))
	  {
	    if (pfound == NULL)
	      {
		/* First nonexact match found.  */
		pfound = p;
		indfound = option_index;
	      }
	    else if (long_only
		     || pfound->has_arg != p->has_arg
		     || pfound->flag != p->flag
		     || pfound->val != p->val)
	      {
		/* Second or later nonexact match found.  */
		if (ambig_set != &ambig_fallback)
		  {
		    if (!print_errors)
		      /* Don't waste effort tracking the ambig set if
			 we're not going to print it anyway.  */
		      ambig_set = &ambig_fallback;
		    else if (!ambig_set)
		      {
			if (0)
			  ambig_set = 0;
			else
			  {
			    ambig_malloced = malloc (n_options);
			    /* Fall back to simpler diagnostic if
			       memory allocation fails.  */
			    ambig_set = (ambig_malloced ? ambig_malloced
					 : &ambig_fallback);
			  }

			if (ambig_set != &ambig_fallback)
			  {
			    memset (ambig_set, 0, n_options);
			    ambig_set[indfound] = 1;
			  }
		      }
		    if (ambig_set && ambig_set != &ambig_fallback)
		      ambig_set[option_index] = 1;
		  }
	      }
	  }

      if (ambig_set)
	{
	  if (print_errors)
	    {
	      if (ambig_set == &ambig_fallback)
		fprintf (stderr, gettext("%s: option '%s%s' is ambiguous\n"),
			 argv[0], prefix, nextchar);
	      else
		{
		  fprintf (stderr,
			   gettext("%s: option '%s%s' is ambiguous; possibilities:"),
			   argv[0], prefix, nextchar);

		  for (option_index = 0; option_index < n_options; option_index++)
		    if (ambig_set[option_index])
		      fprintf (stderr, " '%s%s'",
			       prefix, longopts[option_index].name);

		  /* This must use 'fprintf' even though it's only
		     printing a single character, so that it goes through
		     __fxprintf_nocancel when compiled as part of glibc.  */
		  fprintf (stderr, "\n");
		}
	    }
	  free (ambig_malloced);
	  nextchar += strlen (nextchar);
	  optind++;
	  optopt = 0;
	  return '?';
	}

      option_index = indfound;
    }

  if (pfound == NULL)
    {
      /* Can't find it as a long option.  If this is not getopt_long_only,
	 or the option starts with '--' or is not a valid short option,
	 then it's an error.  */
      if (!long_only || argv[optind][1] == '-'
	  || strchr (optstring, *nextchar) == NULL)
	{
	  if (print_errors)
	    fprintf (stderr, gettext("%s: unrecognized option '%s%s'\n"),
		     argv[0], prefix, nextchar);

	  nextchar = NULL;
	  optind++;
	  optopt = 0;
	  return '?';
	}

      /* Otherwise interpret it as a short option.  */
      return -1;
    }

  /* We have found a matching long option.  Consume it.  */
  optind++;
  nextchar = NULL;
  if (*nameend)
    {
      /* Don't test has_arg with >, because some C compilers don't
	 allow it to be used on enums.  */
      if (pfound->has_arg)
	optarg = nameend + 1;
      else
	{
	  if (print_errors)
	    fprintf (stderr,
		     gettext("%s: option '%s%s' doesn't allow an argument\n"),
		     argv[0], prefix, pfound->name);

	  optopt = pfound->val;
	  return '?';
	}
    }
  else if (pfound->has_arg == 1)
    {
      if (optind < argc)
	optarg = argv[optind++];
      else
	{
	  if (print_errors)
	    fprintf (stderr,
		     gettext("%s: option '%s%s' requires an argument\n"),
		     argv[0], prefix, pfound->name);

	  optopt = pfound->val;
	  return optstring[0] == ':' ? ':' : '?';
	}
    }

  if (longind != NULL)
    *longind = option_index;
  if (pfound->flag)
    {
      *(pfound->flag) = pfound->val;
      return 0;
    }
  return pfound->val;
}

/* Initialize internal data upon the first call to getopt.  */

static const char *
initialize (int argc,
		    char **argv, const char *optstring,
		    int unused)
{
  /* Start processing options with ARGV-element 1 (since ARGV-element 0
     is the program name); the sequence of previously skipped
     non-option ARGV-elements is empty.  */
  if (optind == 0)
    optind = 1;

  first_nonopt = last_nonopt = optind;
  nextchar = NULL;

  /* Determine how to handle the ordering of options and nonoptions.  */
  if (optstring[0] == '-')
    {
      ordering = RETURN_IN_ORDER;
      ++optstring;
    }
  else if (optstring[0] == '+')
    {
      ordering = REQUIRE_ORDER;
      ++optstring;
    }
  else if (0 || !!getenv ("POSIXLY_CORRECT"))
    ordering = REQUIRE_ORDER;
  else
    ordering = PERMUTE;

  initialized = 1;
  return optstring;
}

/* Scan elements of ARGV (whose length is ARGC) for option characters
   given in OPTSTRING.

   If an element of ARGV starts with '-', and is not exactly "-" or "--",
   then it is an option element.  The characters of this element
   (aside from the initial '-') are option characters.  If 'getopt'
   is called repeatedly, it returns successively each of the option characters
   from each of the option elements.

   If 'getopt' finds another option character, it returns that character,
   updating 'optind' and 'nextchar' so that the next call to 'getopt' can
   resume the scan with the following option character or ARGV-element.

   If there are no more option characters, 'getopt' returns -1.
   Then 'optind' is the index in ARGV of the first ARGV-element
   that is not an option.  (The ARGV-elements have been permuted
   so that those that are not options now come last.)

   OPTSTRING is a string containing the legitimate option characters.
   If an option character is seen that is not listed in OPTSTRING,
   return '?' after printing an error message.  If you set 'opterr' to
   zero, the error message is suppressed but we still return '?'.

   If a char in OPTSTRING is followed by a colon, that means it wants an arg,
   so the following text in the same ARGV-element, or the text of the following
   ARGV-element, is returned in 'optarg'.  Two colons mean an option that
   wants an optional arg; if there is text in the current ARGV-element,
   it is returned in 'optarg', otherwise 'optarg' is set to zero.

   If OPTSTRING starts with '-' or '+', it requests different methods of
   handling the non-option ARGV-elements.
   See the comments about RETURN_IN_ORDER and REQUIRE_ORDER, above.

   Long-named options begin with '--' instead of '-'.
   Their names may be abbreviated as long as the abbreviation is unique
   or is an exact match for some defined option.  If they have an
   argument, it follows the option name in the same ARGV-element, separated
   from the option name by a '=', or else the in next ARGV-element.
   When 'getopt' finds a long-named option, it returns 0 if that option's
   'flag' field is nonzero, the value of the option's 'val' field
   if the 'flag' field is zero.

   The elements of ARGV aren't really const, because we permute them.
   But we pretend they're const in the prototype to be compatible
   with other systems.

   LONGOPTS is a vector of 'struct option' terminated by an
   element containing a name which is zero.

   LONGIND returns the index in LONGOPT of the long-named option found.
   It is only valid when a long-named option has been found by the most
   recent call.

   If LONG_ONLY is nonzero, '-' as well as '--' can introduce
   long-named options.  */

int
gnu_getopt (int argc, char **argv, const char *optstring,
		    const struct option *longopts, int *longind,
		    int long_only)
{
  int print_errors = opterr;

  if (argc < 1)
    return -1;

  optarg = NULL;

  if (optind == 0 || !initialized)
    optstring = initialize (argc, argv, optstring, 0);
  else if (optstring[0] == '-' || optstring[0] == '+')
    optstring++;

  if (optstring[0] == ':')
    print_errors = 0;

  /* Test whether ARGV[optind] points to a non-option argument.  */
#define NONOPTION_P (argv[optind][0] != '-' || argv[optind][1] == '\0')

  if (nextchar == NULL || *nextchar == '\0')
    {
      /* Advance to the next ARGV-element.  */

      /* Give FIRST_NONOPT & LAST_NONOPT rational values if OPTIND has been
	 moved back by the user (who may also have changed the arguments).  */
      if (last_nonopt > optind)
	last_nonopt = optind;
      if (first_nonopt > optind)
	first_nonopt = optind;

      if (ordering == PERMUTE)
	{
	  /* If we have just processed some options following some non-options,
	     exchange them so that the options come first.  */

	  if (first_nonopt != last_nonopt
	      && last_nonopt != optind)
	    exchange (argv);
	  else if (last_nonopt != optind)
	    first_nonopt = optind;

	  /* Skip any additional non-options
	     and extend the range of non-options previously skipped.  */

	  while (optind < argc && NONOPTION_P)
	    optind++;
	  last_nonopt = optind;
	}

      /* The special ARGV-element '--' means premature end of options.
	 Skip it like a null option,
	 then exchange with previous non-options as if it were an option,
	 then skip everything else like a non-option.  */

      if (optind != argc && !strcmp (argv[optind], "--"))
	{
	  optind++;

	  if (first_nonopt != last_nonopt
	      && last_nonopt != optind)
	    exchange (argv);
	  else if (first_nonopt == last_nonopt)
	    first_nonopt = optind;
	  last_nonopt = argc;

	  optind = argc;
	}

      /* If we have done all the ARGV-elements, stop the scan
	 and back over any non-options that we skipped and permuted.  */

      if (optind == argc)
	{
	  /* Set the next-arg-index to point at the non-options
	     that we previously skipped, so the caller will digest them.  */
	  if (first_nonopt != last_nonopt)
	    optind = first_nonopt;
	  return -1;
	}

      /* If we have come to a non-option and did not permute it,
	 either stop the scan or describe it to the caller and pass it by.  */

      if (NONOPTION_P)
	{
	  if (ordering == REQUIRE_ORDER)
	    return -1;
	  optarg = argv[optind++];
	  return 1;
	}

      /* We have found another option-ARGV-element.
	 Check whether it might be a long option.  */
      if (longopts)
	{
	  if (argv[optind][1] == '-')
	    {
	      /* "--foo" is always a long option.  The special option
		 "--" was handled above.  */
	      nextchar = argv[optind] + 2;
	      return process_long_option (argc, argv, optstring, longopts,
					  longind, long_only,
					  print_errors, "--");
	    }

	  /* If long_only and the ARGV-element has the form "-f",
	     where f is a valid short option, don't consider it an
	     abbreviated form of a long option that starts with f.
	     Otherwise there would be no way to give the -f short
	     option.

	     On the other hand, if there's a long option "fubar" and
	     the ARGV-element is "-fu", do consider that an
	     abbreviation of the long option, just like "--fu", and
	     not "-f" with arg "u".

	     This distinction seems to be the most useful approach.  */
	  if (long_only && (argv[optind][2]
			    || !strchr (optstring, argv[optind][1])))
	    {
	      int code;
	      nextchar = argv[optind] + 1;
	      code = process_long_option (argc, argv, optstring, longopts,
					  longind, long_only,
					  print_errors, "-");
	      if (code != -1)
		return code;
	    }
	}

      /* It is not a long option.  Skip the initial punctuation.  */
      nextchar = argv[optind] + 1;
    }

  /* Look at and handle the next short option-character.  */

  {
    char c = *nextchar++;
    const char *temp = strchr (optstring, c);

    /* Increment 'optind' when we start to process its last character.  */
    if (*nextchar == '\0')
      ++optind;

    if (temp == NULL || c == ':' || c == ';')
      {
	if (print_errors)
	  fprintf (stderr, gettext("%s: invalid option -- '%c'\n"), argv[0], c);
	optopt = c;
	return '?';
      }

    /* Convenience. Treat POSIX -W foo same as long option --foo */
    if (temp[0] == 'W' && temp[1] == ';' && longopts != NULL)
      {
	/* This is an option that requires an argument.  */
	if (*nextchar != '\0')
	  optarg = nextchar;
	else if (optind == argc)
	  {
	    if (print_errors)
	      fprintf (stderr,
		       gettext("%s: option requires an argument -- '%c'\n"),
		       argv[0], c);

	    optopt = c;
	    if (optstring[0] == ':')
	      c = ':';
	    else
	      c = '?';
	    return c;
	  }
	else
	  optarg = argv[optind];

	nextchar = optarg;
	optarg = NULL;
	return process_long_option (argc, argv, optstring, longopts, longind,
				    0 /* long_only */, print_errors, "-W ");
      }
    if (temp[1] == ':')
      {
	if (temp[2] == ':')
	  {
	    /* This is an option that accepts an argument optionally.  */
	    if (*nextchar != '\0')
	      {
		optarg = nextchar;
		optind++;
	      }
	    else
	      optarg = NULL;
	    nextchar = NULL;
	  }
	else
	  {
	    /* This is an option that requires an argument.  */
	    if (*nextchar != '\0')
	      {
		optarg = nextchar;
		/* If we end this ARGV-element by taking the rest as an arg,
		   we must advance to the next element now.  */
		optind++;
	      }
	    else if (optind == argc)
	      {
		if (print_errors)
		  fprintf (stderr,
			   gettext("%s: option requires an argument -- '%c'\n"),
			   argv[0], c);

		optopt = c;
		if (optstring[0] == ':')
		  c = ':';
		else
		  c = '?';
	      }
	    else
	      /* We already incremented 'optind' once;
		 increment it again when taking next ARGV-elt as argument.  */
	      optarg = argv[optind++];
	    nextchar = NULL;
	  }
      }
    return c;
  }
}
