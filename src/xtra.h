/*
 * Assorted helper macros.
 *
 * Copyright © 2023-2024 Nick Bowler
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DX_XTRA_H_
#define DX_XTRA_H_

#define XTRA_PASTE(a, b) a ## b
#define XTRA_PASTE2(a, b) XTRA_PASTE(a, b)

#define XTRA_ARRAYSIZE(a) (sizeof (a) / sizeof (a)[0])

/*
 * XTRA_PACKED_LOPTS may be used to simplify construction of a 'struct option'
 * array from the packed format produced by gen-options.awk.
 *
 * The XTRA_PACKED_LOPTS2 is the same, except you can additionally specify
 * the name of the generated constant integer array containing the list of
 * packed values.
 *
 * Expanding this in a function will
 *
 *   (1) declare a constant array with static storage duration that
 *       contains the packed representation, and
 *   (2) declare an array of struct option of suitable length, with
 *       the given name, and
 *   (3) emit code to fill the struct option array from the packed
 *       representation.
 *
 * As this macro expands to a declaration followed by a statement, keep
 * in mind that C89 required all declarations to precede statements in
 * a block.  Thus, for portability to such compilers, expand this macro
 * after the last declaration (if any) but before the first statement.
 *
 * The appropriate uint_leastXX_t type is used based on the packing size.
 */
#define XTRA_PACKED_LOPTS(optname) \
	XTRA_PACKED_LOPTS2(optname, XTRA_PASTE(optname,_packed))

#define XTRA_PACKED_LOPTS2(optname, packname) \
	static const LOPT_PACK_TYPE packname[] = {LOPTS_PACKED_INITIALIZER}; \
	struct option optname[XTRA_ARRAYSIZE(packname)+1] = {0}; \
	do { \
		unsigned i; \
		for (i = 0; i < XTRA_ARRAYSIZE(packname); i++) \
			LOPT_UNPACK(optname[i], packname[i]); \
	} while (0)

#endif
