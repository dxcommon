/*
 * Portability wrapper to provide <getopt.h> declarations when the
 * system is missing them.
 *
 * Copyright © 2024 Nick Bowler
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * If either HAVE_GETOPT_LONG or HAVE_GETOPT_LONG_ONLY are defined to 1,
 * then the system <getopt.h> is used to get a declaration of struct
 * option and the global getopt variables.  If neither are defined,
 * then <getopt.h> is not included and replacements are provided.
 *
 * These replacements will interfere with POSIX getopt which uses the
 * same variable names so that cannot be used at the same time.  For
 * the same reason, do not use both getopt_long and getopt_long_only
 * in the same program, as some systems provide one but not both.
 */

#ifndef DX_GETOPT_H_
#define DX_GETOPT_H_

#if HAVE_GETOPT_LONG || HAVE_GETOPT_LONG_ONLY
#	include <getopt.h>
#else
struct option {
	const char *name;
	int has_arg, *flag, val;
};

extern int optind, optopt, opterr;
extern char *optarg;
#endif

/* Local fallback implementation */
int gnu_getopt(int, char **, const char *, const struct option *, int *, int);

#if !HAVE_GETOPT_LONG
#define getopt_long(argc, argv, sopts, lopts, idx) \
	gnu_getopt(argc, argv, sopts, lopts, idx, 0)
#endif

#if !HAVE_GETOPT_LONG_ONLY
#define getopt_long_only(argc, argv, sopts, lopts, idx) \
	gnu_getopt(argc, argv, sopts, lopts, idx, 1)
#endif

#endif
