# Copyright © 2011-2013, 2019, 2021-2023 Nick Bowler
#
# Automake fragment to generate a Gnulib config header to rewrite exported
# symbols.  This fragment relies on the Gnulib makefile postprocessing done by
# fix-gnulib.pl.  Furthermore, the following additions to configure.ac may be
# required:
#
#   * Add AM_PROC_CC_C_O to configure.ac
#   * Pass the desired symbol prefix to DX_GLSYM_PREFIX in configure.ac,
#     after including the fix-gnulib.pl output.
#
# License WTFPL2: Do What The Fuck You Want To Public License, version 2.
# This is free software: you are free to do what the fuck you want to.
# There is NO WARRANTY, to the extent permitted by law.

GLCONFIG = @GLSRC@/glconfig.h
GLCAT    = cat /dev/null

GLSYM_V   = $(GLSYM_V_@AM_V@)
GLSYM_V_  = $(GLSYM_V_@AM_DEFAULT_V@)
GLSYM_V_0 = @printf '  %$(DX_ALIGN_V)s %s\n' 'GLSYM   ' $<;

gnulib_headers += $(GLCONFIG)

# This suffix rule triggers symbol generation only on demand.  Dependencies are
# not tracked directly, so it must remain phony and thus not create the target.
.c.glsym:
	$(GLSYM_V) $(MKDIR_P) $(@D)/.syms
	$(AM_V_at) depfile=$(@D)/.syms/$(*F).deps \
		source=$< object=$(GLCONFIG) $(CCDEPMODE) \
		$(depcomp) $(COMPILE) -DNO_GLCONFIG \
		-c -o $(@D)/.syms/$(*F).o $<
	$(AM_V_at) $(SHELL) $(top_builddir)/exported.sh \
		$(@D)/.syms/$(*F).o > $(@D)/.syms/$(*F).sym

clean-local: clean-glconfig
clean-glconfig:
	@for sym in $(libgnu_la_SOURCES) $(EXTRA_libgnu_la_SOURCES); do \
		symdir=`expr "$$sym" : '\(.*/\)'`.syms; \
		if test -d "$$symdir"; then \
			echo "rm -rf $$symdir"; rm -rf "$$symdir"; \
		fi; \
	done
.PHONY: clean-glconfig

# Produce the list of all currently-enabled gnulib object files to assist with
# external build helpers.
GLCONFIG_OBJECTS = &1
glconfig-objects:
	@:; { \
	  for f in $(gnulib_objects); do echo "$$f"; done; \
	} >$(GLCONFIG_OBJECTS)
.PHONY: glconfig-objects

# The config header requires compilation of all gnulib object files via the
# .glsym rule above.  However, it cannot depend on those build products
# directly because they are phony, and would make this header never up-to-date.
#
# Thus, we use a recursive make call to regenerate the header, which avoids
# the need to list prerequisites.
#
# Since GNU make does not appear to allow the target of a suffix rule to be
# marked .PHONY, we also delete the .glsym files here just in case they were
# created for some reason (e.g., make -t).
$(GLCONFIG): $(gnulib_core_headers)
	-$(AM_V_at) rm -f $(gnulib_symfiles)
	$(AM_V_at) $(MAKE) $(AM_MAKEFLAGS) glconfig-gen
	$(AM_V_GEN) mv -f $@.tmp $@
CLEANFILES += $(GLCONFIG)

# The glconfig-gen target is intended only for use in recursive make
# invocations.
glconfig-gen: $(gnulib_symfiles)
	$(AM_V_at) $(MKDIR_P) @GLSRC@
	$(AM_V_at) depfiles=; symfiles=; \
	for sym in $(gnulib_symfiles); do \
	  symdir=`expr "$$sym" : '\(.*/\)'`; \
	  symfile=`expr "$$sym" : '.*/\(.*\)' || printf '%s\n' "$$sym"`; \
	  symbase=$$symdir.syms/`expr "$$symfile" : '\(.*\)\..*'`; \
	  test -f "$$symbase.deps" && \
	    depfiles="$$depfiles $$symbase.deps"; \
	    symfiles="$$symfiles $$symbase.sym"; \
	done; \
	$(glconfig_nodeps) \
	  || $(GLCAT) $$depfiles > @GLSRC@/$(DEPDIR)/glconfig.Ph || exit; \
	$(GLCAT) $$symfiles | sed 's/.*/#define & $(GLSYM_PREFIX)&/' \
	  >$(GLCONFIG).tmp
.PHONY: glconfig-gen

if AMDEP
glconfig_nodeps = false
@am__include@ @am__quote@@GLSRC@/$(DEPDIR)/glconfig.Ph@am__quote@@dx_include_marker@
else
glconfig_nodeps = true
endif

# Automake 1.16 and newer use make rules to generate the dependency stubs.
# we must hook those which is kind of annoying to do.  This should be harmless
# on previous versions which generate the stubs directly in config.status.
@dx_depfiles_target@: @GLSRC@/$(DEPDIR)/glconfig.Ph
@GLSRC@/$(DEPDIR)/glconfig.Ph:
	@$(MKDIR_P) $(@D)
	@echo '# dummy' >$@-t && mv -f $@-t $@
DISTCLEANFILES += @GLSRC@/$(DEPDIR)/glconfig.Ph
