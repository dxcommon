# Copyright © 2015, 2019-2024 Nick Bowler
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Automake fragment to hook up a basic Autotest test suite into the
# build.  It is expected that a testsuite.at file exists in $(srcdir).
# The testsuite will be output to $(builddir)/testsuite.
#
# The DX_AUTOMAKE_COMPAT macro must be expanded by configure.ac to
# provide necessary substitutions.
#
# You must define the AUTOTEST variable to the Autotest program (normally,
# this is autom4te -l autotest).  You must also define the HAVE_AUTOTEST
# Automake conditional which controls whether the testsuite-generating
# rules are enabled (that is, if Autotest is available and works).
#
# The DX_PROG_AUTOTEST_AM macro may be used to set all of the above.

check-local: check-autotest
check-autotest: testsuite
	$(SHELL) $? $(TESTSUITEFLAGS)
.PHONY: check-autotest

clean-local: clean-autotest
clean-autotest:
	for f in testsuite "$(srcdir)/testsuite"; do \
	  test ! -f "$$f" || { $(SHELL) "$$f" --clean; break; }; \
	done
.PHONY: clean-autotest

maintainer-clean-local: maintainer-clean-autotest
maintainer-clean-autotest:
	rm -f testsuite testsuite.deps package.m4
.PHONY: maintainerclean-autotest

package.m4: $(top_srcdir)/configure.ac
	$(AM_V_GEN) :; { \
	  printf 'm4_define([%s], [%s])\n' \
	    AT_PACKAGE_NAME      '$(PACKAGE_NAME)' \
	    AT_PACKAGE_TARNAME   '$(PACKAGE_TARNAME)' \
	    AT_PACKAGE_VERSION   '$(PACKAGE_VERSION)' \
	    AT_PACKAGE_STRING    '$(PACKAGE_STRING)' \
	    AT_PACKAGE_BUGREPORT '$(PACKAGE_BUGREPORT)' \
	    AT_PACKAGE_URL       '$(PACKAGE_URL)' \
	; } >$@.tmp
	$(AM_V_at) mv -f $@.tmp $@

DX_AUTOTEST_ERROR = @:; { \
	  printf 'ERROR: Autotest was not available at configure time.\n'; \
	  printf 'You should only need it if you modified the test suite.\n'; \
	  printf 'Autotest is part of autom4te, included in the GNU\n'; \
	  printf 'Autoconf package: <https://gnu.org/s/autoconf/>\n'; \
	} 1>&2; false

DX_AUTOTEST = $(AUTOTEST) -I$(srcdir) $(ATFLAGS)

testsuite: testsuite.at package.m4
if !HAVE_AUTOTEST
	$(DX_AUTOTEST_ERROR)
endif
	$(AM_V_GEN) $(DX_AUTOTEST) -p m4_include -o $@.tmp testsuite.at
	$(AM_V_at) $(DX_AUTOTEST) -t m4_include:'$$1' -o $@.deps.tmp \
	  testsuite.at
if AMDEP
	$(AM_V_at) exec 3<$@.deps.tmp 4>$(DEPDIR)/$(@F).P; \
	  while read f <&3; do printf '$@: %s\n%s:\n' "$$f" "$$f" >&4; done
endif
	$(AM_V_at) mv -f $@.deps.tmp $@.deps
	$(AM_V_at) mv -f $@.tmp $@
testsuite.deps: testsuite

dist-hook: dist-autotest-deps
dist-autotest-deps: testsuite.deps
	exec 3<$?; while read f <&3; do \
	  if test -f "$(distdir)/$$f"; then \
	    { diff "$(srcdir)/$$f" "$(distdir)/$$f" && continue; } || exit; \
	  fi; \
	  dir=`expr "$$f" : '\(.*\)/'`; \
	  $(MKDIR_P) "$(distdir)/$$dir" && \
	  chmod u+w "$(distdir)/$$dir" && \
	  cp -p "$(srcdir)/$$f" "$(distdir)/$$f" || exit; \
	done
.PHONY: dist-autotest-deps

EXTRA_DIST += testsuite testsuite.at testsuite.deps package.m4
DISTCLEANFILES += atconfig $(DEPDIR)/testsuite.P

@AMDEP_TRUE@@am__include@ @am__quote@$(DEPDIR)/testsuite.P@am__quote@@dx_include_marker@

# Automake 1.16 and newer use make rules to generate the dependency stubs.
# Use this opportunity to generate accurate prerequisites from distributed
# testsuite dependencies, needed so that make knows to rebuild the testsuite
# when source files are modified in a distribution tarball, or after a make
# distclean.
#
# With older Automake the stubs are generated directly in config.status.
# Since this rule is ignored things should still work but the build may
# miss modifications to the testsuite on the first build from a tarball.
@dx_depfiles_target@: ./$(DEPDIR)/testsuite.P
./$(DEPDIR)/testsuite.P:
	@$(MKDIR_P) $(@D)
	@if test -f $(builddir)/testsuite.deps; then \
	  exec 3<$(builddir)/testsuite.deps; \
	elif test -f $(srcdir)/testsuite.deps; then \
	  exec 3<$(srcdir)/testsuite.deps; \
	else \
	  echo '# dummy' >$@-t && exit; \
	fi; exec 4>$@-t; while read f <&3; do \
	  printf 'testsuite: %s\n%s:\n' "$$f" "$$f" >&4; \
	done
	@mv -f $@-t $@
