# Copyright © 2011-2012, 2023 Nick Bowler
#
# Automake fragment to distribute and install gettext .po/.mo files, intended
# for use in a non-recursive build setup.  This does not include rules to
# generate the .pot file.  Two variables must be defined:
#
#   POFILES: the complete list of .po files included with the package, and
#   MOFILES: the set of .mo files to be installed (a user-selected subset
#            of the available options).
#
# One way to define these variables is to use the DX_LINGUAS autoconf macro
# which normally accompanies this fragment.
#
# License WTFPL2: Do What The Fuck You Want To Public License, version 2.
# This is free software: you are free to do what the fuck you want to.
# There is NO WARRANTY, to the extent permitted by law.

ALL_MOFILES = $(POFILES:.po=.mo)
EXTRA_DIST += po/LINGUAS $(POFILES) $(ALL_MOFILES)
MAINTAINERCLEANFILES += $(ALL_MOFILES)

.po.mo:
	$(AM_V_at) $(MKDIR_P) ./$(@D)
	$(AM_V_GEN) $(MSGFMT) $(MSGFMTFLAGS) -c -o $@ $<

install-data-local: install-mofiles
install-mofiles: $(MOFILES)
	mos='$(MOFILES)'; for mo in $$mos; do \
	  save_IFS=$$IFS; IFS=/.; \
	  set x $$mo; shift; eval 'set x $$mo; lang=$$'$$#; \
	  IFS=$$save_IFS; \
	  d="$(DESTDIR)$(localedir)/$$lang/LC_MESSAGES"; \
	  echo "$(MKDIR_P) $$d"; $(MKDIR_P) "$$d" || exit; \
	  test -f "$$mo" && s="$$mo" || s="$(srcdir)/$$mo"; \
	  d="$$d/$(PACKAGE).mo"; \
	  echo "$(INSTALL_DATA) $$s $$d"; $(INSTALL_DATA) "$$s" "$$d" || exit; \
	done
.PHONY: install-mofiles

installdirs-local: installdirs-mofiles
installdirs-mofiles:
	d= mos='$(MOFILES)'; for mo in $$mos; do \
	  save_IFS=$$IFS; IFS=/.; \
	  set x $$mo; shift; eval 'set x $$mo; lang=$$'$$#; \
	  IFS=$$save_IFS; \
	  d="$(DESTDIR)$(localedir)/$$lang/LC_MESSAGES"; \
	  echo "$(MKDIR_P) $$d"; $(MKDIR_P) "$$d" || exit; \
	done;
.PHONY: installdirs-mofiles

uninstall-local: uninstall-mofiles
uninstall-mofiles:
	mos='$(ALL_MOFILES)'; for mo in $$mos; do \
	  save_IFS=$$IFS; IFS=/.; \
	  set x $$mo; shift; eval 'set x $$mo; lang=$$'$$#; \
	  IFS=$$save_IFS; \
	  d="$(DESTDIR)$(localedir)/$$lang/LC_MESSAGES/$(PACKAGE).mo"; \
	  echo "rm -f $$d"; rm -f "$$d"; :; \
	done
.PHONY: uninstall-mofiles
