dnl Copyright © 2021 Nick Bowler
dnl
dnl License WTFPL2: Do What The Fuck You Want To Public License, version 2.
dnl This is free software, you are free to do what the fuck you want to.
dnl There is NO WARRANTY, to the extent permitted by law.

dnl DX_PROG_BISON([min-version], [action-if-found], [action-if-not-found])
dnl
dnl Search PATH for a reasonably modern GNU Bison of at least the given
dnl minimum version, which may be empty to accept any version.  Here,
dnl "reasonably modern" means it supports the %code construct and (if
dnl a version is specified), the %require construct.
dnl
dnl If found, the BISON variable (which is substituted by AC_SUBST) is
dnl set with the result, and action-if-found is executed.  Otherwise,
dnl action-if-not-found is executed if nonempty, else configure will
dnl exit with a fatal error.
AC_DEFUN([DX_PROG_BISON],
[AC_ARG_VAR([BISON], [GNU Bison command])dnl
AC_ARG_VAR([BISONFLAGS], [Additional options to pass to Bison])dnl
cat >conftest.y <<'EOF'
m4_ifnblank([$1], [%require "$1"
])%code top {
/* SUPPORTS_CODE_TOP_WITNESS */
}
%code requires {
/* SUPPORTS_CODE_REQUIRES_WITNESS */
}
%{
int yylex(void) { return 0; }
void yyerror(const char *msg) { }
%}
%%
input:
%%
int main(void) { return yyparse(); }
EOF
AC_CACHE_CHECK([for bison], [ac_cv_path_BISON],
[dx_cv_bison_found=:
AC_PATH_PROGS_FEATURE_CHECK([BISON], [bison],
[_DX_BISON_DO_TEST([$ac_path_BISON],
[DX_BASENAME([bison_relcmd], ["$ac_path_BISON"])
bison_bypath=`{ command -v "$bison_relcmd"; } 2>/dev/null` #''
ac_cv_path_BISON=$ac_path_BISON
test x"$bison_bypath" = x"$ac_path_BISON" && ac_cv_path_BISON=$bison_relcmd
ac_path_BISON_found=:
dx_cv_bison_works=yes])], [ac_cv_path_BISON=no dx_cv_bison_found=false])])
AS_IF([$dx_cv_bison_found],
[BISON=$ac_cv_path_BISON
AC_CACHE_CHECK([whether $BISON works and is m4_ifnblank([$1],
    [at least version $1], [reasonably modern])],
  [dx_cv_bison_works],
  [_DX_BISON_DO_TEST([$BISON], [dx_cv_bison_works=yes], [dx_cv_bison_works=no])])])
AS_IF([test x"$dx_cv_bison_works" = x"yes"],
  [$2], [m4_default([$3], [AC_MSG_FAILURE(
[GNU bison m4_ifnblank([$1], [version at least $1 ])is required.

The latest version may be found at <https://www.gnu.org/software/bison/>.
])])])
rm -f conftest.tab.[[ch]] conftest.y y.tab.[[ch]]
])

AC_DEFUN([_DX_BISON_DO_TEST], [rm -f conftest.tab.[[ch]] y.tab.[[ch]]
AS_IF([DX_RUN_LOG([$1 --file-prefix=conftest --defines conftest.y])],
  [AC_LINK_IFELSE([AC_LANG_SOURCE([[#include "conftest.tab.c"]])],
    [AS_IF([DX_RUN_LOG([grep SUPPORTS_CODE_TOP_WITNESS conftest.tab.c]) &&
            DX_RUN_LOG([grep SUPPORTS_CODE_REQUIRES_WITNESS conftest.tab.h])],
      [$2], [$3])], [$3])], [$3])])
