# Copyright © 2023 Nick Bowler
#
# License WTFPL2: Do What The Fuck You Want To Public License, version 2.
# This is free software: you are free to do what the fuck you want to.
# There is NO WARRANTY, to the extent permitted by law.

# DX_C_FOR_DECLARATIONS
#
# Probe whether for-loop declarations are accepted by the C compiler,
# as some pre-C99 implementations do have them.  If supported, the cache
# variable dx_cv_have_for_decls is set to "yes" and the HAVE_FOR_DECLS
# macro is defined to 1.
#
# We skip the test (and define the macro unconditionally) if Autoconf
# has previously determined that the C compiler supports C99 or a newer
# standard, since the C99 test program checks this.  However, autoconf-2.70
# has a broken check which indicates C99 compatibility for C89 compilers,
# so on this version we still do the check if C99 support is indicated.
#
# Annoyingly, autoconf-2.71 removed the assignment of ac_cv_prog_cc_c99,
# even though this was actual documented behaviour...
AC_DEFUN([DX_C_FOR_DECLARATIONS],
[AS_CASE([${ac_cv_prog_cc_c99-no}/${ac_prog_cc_stdc-no}],
  [m4_do([no/no|*/c89],
    [_DX_IF_AC270_C99_BUG([|*/c99])])],
  [AC_CACHE_CHECK([if $CC supports for-loop declarations],
    [dx_cv_have_for_decls],
    [AC_COMPILE_IFELSE([AC_LANG_PROGRAM([], [for (int i; i = 1;) return i;])],
    [dx_cv_have_for_decls=yes], [dx_cv_have_for_decls=no])])],
  [dx_cv_have_for_decls=yes])
AS_CASE([$dx_cv_have_for_decls],
  [yes], [AC_DEFINE([HAVE_FOR_DECLS], [1],
    [Define to 1 if the C compiler supports for-loop declarations.])])])

m4_define([_DX_IF_AC270_C99_BUG], [m4_ifdef([_AC_PROG_CC_C99],
  [m4_bmatch(m4_defn([_AC_PROG_CC_C99]),
    [_AC_C_STD_TRY.\[c99\], *\[ac_c_conftest_c89_program\]],
    [$1])])])
