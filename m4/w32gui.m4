dnl Copyright © 2022 Nick Bowler
dnl
dnl License WTFPL2: Do What The Fuck You Want To Public License, version 2.
dnl This is free software: you are free to do what the fuck you want to.
dnl There is NO WARRANTY, to the extent permitted by law.

dnl DX_W32_GUI
dnl
dnl Determine the C linker options needed to build a Windows graphical
dnl application (i.e., for the "windows" subsystem).  The result is saved
dnl in the cache variable dx_cv_w32_gui_flags.
dnl
dnl If no suitable flags could be determined, the cache variable dx_cv_w32_gui
dnl is set to "unknown" and dx_cv_w32_gui_flags is set to the empty string.

AC_DEFUN([DX_W32_GUI], [AC_REQUIRE([AC_PROG_AWK])dnl
AC_CACHE_CHECK([for $CC option to target W32 GUI], [dx_cv_w32_gui],
[_dx_check_pe_subsys () {
  $AWK -f m4_do([m4_pushdef([m4_include], [$][1])],
                [m4_include(DX_BASEDIR[/scripts/pe-subsys.awk])],
                [m4_popdef([m4_include])]) "conftest$EXEEXT"
}
dx_cv_w32_gui=unknown _dx_save_LDFLAGS=$LDFLAGS
for dx_cv_w32_gui_flags in '' -mwindows; do
LDFLAGS="$_dx_save_LDFLAGS $dx_cv_w32_gui_flags"
AC_LINK_IFELSE([AC_LANG_PROGRAM],
[AS_CASE([`_dx_check_pe_subsys`],
[gui], [dx_cv_w32_gui=${dx_cv_w32_gui_flags:-none needed}; break]
)])
done
test x"$dx_cv_w32_gui" != x"unknown" || dx_cv_w32_gui_flags=
LDFLAGS=$_dx_save_LDFLAGS])])
