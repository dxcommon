dnl Copyright © 2022 Nick Bowler
dnl
dnl License WTFPL2: Do What The Fuck You Want To Public License, version 2.
dnl This is free software: you are free to do what the fuck you want to.
dnl There is NO WARRANTY, to the extent permitted by law.

AC_DEFUN([DX_LIB_CURSES],
[AC_BEFORE([$0], [_DX_CURSES_LINKTEST])dnl
DX_LIB_SETUP([curses])dnl
DX_LIB_PKGCONFIG_FLAGS([ncurses])
AC_MSG_CHECKING([for curses])
DX_LIB_SEARCH_LINK([curses],
  [_DX_LIB_CURSES_TEST],
  [ [[$CURSES_CFLAGS], [$CURSES_LIBS]],
    [[$dx_cv_ncurses_pkg_cflags], [$dx_cv_ncurses_pkg_libs],
      [test x"$dx_cv_ncurses_pkg_found" = x"yes"]],
    [[], [-lcurses]] ])
AS_IF([test x"$dx_cv_curses_lib_found" = x"yes"], [$1],
  [m4_default([$2], [AC_MSG_FAILURE(
[the curses library is required.  A suitable version
may be found at <https://invisible-island.net/ncurses/>.
m4_newline([DX_LIB_USERFLAG_BLURB([curses])])
m4_newline([DX_LIB_PKGCONFIG_BLURB([ncurses])])
])])])])

m4_define([_DX_LIB_CURSES_TEST], [AC_LANG_PROGRAM(
[#include <curses.h>
], [initscr(); curs_set(0); endwin();])])
