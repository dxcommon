# Macro to find an working autotest installation.
#
# Copyright © 2015, 2021, 2023-2024 Nick Bowler
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# DX_PROG_AUTOTEST
#
# Determine a command which works to compile Autotest testsuites.
#
# If found, the AUTOTEST variable (which is substituted by AC_SUBST)
# is set with the result, and the cache variable dx_cv_autotest_works
# is set to "yes".  Otherwise, dx_cv_autotest_works is set to "no".
AC_DEFUN_ONCE([DX_PROG_AUTOTEST],
[AC_ARG_VAR([AUTOTEST], [command to compile Autotest testsuites])dnl
_DX_PROG_AUTOTEST
AS_IF([$dx_cv_autotest_found],
  [AC_CACHE_CHECK([whether $AUTOTEST works],
    [dx_cv_autotest_works], [_DX_PROG_AUTOTEST_CHECK])])])

# DX_PROG_AUTOTEST_AM
#
# Like DX_PROG_AUTOTEST, but also define various additional things
# needed to use the autotest.mk snippet.
AC_DEFUN_ONCE([DX_PROG_AUTOTEST_AM],
[AC_REQUIRE([DX_PROG_AUTOTEST])AC_REQUIRE([DX_AUTOMAKE_COMPAT])dnl
AM_CONDITIONAL([HAVE_AUTOTEST], [test x"$dx_cv_autotest_works" = xyes])])

m4_define([_DX_PROG_AUTOTEST],
[AC_CACHE_CHECK([for autotest], [dx_cv_autotest_cmd],
[dx_cv_autotest_found=false
AS_VAR_SET_IF([AUTOTEST],
  [dx_cv_autotest_cmd=$AUTOTEST dx_cv_autotest_found=:],
[for dx_cv_autotest_cmd
in ${AUTOM4TE:+"$AUTOM4TE -l autotest"} "autom4te -l autotest"
do
  _DX_PROG_AUTOTEST_CHECK([dx_cv_autotest_found=:; break])
done])
$dx_cv_autotest_found || dx_cv_autotest_cmd=no])
$dx_cv_autotest_found && AUTOTEST=$dx_cv_autotest_cmd])

m4_define([_DX_PROG_AUTOTEST_CHECK],
[cat >conftest.at <<'EOF'
[m4@&t@_define([AT_PACKAGE_STRING], [mypackage])]
[m4@&t@_define([AT_PACKAGE_BUGREPORT], [nobody])]
AT_INIT
AT_SETUP([test])
AT_CHECK([:])
AT_CLEANUP
EOF
dx_cv_autotest_works=no
AS_IF(
  [DX_RUN_LOG([$dx_cv_autotest_cmd conftest.at >conftest.sh &&
    $SHELL conftest.sh --version >conftest.out &&
      grep '[(]mypackage[)]' conftest.out >/dev/null 2>&1])],
  [dx_cv_autotest_works=yes])
rm -f conftest.at conftest.sh conftest.out[]m4_ifnblank([$1], [
AS_CASE([$dx_cv_autotest_works], [yes], [$1])])])
