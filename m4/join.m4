dnl Copyright © 2022 Nick Bowler
dnl
dnl License WTFPL2: Do What The Fuck You Want To Public License, version 2.
dnl This is free software: you are free to do what the fuck you want to.
dnl There is NO WARRANTY, to the extent permitted by law.

dnl DX_PROG_JOIN
dnl
dnl Search PATH for standard POSIX "join" utility.  If found, the JOIN
dnl variable (which is substituted by AC_SUBST) is set to the result and
dnl the cache variable dx_cv_join_works is set to "yes".  Otherwise,
dnl dx_cv_join_works is set to "no" and JOIN is set to an incomplete
dnl awk-based implementation.
dnl
dnl The awk replacement currently has the following limitations:
dnl
dnl   - Only the -v and -a options are supported.
dnl
dnl   - Due to limitations of some awk implementations, a filename of "-"
dnl     to mean standard input should be avoided for the second file name.
dnl     Nevertheless, using "-" for the first filename should be portable.

AC_DEFUN([DX_PROG_JOIN], [AC_PREREQ([2.62])dnl
AC_REQUIRE([AC_PROG_AWK])dnl
AC_CACHE_CHECK([for join], [ac_cv_path_JOIN],
[dx_cv_join_found=:
AC_PATH_PROGS_FEATURE_CHECK([JOIN], [join],
[_DX_JOIN_DO_TEST([$ac_path_JOIN],
[DX_BASENAME([join_relcmd], ["$ac_path_JOIN"])
join_bypath=`{ command -v "$join_relcmd"; } 2>/dev/null` #''
ac_cv_path_JOIN=$ac_path_JOIN
test x"$join_bypath" = x"$ac_path_JOIN" && ac_cv_path_JOIN=$join_relcmd
ac_path_JOIN_found=:
dx_cv_join_works=yes])], [ac_cv_path_JOIN=no dx_cv_join_found=false])])
AS_IF([$dx_cv_join_found],
[JOIN=$ac_cv_path_JOIN
AC_SUBST([JOIN])dnl
AC_CACHE_CHECK([whether $JOIN works], [dx_cv_join_works],
[_DX_JOIN_DO_TEST([$JOIN], [dx_cv_join_works=yes], [dx_cv_join_works=no])])])
rm -f conftest.a conftest.b conftest.c
AS_IF([test x"$dx_cv_join_works" != x"yes"],
[JOIN="$AWK -f m4_do(
  [m4_pushdef([m4_include], [$][1])],
  [m4_include(DX_BASEDIR[/scripts/join.awk])],
  [m4_popdef([m4_include])]) --"])])

AC_DEFUN([_DX_JOIN_DO_TEST],
[AS_IF([test ! -f conftest.b],
[cat >conftest.a <<'EOF'
1 a
3 a1 x
3 a2 x
5 a
7 a
9 a1 x
9 a2 x
EOF
cat >conftest.b <<'EOF'
2 b
2 b
3 b y
4 b
6 b
7 b1 y
7 b2 y
8 b
9 b1 y
9 b2 y
EOF])
AS_IF([$1 conftest.a conftest.b >conftest.c 2>&AS_MESSAGE_LOG_FD],
[if diff - conftest.c >/dev/null 2>&1 <<'EOF'
3 a1 x b y
3 a2 x b y
7 a b1 y
7 a b2 y
9 a1 x b1 y
9 a1 x b2 y
9 a2 x b1 y
9 a2 x b2 y
EOF
then :
$2
else :
$3
fi], [$3])])
