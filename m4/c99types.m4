# Copyright © 2024 Nick Bowler
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

AC_DEFUN([DX_C99TYPES],
[AC_REQUIRE([AC_HEADER_STDBOOL])dnl
AC_REQUIRE([AC_TYPE_UNSIGNED_LONG_LONG_INT])dnl
AS_IF([test x"$ac_cv_type_unsigned_long_long_int" != x"yes"],
  [AC_CHECK_TYPES([unsigned __int64], [], [],
[/* avoid false positive result on VAX C */
unsigned __int64 foo = -1;])])])
