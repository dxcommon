# Copyright © 2010, 2014, 2024 Nick Bowler
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# DX_LIB_GTK2([min-version], [action-if-ok], [action-if-failed])
#
# Search for a working GTK+-2 installation.  If such a library is found,
# appropriate compiler and linker flags are stored in GTK_CFLAGS and
# GTK_LIBS, respectively, and the action-if-ok is performed (may be empty).
# If no suitable library is found, then action-if-failed is performed if
# non-empty, oherwise configure will stop with a fatal error.
AC_DEFUN([DX_LIB_GTK2], [dnl
DX_LIB_SETUP([gtk], [GTK+])dnl
DX_LIB_PKGCONFIG_FLAGS([gtk], [gtk+-2.0])
AC_MSG_CHECKING([for GTK+ at least version m4_default([$1], [2.0])])
DX_LIB_SEARCH_LINK([gtk],
  [m4_apply([_DX_LIB_GTK2_TEST], m4_split([$1], [\.]))],
  [ [[$GTK_CFLAGS], [$GTK_LIBS]],
    [[$dx_cv_gtk_pkg_cflags], [$dx_cv_gtk_pkg_libs],
      [test x"$dx_cv_gtk_pkg_found" = x"yes"]] ])
AS_IF([test x"$dx_cv_gtk_lib_found" = x"yes"], [$2],
  [m4_default([$3], [AC_MSG_FAILURE(
[GTK+ version m4_default([$1], [2.0]) or newer is required.  The latest
version may be found at <ftp://ftk.gtk.org/>.
m4_newline([DX_LIB_USERFLAG_BLURB([gtk], [GTK+])])
m4_newline([DX_LIB_PKGCONFIG_BLURB([gtk], [GTK+])])
])])])])

# Internal test program for GTK+.  Check that the version numbers in the
# header are sufficient and that some important functions are defined.
m4_define([_DX_LIB_GTK2_TEST], [AC_LANG_PROGRAM(
[#include <gtk/gtk.h>
@%:@if !GTK_CHECK_VERSION(m4_do(
	[m4_default([$1], [2]), ],
	[m4_default([$2], [0]), ],
	[m4_default([$3], [0]))])
DX_LIB_COMPILE_ERROR([insufficient GTK version])
#endif
],
[int argc = 0;
char *arg = NULL;
char **argv = &arg;
gtk_init(&argc, &argv);
gtk_main();
])])
