dnl Copyright © 2021, 2023 Nick Bowler
dnl
dnl License WTFPL2: Do What The Fuck You Want To Public License, version 2.
dnl This is free software: you are free to do what the fuck you want to.
dnl There is NO WARRANTY, to the extent permitted by law.

dnl DX_CHECK_SYSLOG
dnl
dnl Expands AC_CHECK_HEADERS([syslog.h])
dnl
dnl Then, if syslog.h is available, checks that a program can be linked
dnl against the syslog family of functions.
dnl
dnl If successful, the cache variable dx_cv_have_syslog is set to "yes".
dnl Otherwise, dx_cv_have_syslog is set to "no".
AC_DEFUN([DX_CHECK_SYSLOG],
[AC_CHECK_HEADERS([syslog.h], [], [], [@&t@])
AS_IF([test x"$ac_cv_header_syslog_h" = x"yes"],
  [AC_CACHE_CHECK([for syslog], [dx_cv_have_syslog],
    [AC_LINK_IFELSE([AC_LANG_PROGRAM([#include <syslog.h>],
[openlog("conftest", LOG_PID, LOG_USER);
syslog(LOG_ERR, "%s", "hello");
closelog();])], [dx_cv_have_syslog=yes], [dx_cv_have_syslog=no])])])
AS_IF([test x"$dx_cv_have_syslog" = x"yes"],
  [AC_DEFINE([HAVE_SYSLOG], [1],
    [Define to 1 if syslog et al. are available.])])])
