dnl This m4 snippet does nothing; its existence is used to identify the
dnl dxcommon checkout location.  We define a dummy macro so that aclocal
dnl can pick it up.
AC_DEFUN([_DX_STAMP_DUMMY])dnl
