dnl Copyright © 2018 Nick Bowler
dnl
dnl License WTFPL2: Do What The Fuck You Want To Public License, version 2.
dnl This is free software: you are free to do what the fuck you want to.
dnl There is NO WARRANTY, to the extent permitted by law.

dnl DX_LIB_LIBGERBV([min-version], [action-if-ok], [action-if-failed])
dnl
dnl Search for a working libgerbv installation.  If such a library is found,
dnl appropriate compiler and linker flags are stored in LIBGERBV_CFLAGS and
dnl LIBGERBV_LIBS, respectively, and the action-if-ok is performed (may be
dnl empty).  If no suitable library is found, then action-if-failed is
dnl performed if non-empty, otherwise configure will stop with a fatal error.
AC_DEFUN([DX_LIB_LIBGERBV],
[DX_LIB_SETUP([libgerbv])DX_LIB_PKGCONFIG_FLAGS([libgerbv])
AC_MSG_CHECKING([for libgerbv])
DX_LIB_SEARCH_LINK([libgerbv],
  [m4_apply([_DX_LIB_LIBGERBV_TEST], m4_split([$1], [\.]))],
  [ [[$LIBGERBV_CFLAGS], [$LIBGERBV_LIBS]],
    [[$dx_cv_libgerbv_pkg_cflags], [$dx_cv_libgerbv_pkg_libs],
      [test x"$dx_cv_libgerbv_pkg_found" = x"yes"]],
    [[], [-lgerbv]] ])
AS_IF([test x"$dx_cv_libgerbv_lib_found" = x"yes"], [$2],
  [m4_default([$3], [AC_MSG_FAILURE([dnl
libgerbv is required.  The latest
version may be found at <http://gerbv.geda-project.org/>.
m4_newline([DX_LIB_USERFLAG_BLURB([libgerbv])])
m4_newline([DX_LIB_PKGCONFIG_BLURB([libgerbv])])
])])])])

dnl Internal test program for libgerbv.  The version test is currently
dnl unimplemented.
m4_define([_DX_LIB_LIBGERBV_TEST], [AC_LANG_PROGRAM([dnl
#include <gerbv.h>
], [dnl
gerbv_project_t *gp = gerbv_create_project();
gerbv_destroy_project(gp);
])])
