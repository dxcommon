# Copyright © 2009-2010, 2014, 2024 Nick Bowler
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# DX_LIB_LIBPNG([min-version], [action-if-ok], [acton-if-failed])
#
# Search for a suitable libpng.  If min-version is non-empty, the test will
# attempt to reject a libpng installation which is older than the specified
# version number (as determined by the libpng header file).
#
# If a working library is found, appropriate compiler and linker flags
# are stored in LIBPNG_CFLAGS and LIBPNG_LIBS, respectively, and the
# action-if-ok is performed (may be empty).  If no such library is found,
# then action-if-failed is performed if non-empty; otherwise configure will
# stop with a fatal error.

AC_DEFUN([DX_LIB_LIBPNG], [dnl
DX_LIB_SETUP([libpng])dnl
DX_LIB_PKGCONFIG_FLAGS([libpng])
AC_MSG_CHECKING([for libpng[]m4_ifnblank([$1], [ at least version $1])])
DX_LIB_SEARCH_LINK([libpng],
  [m4_apply([_DX_LIB_LIBPNG_TEST], m4_split([$1], [\.]))],
  [ [[$LIBPNG_CFLAGS], [$LIBPNG_LIBS]],
    [[$dx_cv_libpng_pkg_cflags], [$dx_cv_libpng_pkg_libs],
      [test x"$dx_cv_libpng_pkg_found" = x"yes"]],
    [[], [-lpng]] ])
AS_IF([test x"$dx_cv_libpng_lib_found" = x"yes"], [$2],
  [m4_default([$3], [AC_MSG_FAILURE(
[libpng[]m4_ifnblank([$1], [ version $1 or newer]) is required.  The latest
version may be found at <http://libpng.org/pub/png/libpng.html>.
m4_newline([DX_LIB_USERFLAG_BLURB([libpng])])
m4_newline([DX_LIB_PKGCONFIG_BLURB([libpng])])
])])])])

# Internal test program for libpng.  Check that the version numbers in the
# header are sufficient and that some important functions are defined.
m4_define([_DX_LIB_LIBPNG_TEST], [AC_LANG_PROGRAM([dnl
#include <png.h>
m4_ifnblank([$1], [dnl
#if PNG_LIBPNG_VER_MAJOR < $1
DX_LIB_COMPILE_ERROR([major version insufficient])
m4_ifnblank([$2], [dnl
#elif PNG_LIBPNG_VER_MAJOR == $1
#  if PNG_LIBPNG_VER_MINOR < $2
DX_LIB_COMPILE_ERROR([minor version insufficient])
m4_ifnblank([$3], [dnl
#  elif PNG_LIBPNG_VER_MINOR == $2
#    if PNG_LIBPNG_VER_RELEASE < $3
DX_LIB_COMPILE_ERROR([release version insufficient])
#    endif
])dnl
#  endif
])dnl
#endif
])], [dnl
png_structp png = png_create_read_struct("", 0, 0, 0);
png_infop info = png_create_info_struct(png);
png_destroy_read_struct(&png, &info, 0);
])])
