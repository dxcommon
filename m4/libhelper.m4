# Copyright © 2014, 2019, 2024 Nick Bowler
#
# Helper macros for implementing library tests.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# DX_LIB_SETUP(env-base, [human-name])
#
# Helper to setup a library test.  Defines two user variables FOO_CFLAGS
# and FOO_LIBS, where FOO is replaced with env-base in uppercase.  The
# human-name is the name of the library used in help text, by default the
# same as env-base.
AC_DEFUN([DX_LIB_SETUP],
[AC_ARG_VAR(AS_TR_SH([m4_toupper([$1_CFLAGS])]),
  [C compiler flags for ]m4_default([$2], [$1]))dnl
AC_ARG_VAR(AS_TR_SH([m4_toupper([$1_LIBS])]),
  [Linker flags for ]m4_default([$2], [$1]))])

# DX_LIB_PKGCONFIG_FLAGS(env-base, [library-name])
#
# Helper for querying a library's CFLAGS and LIBS values from the
# pkg-config database.  The results are stored in the cache variables
# dx_cv_foo_pkg_cflags and dx_cv_foo_pkg_libs, where foo is replaced with
# env-base in lowercase.  If not specified, the library name defaults to
# env-base.
AC_DEFUN([DX_LIB_PKGCONFIG_FLAGS],
[_DX_LIB_PKGCONFIG_FLAGS(AS_TR_SH([m4_tolower([$1])]),
  m4_default_quoted([$2], [$1]))])

AC_DEFUN([_DX_LIB_PKGCONFIG_FLAGS],
[AS_IF([test ${PKG_CONFIG:+y}],
  [AC_CACHE_CHECK([pkg-config database for $2], [dx_cv_$1_pkg_found],
    [DX_PKG_CONFIG([dx_cv_$1_pkg_cflags], [--cflags "$2"],
      [DX_PKG_CONFIG([dx_cv_$1_pkg_libs], [--libs "$2"],
        [dx_cv_$1_pkg_found=yes], [dx_cv_$1_pkg_found=no])],
      [dx_cv_$1_pkg_found=no])])])])

# DX_LIB_SEARCH(env-base, test, test-matrix)
#
# Helper to search for the correct compiler/linker flags for a particular
# library.  The test-matrix is an ordered, quoted list of quoted lists.
# Each entry in the test matrix has the following form:
#
#    [cflags, libs, shell-condition]
#
# For each item, the specified cflags and libs are temporarily appended to
# CFLAGS and LIBS, respectively, then the given test is run.  The test
# argument must be an m4 macro which takes one argument (action-if-ok).
# This macro shall expand to shell code which executes action-if-ok if and
# only if the test was successful.  The test macro is expanded only once.
#
# If a test is successful, dx_cv_foo_lib_found is set to "yes", where foo is
# replaced with env-base in lowercase.  Subsequent tests are not tried.
# The cflags and libs values from a successful test are stored in FOO_CFLAGS
# and FOO_LIBS, respectively, where FOO is replaced with env-base in
# uppercase.  These values are AC_SUBST-ed.  If no test was successful,
# dx_cv_foo_lib_found is set to "no".
#
# The result ("yes" or "no") is printed so this macro should be preceded
# by a call to AC_MSG_CHECKING.
AC_DEFUN([DX_LIB_SEARCH],
[_DX_LIB_SEARCH(m4_toupper([$1]),
  AS_TR_SH([m4_tolower([dx_cv_$1_lib])]),
  AS_TR_SH([m4_tolower([dx_fn_$1_lib])]),
  m4_shift($@))])

# _DX_LIB_SEARCH(cache-base, function, env-base, test, test-matrix)
m4_define([_DX_LIB_SEARCH], [AC_CACHE_VAL([$2_found],
[m4_divert_push([INIT_PREPARE])dnl
$3 () {
  CFLAGS="$_dx_save_cflags $[1]" LIBS="$_dx_save_libs $[2]"
  $4([$2_found=yes $2_cflags=$][1 $2_libs=$][2])
  test x"$$2_found" = x"yes"
}
m4_divert_pop([INIT_PREPARE])dnl
_dx_save_cflags=$CFLAGS _dx_save_libs=$LIBS
for x in x; do
m4_map_args_sep([_DX_LIB_SEARCH_TEST([$3],m4_unquote(], [))
], [], $5)done
DX_VAR_NORMALIZE_SPACE([$2_cflags])
DX_VAR_NORMALIZE_SPACE([$2_libs])
CFLAGS=$_dx_save_cflags LIBS=$_dx_save_LIBS
])
AC_MSG_RESULT([$$2_found])
AS_CASE([$$2_found], [yes],
  [AC_SUBST([$1_CFLAGS], [$$2_cflags]) AC_SUBST([$1_LIBS], [$$2_libs])])])

m4_define([_DX_LIB_SEARCH_TEST],
[  m4_ifnblank([$4], [$4 &&
    ])$1 "$2" "$3" && break])

# DX_LIB_SEARCH_LINK(env-base, test-program, test-matrix)
#
# Convenience wrapper around DX_LIB_SEARCH which implements the test
# function by attempting to linking the provided test program.
AC_DEFUN([DX_LIB_SEARCH_LINK],
  [DX_LIB_SEARCH([$1], [m4_curry([AC_LINK_IFELSE], [$2])], [$3])])

# DX_LIB_USERFLAG_BLURB(env-base, [human-name])
#
# Expand to text explaining the FOO_CFLAGS and FOO_LIBS environment
# variables, used in error messages, where FOO is replaced with env-base
# in uppercase.  The human-name is the name of the library, by default the
# same as env-base.
AC_DEFUN([DX_LIB_USERFLAG_BLURB],
  [_DX_LIB_USERFLAG_BLURB(m4_toupper([$1]), m4_default_quoted([$2], [$1]))])

# Internal helper macro for the above.
AC_DEFUN([_DX_LIB_USERFLAG_BLURB],
[If $2 is installed but was not detected by this configure script,
consider adjusting $1_CFLAGS and/or $1_LIBS as necessary.])

# DX_LIB_USERFLAG_BLURB(env-base, [human-name])
#
# Expand to text explaining the PKG_CONFIG_PATH environment variable,
# used in error messages.  The human-name is the name of the library, by
# default the same as env-base.
AC_DEFUN([DX_LIB_PKGCONFIG_BLURB],
  [_DX_LIB_PKGCONFIG_BLURB([$1], m4_default_quoted([$2], [$1]))])

dnl Internal helper macro for the above.
AC_DEFUN([_DX_LIB_PKGCONFIG_BLURB],
[If pkg-config is installed, it may help to adjust PKG_CONFIG_PATH if
$2 is installed in a non-standard prefix.])

# DX_LIB_COMPILE_ERROR([text])
#
# Expand to C code which should fail compilation.  The given text should
# appear in any error message from the compiler, and should not contain
# any newlines.
AC_DEFUN([DX_LIB_COMPILE_ERROR],
[#error $1
static int AS_TR_SH([$1])[[-1]]])
