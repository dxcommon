# Copyright © 2024 Nick Bowler
#
# Monkey patches for libtool.
#
# License WTFPL2: Do What The Fuck You Want To Public License, version 2.
# This is free software: you are free to do what the fuck you want to.
# There is NO WARRANTY, to the extent permitted by law.

# DX_PATCH_LIBTOOL
#
# Apply all the patches described below.  Should be expanded before LT_INIT.
AC_DEFUN([DX_PATCH_LIBTOOL],
[AC_BEFORE([$0], [LT_INIT])dnl
m4_foreach_w([patch], m4_defn([_DX_LT_PATCHES]),
  [m4_indir([_DX_LT_PATCH(]m4_defn([patch])[)])])])

m4_define([_DX_LT_PATCHES])
m4_define([_DX_LT_PATCH],
  [m4_define([_DX_LT_PATCH($1)], [$2])m4_define([_DX_LT_PATCHES],
    m4_defn([_DX_LT_PATCHES])[ $1])])

# Avoid "const" on VAX C for libtool's dynamic symbol bits, like with OSF and
# Windows platforms, as there seem to be some problems with relocations of
# function pointer values in readonly memory.
#
# We patch it in ltmain.sh (via config.status) and in configure directly.
# This code is also duplicated in libltdl so further fixes might be needed
# for packages using bindled libltdl (not done here... yet?).
_DX_LT_PATCH([ltmain-sh-vax-c],
  [DX_PATCH_MACRO([_LT_CONFIG], [^\( *\)[$]SED '[$]q' "[$]ltmain"],
  [\1# Use this opportunity also to work around a VAX C bug in upstream libtool
\1$SED 's/^# *elif.*__osf__$/& || (defined vaxc \\&\\& defined VAXC)/' "$ltmain"])])

_DX_LT_PATCH([libtool-m4-vax-c],
  [DX_PATCH_MACRO([_LT_CMD_GLOBAL_SYMBOLS], [^# *elif.*__osf__$],
    [\& || (defined vaxc && defined VAXC)])])
