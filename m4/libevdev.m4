dnl Copyright © 2017 Nick Bowler
dnl
dnl License WTFPL2: Do What The Fuck You Want To Public License, version 2.
dnl This is free software: you are free to do what the fuck you want to.
dnl There is NO WARRANTY, to the extent permitted by law.

dnl DX_LIB_LIBEVDEV
dnl
dnl Search for libevdev.  This library does not expose version information
dnl so min-version checks are not supported.
dnl
dnl If a working library is found, appropriate compiler and linker flags are
dnl stored in LIBEVDEV_CFLAGS and LIBEVDEV_LIBS, respectively, and the
dnl action-if-ok is performed (may be empty).  If no such library is found,
dnl then action-if-failed is performed if non-empty; otherwise configure will
dnl stop with a fatal error.
AC_DEFUN([DX_LIB_LIBEVDEV],
[DX_LIB_SETUP([libevdev])dnl
DX_LIB_PKGCONFIG_FLAGS([libevdev])
AC_MSG_CHECKING([for libevdev])
DX_LIB_SEARCH_LINK([libevdev], [_DX_LIB_LIBEVDEV_TEST],
  [ [[$LIBEVDEV_CFLAGS], [$LIBEVDEV_LIBS]],
    [[$dx_cv_libevdev_pkg_cflags], [$dx_cv_libevdev_pkg_libs],
      [test x"$dx_cv_libevdev_pkg_found" = x"yes"]],
    [[], [-levdev]] ])
AS_IF([test x"$dx_cv_libevdev_lib_found" = x"yes"], [$1],
  [m4_default([$2], [AC_MSG_FAILURE(
[libevdev is required.  The latest version may be found at
<https://www.freedesktop.org/wiki/Software/libevdev/>.
m4_newline([DX_LIB_USERFLAG_BLURB([libevdev])])
m4_newline([DX_LIB_PKGCONFIG_BLURB([libevdev])])
])])])])

dnl Internal test program for libevdev.
m4_define([_DX_LIB_LIBEVDEV_TEST], [AC_LANG_PROGRAM(
[#include <libevdev/libevdev.h>
],
[libevdev_free(libevdev_new());
])])
