# Copyright © 2024 Nick Bowler
#
# License WTFPL2: Do What The Fuck You Want To Public License, version 2.
# This is free software: you are free to do what the fuck you want to.
# There is NO WARRANTY, to the extent permitted by law.

# DX_C_ALIGNOF
#
# Probe whether the C compiler understands _Alignof(T).
#
# Some implementations (e.g., FreeBSD 9) define a usable _Alignof macro
# in <stddef.h> (and other headers), so we can use that if available.
#
# If not supported, _Alignof(T) is defined as a function-like macro which
# uses offsetof to guess the alignment of T, which must be a type such that
# T x; is a valid declaration of x as an object of type T.
#
# For these reasons, callers should #include <stddef.h> before using _Alignof.
#
# We skip the test if Autoconf has previously determined that the C compiler
# supports C11 or a newer standard, since the C11 test program checks this.
#
# Annoyingly, autoconf-2.71 removed the assignment of ac_cv_prog_cc_c11,
# even though this was actual documented behaviour...
AC_DEFUN([DX_C_ALIGNOF],
[AS_CASE([${ac_cv_prog_cc_c11-no}/${ac_prog_cc_stdc-no}],
  [no/no|*/c89|*/c99],
    [AC_CACHE_CHECK([if $CC supports _Alignof], [dx_cv_have_alignof],
      [AC_COMPUTE_INT([_dx_tmp], [_Alignof(char)], [#include <stddef.h>
], [_dx_tmp=0])
AS_CASE([$_dx_tmp], [1], [dx_cv_have_alignof=yes], [dx_cv_have_alignof=no])])],
  [dx_cv_have_alignof=yes])
AS_CASE([$dx_cv_have_alignof], [no], [AC_DEFINE([_Alignof(T)],
  [offsetof(struct { char a; T b; }, b)],
  [Define _Alignof(T) to a suitable fallback if _Alignof is unsupported.])])])

# DX_C_ALIGNAS
#
# Probe whether the C compiler understands _Alignas(X).
#
# Some implementations (e.g., FreeBSD 9) define a usable _Alignas macro
# in <stddef.h> (and other headers), so we can use that if available.  For
# this reason, callers should #include <stddef.h> before using _Alignas.
#
# If not supported, but the compiler supports the GNU __attribute__ or
# Microsoft __declspec methods to set alignment, _Alignas(X) is defined
# as a function-like macro which expands to such syntax.  These only
# work if X is an integer constant (as opposed to a type name).
#
# If there is no known method of declaring a variable with increased
# alignment, then _Alignas(X) is defined as a function-like macro which
# expands to nothing, which will compile but has no runtime effect.
#
# We skip the test if Autoconf has previously determined that the C
# compiler supports C11 or a newer standard, since the C11 test program
# checks this.
AC_DEFUN([DX_C_ALIGNAS],
[AS_CASE([${ac_cv_prog_cc_c11-no}/${ac_prog_cc_stdc-no}],
  [no/no|*/c89|*/c99],
    [AC_CACHE_CHECK([if $CC supports _Alignas], [dx_cv_have_alignas],
[dx_cv_have_alignas=no
for _dx_alignas dnl Eat newline to work around bash-5 parsing bug.
in '_Alignas(X)' '__attribute__((__aligned__(X)))' '__declspec(align(X))'
do
AC_COMPUTE_INT([_dx_tmp],
  [sizeof (struct { char a; char ALIGNAS_TEST(4) b; }) >= 8],
  [#include <stddef.h>
#define ALIGNAS_TEST(X) $_dx_alignas
],[_dx_tmp=0])
AS_CASE([$_dx_tmp/$_dx_alignas],
  [1/_Alignas*], [dx_cv_have_alignas=yes; break],
  [1/*], [dx_cv_have_alignas=$_dx_alignas; break])
done])],
  [dx_cv_have_alignas=yes])
AS_CASE([$dx_cv_have_alignas],
  [*'(X)'*], [AC_DEFINE_UNQUOTED([_Alignas(X)], [$dx_cv_have_alignas],
    [Define _Alignas(X) to a suitable fallback if _Alignas is unsupported.])],
  [no], [AC_DEFINE([_Alignas(X)], [/**/])])])
