AC_DEFUN([DX_LIB_LIBDISCID],
[DX_LIB_SETUP([libdiscid])dnl
DX_LIB_PKGCONFIG_FLAGS([libdiscid])
AC_MSG_CHECKING([for libdiscid[]m4_ifnblank([$1], [ at least version $1])])
DX_LIB_SEARCH_LINK([libdiscid],
  [m4_apply([_DX_LIB_LIBDISCID_TEST], m4_split([$1], [\.]))],
  [ [[$LIBDISCID_CFLAGS], [$LIBDISCID_LIBS]],
    [[$dx_cv_libdiscid_pkg_cflags], [$dx_cv_libdiscid_pkg_libs],
      [test x"$dx_cv_libdiscid_pkg_found" = x"yes"]],
    [[], [-ldiscid]] ])
AS_IF([test x"$dx_cv_libdiscid_lib_found" = x"yes"], [$2],
  [m4_default([$3], [AC_MSG_FAILURE(
[libdiscid[]m4_ifnblank([$1], [ version $1 or newer]) is required.  The latest
version may be found at <https://musicbrainz.org/doc/libdiscid>.
m4_newline([DX_LIB_USERFLAG_BLURB([libdiscid])])
m4_newline([DX_LIB_PKGCONFIG_BLURB([libdiscid])])
])])])])

dnl Internal test program for libdiscid.  Check that the version numbers in the
dnl header are sufficient and that some important functions are defined.
m4_define([_DX_LIB_LIBDISCID_TEST], [AC_LANG_PROGRAM(
[#include <discid/discid.h>
m4_ifnblank([$1], [dnl
#if DISCID_VERSION_MAJOR < $1
DX_LIB_COMPILE_ERROR([major version insufficient])
m4_ifnblank([$2], [dnl
#elif DISCID_VERSION_MAJOR == $1
#  if DISCID_VERSION_MINOR < $2
DX_LIB_COMPILE_ERROR([minor version insufficient])
m4_ifnblank([$3], [dnl
#  elif DISCID_VERSION_MINOR == $2
#    if DISCID_VERSION_PATCH < $3
DX_LIB_COMPILE_ERROR([patch version insufficient])
#    endif
])dnl
#  endif
])dnl
#endif
])], [discid_free(discid_new());])])
