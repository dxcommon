dnl Copyright © 2011, 2015 Nick Bowler
dnl
dnl License WTFPL2: Do What The Fuck You Want To Public License, version 2.
dnl This is free software: you are free to do what the fuck you want to.
dnl There is NO WARRANTY, to the extent permitted by law.

dnl DX_LIB_LIBMODPLUG([action-if-ok], [action-if-failed])
dnl
dnl Search for libmodplug.  This library does not expose version information
dnl so min-version checks are not supported.
dnl
dnl If a working library is found, appropriate compiler and linker flags are
dnl stored in LIBMODPLUG_CFLAGS and LIBMODPLUG_LIBS, respectively, and the
dnl action-if-ok is performed (may be empty).  If no such library is found,
dnl then action-if-failed is performed if non-empty; otherwise configure will
dnl stop with a fatal error.
AC_DEFUN([DX_LIB_LIBMODPLUG],
[DX_LIB_SETUP([libmodplug])dnl
DX_LIB_PKGCONFIG_FLAGS([libmodplug])
AC_MSG_CHECKING([for libmodplug])
DX_LIB_SEARCH_LINK([libmodplug], [_DX_LIB_LIBMODPLUG_TEST],
  [ [[$LIBMODPLUG_CFLAGS], [$LIBMODPLUG_LIBS]],
    [[$dx_cv_libmodplug_pkg_cflags], [$dx_cv_libmodplug_pkg_libs],
      [test x"$dx_cv_libmodplug_pkg_found" = x"yes"]],
    [[], [-lmodplug]] ])
AS_IF([test x"$dx_cv_libmodplug_lib_found" = x"yes"], [$1],
  [m4_default([$2], [AC_MSG_FAILURE([dnl
libmodplug is required.  The latest version may be found at
<http://modplug-xmms.sourceforge.net>.
m4_newline([DX_LIB_USERFLAG_BLURB([libmodplug])])
m4_newline([DX_LIB_PKGCONFIG_BLURB([libmodplug])])
])])])])

dnl Internal test program for libmodplug.
m4_define([_DX_LIB_LIBMODPLUG_TEST], [AC_LANG_PROGRAM([dnl
#include <libmodplug/modplug.h>
], [dnl
unsigned char buf[[128]];
ModPlugFile *f = ModPlug_Load(buf, sizeof buf);
ModPlug_Unload(f);
])])
