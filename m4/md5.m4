dnl Copyright © 2021-2022 Nick Bowler
dnl
dnl License WTFPL2: Do What The Fuck You Want To Public License, version 2.
dnl This is free software: you are free to do what the fuck you want to.
dnl There is NO WARRANTY, to the extent permitted by law.

dnl DX_PROG_MD5
dnl
dnl Search PATH for a command-line md5 digest utility, such as GNU md5sum,
dnl FreeBSD md5 or openssl md5.  If found, the MD5 variable (which is
dnl substituted by AC_SUBST) is set to the result and the cache variable
dnl dx_cv_md5_works is set to "yes".  Otherwise, dx_cv_md5_works is set to
dnl "no".
dnl
dnl The utilities that can be found have slightly different options and
dnl output formats.  For example, GNU md5sum prints each line with the
dnl digest followed by the filename, whereas openssl md5 prints the filename
dnl followed by the digest.  The MD5 setting will include the -r option, when
dnl supported, to prefer a command that prints the digest first as this is
dnl easier to parse by shell code.  The cache variable dx_cv_md5_outpos will
dnl be set to "front" if the digest appears at the beginning of the line,
dnl and "back" if the digest appears at the end of the line.  Portable usage
dnl should handle both scenarios.
AC_DEFUN([DX_PROG_MD5], [AC_PREREQ([2.62])dnl
AC_ARG_VAR([MD5], [MD5 digest command])dnl
eval ': >conftest.in'
AC_CACHE_CHECK([for utility to compute MD5 digests], [ac_cv_path_MD5],
[dx_cv_md5_found=:
AC_PATH_PROGS_FEATURE_CHECK([MD5], [md5sum md5 openssl],
[DX_BASENAME([md5_relcmd], ["$ac_path_MD5"])
set x '-r' ''; shift
AS_CASE([$md5_relcmd], [openssl], [ac_path_MD5="$ac_path_MD5 md5"],
                       [md5sum], [shift])
eval ': >conftest.in'
for md5_arg
do
_DX_MD5_DO_TEST([$ac_path_MD5 $md5_arg],
  [ac_path_MD5="$ac_path_MD5 $md5_arg"; break])
done
AS_IF([test x"$dx_cv_md5_works" = x"yes"],
[md5_bypath=`{ command -v "$md5_relcmd"; } 2>/dev/null` #''
ac_cv_path_MD5=$ac_path_MD5
set x $ac_path_MD5; shift
if test x"$md5_bypath" = x"$[1]"; then
  shift; set x $md5_relcmd "$[@]"; shift;
  ac_cv_path_MD5=$[*]
fi
ac_path_MD5_found=:])], [ac_cv_path_MD5='none found' dx_cv_md5_found=false])])
AS_IF([$dx_cv_md5_found],
[MD5=$ac_cv_path_MD5
AC_CACHE_CHECK([for $MD5 digest position], [dx_cv_md5_outpos],
[_DX_MD5_DO_TEST([$MD5])])])
rm -f conftest.in conftest.out])

AC_DEFUN([_DX_MD5_DO_TEST],
[AS_IF([$1 conftest.in >conftest.out 2>&AS_MESSAGE_LOG_FD],
[read a b <conftest.out
AS_IF([test x"$a" = x"d41d8cd98f00b204e9800998ecf8427e"],
  [dx_cv_md5_works=yes dx_cv_md5_outpos=front
$2],
[set x $b; shift; eval "a=\$$[#]"
AS_IF([test x"$a" = x"d41d8cd98f00b204e9800998ecf8427e"],
  [dx_cv_md5_works=yes dx_cv_md5_outpos=back
$2], [dx_cv_md5_works=no dx_cv_md5_outpos='not found'])])],
[dx_cv_md5_works=no dx_cv_md5_outpos='not found'])])
