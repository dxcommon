dnl Copyright © 2014-2015 Nick Bowler
dnl
dnl License WTFPL2: Do What The Fuck You Want To Public License, version 2.
dnl This is free software: you are free to do what the fuck you want to.
dnl There is NO WARRANTY, to the extent permitted by law.

dnl DX_COMMAND_OUTPUT(variable, command, [action-if-ok], [action-if-failed])
dnl
dnl Helper to capture standard output of a shell command.  If the command
dnl is successful then variable is assigned with its output, and action-if-ok
dnl is performed.  Otherwise, if the command is unsuccessful, then variable
dnl is not changed, and action-if-failed is performed.
AC_DEFUN([DX_COMMAND_OUTPUT], [AC_REQUIRE([_DX_COMMAND_OUTPUT_SETUP])dnl
AS_IF([$2 >conftest.do0 2>&AS_MESSAGE_LOG_FD],
  [m4_do([dx_fn_cmdout_collect_output $1],
    [m4_ifnblank([$3], [m4_newline([$3])])])],
  [$4])
rm -f conftest.do0 conftest.do1])

AC_DEFUN_ONCE([_DX_COMMAND_OUTPUT_SETUP], [m4_divert_push([INIT_PREPARE])dnl
# Helper function to store the contents of conftest.do0 into a shell variable.
dx_fn_cmdout_collect_output () {
  AS_UNSET([$][1])
  # Double up backslashes as they will be stripped by read.
  # Heirloom sh read apparently just eats backslashes at EOL, so we compromise
  # by adding a marker that can be stripped out afterwards.
  _dx_eol='@%eol@%'
  sed -e 's/\\/\\\\/g' -e 's/\\$/\\'"$_dx_eol/" conftest.do0 >conftest.do1 ||
    return
  exec 3<conftest.do1
  _dx_save_IFS=$IFS
  while IFS=; read _dx_tmp <&3
  do
    IFS=$_dx_save_IFS
    # strip out EOL marker, if present
    AS_CASE([$_dx_tmp], [*"\\$_dx_eol"],
      [_dx_tmp=`AS_ECHO(["$_dx_tmp"]) | sed "s/$_dx_eol\$//"`])
    AS_VAR_SET_IF([$][1],
      [AS_VAR_APPEND([$][1], ["$as_nl$_dx_tmp"])],
      [AS_VAR_SET([$][1], [$_dx_tmp])])
  done
  IFS=$_dx_save_IFS
  exec 3<&-
}

m4_divert_pop()])
