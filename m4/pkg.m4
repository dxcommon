# Copyright © 2014, 2024 Nick Bowler
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# DX_PKG_CONFIG(shell-variable, arguments,
#               [action-if-ok], [action-if-failed],
#               [action-if-not-available])
#
# Execute pkg-config with the given arguments, and store the output in
# the specified shell variable.  If successful, execute action-if-ok.  If
# unsuccessful, execute action-if-failed.  If pkg-config is not available,
# execute action-if-not-available.
AC_DEFUN([DX_PKG_CONFIG], [AC_REQUIRE([DX_PROG_PKG_CONFIG])dnl
AS_UNSET([$1])
AS_IF([test ${PKG_CONFIG:+y}],
  [DX_COMMAND_OUTPUT([$1], [$PKG_CONFIG --print-errors $2],
    [$3], [m4_default([$4], [AS_SET_STATUS([$?])])])],
  [m4_default([$5], [AS_SET_STATUS([2])])])])

# DX_PROG_PKG_CONFIG
AC_DEFUN_ONCE([DX_PROG_PKG_CONFIG],
[AC_ARG_VAR([PKG_CONFIG],
  [Command to help determine library build flags])dnl
AC_ARG_VAR([PKG_CONFIG_PATH],
  [Additional pkg-config database directories to search])dnl
AC_CHECK_TOOL([PKG_CONFIG], [pkg-config])])
