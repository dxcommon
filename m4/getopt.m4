# Copyright © 2024 Nick Bowler
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# DX_CHECK_GETOPT_LONG
#
# Check whether or not the getopt_long function is available.  If it
# is, the macro HAVE_GETOPT_LONG is defined to 1 and the cache variable
# dx_cv_have_getopt_long is set to "yes".  Otherwise, dx_cv_have_getopt_long
# is set to "no".
AC_DEFUN([DX_CHECK_GETOPT_LONG],
  [_DX_CHECK_GNU_GETOPT([getopt_long])])

# Same as above, but for getopt_long_only:
#
#   - the cache variable name is dx_cv_have_getopt_long_only
#   - the macro name is HAVE_GETOPT_LONG_ONLY
AC_DEFUN([DX_CHECK_GETOPT_LONG_ONLY],
  [_DX_CHECK_GNU_GETOPT([getopt_long_only])])

# These variants are the same as the above, except they additionally define
# Automake conditional HAVE_GETOPT_LONG (or HAVE_GETOPT_LONG_ONLY) to be true
# if the corresponding function is available.
#
# To support the case where a functions is used by only conditionally-compiled
# programs, the macros may be expanded within an m4sh conditional (e.g., AS_IF)
# to control whether or not the compiler test is run.
#
# The Automake conditional will be properly defined to "false" if the test is
# skipped in this manner.
AC_DEFUN([DX_CHECK_GETOPT_LONG_AM],
  [AC_REQUIRE([_DX_GETOPT_LONG_AM])DX_CHECK_GETOPT_LONG])
AC_DEFUN([DX_CHECK_GETOPT_LONG_ONLY_AM],
  [AC_REQUIRE([_DX_GETOPT_LONG_ONLY_AM])DX_CHECK_GETOPT_LONG_ONLY])

AC_DEFUN([_DX_GETOPT_LONG_AM],
  [AC_CONFIG_COMMANDS_PRE([AM_CONDITIONAL([HAVE_GETOPT_LONG],
    [test x"$dx_cv_have_getopt_long" = x"yes"])])])
AC_DEFUN([_DX_GETOPT_LONG_ONLY_AM],
  [AC_CONFIG_COMMANDS_PRE([AM_CONDITIONAL([HAVE_GETOPT_LONG_ONLY],
    [test x"$dx_cv_have_getopt_long_only" = x"yes"])])])

# Probe either getopt_long or getopt_long_only.  This checks that all
# essential parts are defined after including just <getopt.h>.
m4_define([_DX_CHECK_GNU_GETOPT],
[AC_CACHE_CHECK([for $1], [dx_cv_have_$1],
  [AC_LINK_IFELSE([AC_LANG_PROGRAM([#include <getopt.h>
], [struct option lopts[[2]] = {0};
lopts->name = "";
lopts->has_arg = opterr;
lopts->flag = (int *)0;
lopts->val = optopt;
return $1(optind, &optarg, "", lopts, (int *)0);
])], [dx_cv_have_$1=yes], [dx_cv_have_$1=no])])
AS_CASE([$dx_cv_have_$1], [yes],
  [AC_DEFINE(m4_toupper([HAVE_$1]), [1],
    [Define to 1 if the $1 function is available.])])])

m4_define([_DX_CHECK_GNU_GETOPT_AM],
[AC_REQUIRE(m4_toupper([DX_CHECK_$1]))dnl
AM_CONDITIONAL(m4_toupper([HAVE_$1]), [test x"$dx_cv_have_$1" = x"yes"])])
