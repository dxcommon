dnl Copyright © 2009, 2019-2020 Nick Bowler
dnl
dnl License WTFPL2: Do What The Fuck You Want To Public License, version 2.
dnl This is free software: you are free to do what the fuck you want to.
dnl There is NO WARRANTY, to the extent permitted by law.

dnl DX_LIB_GLIB2([min-version], [modules], [action-if-ok], [action-if-fail])
dnl
dnl Search for GLib-2 libraries, with the given minimum version.  The modules
dnl argument specifies which additional GLib libraries are required, which is
dnl a space-separated list of zero or more of the following values:
dnl
dnl   gthread
dnl   gobject
dnl   gio
dnl   gio-unix
dnl   gmodule
dnl
dnl Each detected module's flags are set in LIBxxx_CFLAGS and LIBxxx_LIBS,
dnl where xxx is the module's name in all capital letters and with hyphens
dnl converted to underscores (cf. AS_TR_SH).  The core GLib flags are set
dnl in LIBGLIB_CFLAGS and LIBGLIB_LIBS.  The combination of all selected
dnl and detected modules is set in GLIB_CFLAGS and GLIB_LIBS.  All of
dnl these variables are substituted as normal.
dnl
dnl If GLib and all selected modules are successfully found, action-if-ok
dnl is run.  Otherwise, action-if-fail is run.  If necessary, use the
dnl dx_cv_libxxx_lib_found cache variables to determine exactly which
dnl modules are not available.
AC_DEFUN([DX_LIB_GLIB2],
[GLIB_CFLAGS= GLIB_LIBS= _dx_lib_glib_search_ok=true
m4_define([_DX_LIB_GLIB2_TESTHEAD], [#include <glib.h>])dnl
_DX_LIB_GLIB2_VERSION_TEST(m4_split([$1], [\.]))dnl
m4_define([_DX_LIB_GLIB2_TESTMAIN],
  [const char *v = g_get_prgname();])dnl
m4_define([_DX_GLIB_MODULES], [glib])dnl
m4_append_uniq_w([_DX_GLIB_MODULES], [$2])dnl

m4_map_args_w(m4_defn([_DX_GLIB_MODULES]), [_DX_LIB_GLIB2_MODULE(], [)])
AC_SUBST([GLIB_CFLAGS])dnl
AC_SUBST([GLIB_LIBS])dnl
AS_IF([$_dx_lib_glib_search_ok], [DX_LIB_GLIB_BUSTED_GNU_INLINE
$3], [m4_default([$4], [AC_MSG_FAILURE(
[GLib[]m4_ifnblank([$1], [ version $1 or newer]) is required.  The latest
version may be found at <https://www.gtk.org/>.
m4_newline([DX_LIB_USERFLAG_BLURB([libglib], [GLib])])
m4_newline([DX_LIB_PKGCONFIG_BLURB([glib], [GLib])])
])])])])

AC_DEFUN([_DX_LIB_GLIB2_MODULE],
[DX_LIB_SETUP([lib$1],
[m4_if([$1], [glib],     [GLib],
       [$1], [gio],      [GIO],
       [$1], [gio-unix], [GIO-Unix],
       [$1], [gthread],  [GThread],
       [$1], [gobject],  [GObject],
       [$1], [gmodule],  [GModule])])dnl
DX_LIB_PKGCONFIG_FLAGS([lib$1],
  [m4_if([$1], [gmodule], [gmodule-no-export-2.0], [$1-2.0])])
AC_MSG_CHECKING([for lib$1])
DX_LIB_SEARCH_LINK([lib$1],
  [AC_LANG_PROGRAM([_DX_LIB_GLIB2_TESTHEAD
m4_n(m4_if([$1], [gobject], [[#include <glib-object.h>]]))dnl
m4_n(m4_if([$1], [gio], [[#include <gio/gio.h>]]))dnl
m4_n(m4_if([$1], [gio-unix], [[#include <gio/gunixmounts.h>]]))dnl
m4_n(m4_if([$1], [gmodule], [[#include <gmodule.h>]]))],
    [_DX_LIB_GLIB2_TESTMAIN
m4_if([$1], [glib], [],
      [$1], [gobject], [g_type_init();
{ GObject *obj = g_object_new(G_TYPE_OBJECT, NULL); }],
      [$1], [gthread], [g_thread_init(NULL);
{ GThread *thr = g_thread_self(); }],
      [$1], [gio], [{ GFile *f = g_file_new_for_path("/"); }],
      [$1], [gio-unix], [{ GUnixMountEntry *m = g_unix_mount_at("/", NULL); }],
      [$1], [gmodule], [{ GModule *m = g_module_open(NULL, 0); }],
      [DX_LIB_COMPILE_ERROR([unsupported module])])])],
  [ [[$LIBGLIB_CFLAGS m4_if([$1], [glib], [], [m4_toupper([$lib$1_cflags])])],
     [$LIBGLIB_LIBS m4_if([$1], [glib], [], [m4_toupper([$lib$1_libs])])]],
    [[$AS_TR_SH([dx_cv_lib$1_pkg_cflags])],
     [$AS_TR_SH([dx_cv_lib$1_pkg_libs])],
     [test x"$AS_TR_SH([dx_cv_lib$1_pkg_found])" = x"yes"]],
    m4_if([$1], [gthread], [[[$LIBGLIB_CFLAGS -pthread],
          [-pthread -lgthread-2.0 -lglib-2.0]],])
    [[$LIBGLIB_CFLAGS], [m4_if([$1], [glib], [], [-l$1-2.0]) -lglib-2.0]],
  ])
AS_IF([test x"$AS_TR_SH([dx_cv_lib$1_lib_found])" = x"yes"],
[set -f
set x $AS_TR_SH([m4_toupper([lib$1_cflags])]); shift
for _dx_i
do
  for _dx_j in $GLIB_CFLAGS; do
    if test x"$_dx_i" = x"$_dx_j"; then
      shift; continue 2
    fi
  done
  set x ${1+"$[]@"} "$[]1"; shift; shift
done
GLIB_CFLAGS=${GLIB_CFLAGS:+"$GLIB_CFLAGS "}$[]*
set x $AS_TR_SH([m4_toupper([lib$1_libs])]); shift
for _dx_i
do
  for _dx_j in $GLIB_LIBS; do
    if test x"$_dx_i" = x"$_dx_j"; then
      shift; continue 2
    fi
  done
  set x ${1+"$[]@"} "$[]1"; shift; shift
done
GLIB_LIBS=$[]*${GLIB_LIBS:+" $GLIB_LIBS"}
set +f],
  [_dx_lib_glib_search_ok=false])
])

AC_DEFUN([_DX_LIB_GLIB2_VERSION_TEST], [m4_append([_DX_LIB_GLIB2_TESTHEAD], [
[#if !GLIB_CHECK_VERSION(]m4_default([$1],
[0]), m4_default([$2],
[0]), m4_default([$3],
[0]))
DX_LIB_COMPILE_ERROR([glib version insufficient])
#endif])])

dnl Old glib headers expect gcc to provide GNU inline semantics by default.
dnl This is not the case with modern gcc if the language standard is set to
dnl C99 or later.  This results is linker failures due to multiple definitions
dnl when two source files include the GLib headers.
dnl
dnl Pre-#defining G_INLINE_FUNC to 'static inline' works to avoid this problem,
dnl so we have configure first test to see if the problem occurs, then test
dnl if this definition fixes it, then AC_DEFINE that if necessary.
AC_DEFUN([DX_LIB_GLIB_BUSTED_GNU_INLINE],
[AC_CACHE_CHECK([whether GLib needs obsolete GNU inline semantics],
[dx_cv_glib_busted_gnu_inline],
[save_CFLAGS=$CFLAGS save_LIBS=$LIBS CFLAGS="$LIBGLIB_CFLAGS $CFLAGS"
LIBS="conftest_bar.$OBJEXT $LIBGLIB_LIBS $save_LIBS"
dx_cv_glib_busted_gnu_inline=unknown
for dx_cv_glib_inline_workaround in no G_INLINE_FUNC G_IMPLEMENT_INLINES; do
AS_CASE([$dx_cv_glib_inline_workaround],
[G_INLINE_FUNC], [DEFTEST='#define G_INLINE_FUNC static inline' ret=yes
], [G_IMPLEMENT_INLINES], [DEFTEST='#define G_IMPLEMENT_INLINES 1' ret=yes
], [DEFTEST= ret=no
])
AC_COMPILE_IFELSE([AC_LANG_SOURCE([$DEFTEST
#include <glib.h>
int bar(void) { return 0; }])],
[mv -f conftest.$OBJEXT conftest_bar.$OBJEXT],
[continue])
AC_LINK_IFELSE([AC_LANG_PROGRAM([$DEFTEST
#include <glib.h>
extern int bar(void);], [return bar();])],
[dx_cv_glib_busted_gnu_inline=$ret; break])
rm -f conftest_bar.$OBJEXT
done
CFLAGS=$save_CFLAGS LIBS=$save_LIBS])
AS_CASE([$dx_cv_glib_inline_workaround],
[G_INLINE_FUNC], [AC_DEFINE([G_INLINE_FUNC], [static inline],
  [Define to static inline to work around multiple definitions in old GLib.])
], [G_IMPLEMENT_INLINES], [AC_DEFINE([G_IMPLEMENT_INLINES], [1],
  [If G_INLINE_FUNC cannot work around multiple definition issues,
   define to 1 to disable inlining altogether in GLib.])
])])
