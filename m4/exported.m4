dnl Copyright © 2012, 2022 Nick Bowler
dnl
dnl Generate the exported.sh script used to determine the exported symbols of
dnl a (libtool) object file.  This is required by the glconfig Automake
dnl fragment, but will be called automatically in that case.
dnl
dnl License WTFPL2: Do What The Fuck You Want To Public License, version 2.
dnl This is free software: you are free to do what the fuck you want to.
dnl There is NO WARRANTY, to the extent permitted by law.

AC_DEFUN([DX_EXPORTED_SH],
[AC_REQUIRE([DX_INIT])dnl
AC_REQUIRE([AC_PROG_SED])dnl

AC_CONFIG_COMMANDS_PRE(
[AC_PROVIDE_IFELSE([LT_INIT], , [m4_warn([syntax],
[$0 requires libtool.
Consider adding an invocation of LT_INIT to configure.ac.])])

GLOBAL_SYMBOL_PIPE=$lt_cv_sys_global_symbol_pipe
AC_SUBST([GLOBAL_SYMBOL_PIPE])])

AC_CONFIG_FILES([exported.sh:]DX_BASEDIR[/snippet/exported.sh.in],
  [chmod +x exported.sh])])
