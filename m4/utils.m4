dnl Copyright © 2015, 2018-2019 Nick Bowler
dnl
dnl Assorted utility macros.
dnl
dnl License WTFPL2: Do What The Fuck You Want To Public License, version 2.
dnl This is free software: you are free to do what the fuck you want to.
dnl There is NO WARRANTY, to the extent permitted by law.

dnl DX_DO(arg, ...)
dnl Expand each argument in sequence, inserting newlines between them.
AC_DEFUN([DX_DO], [m4_do([$1], [m4_map_args([m4_newline], m4_shift($@))])])

dnl DX_VAR_NORMALIZE_SPACE(varname)
dnl Trim leading and trailing whitespace from a shell variable, and replace
dnl consecutive sequences of internal whitespace with a single space.
AC_DEFUN([DX_VAR_NORMALIZE_SPACE], [set -f; set x $$1; shift; $1=$[]* set +f])
