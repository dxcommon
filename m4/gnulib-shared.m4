dnl Copyright © 2022-2024 Nick Bowler
dnl
dnl Hack to reduce glsym transformations to a subset of gnulib sources.
dnl
dnl License WTFPL2: Do What The Fuck You Want To Public License, version 2.
dnl This is free software: you are free to do what the fuck you want to.
dnl There is NO WARRANTY, to the extent permitted by law.

dnl DX_GNULIB_SYMFILES(filename, [shortname])
dnl
dnl When using the glconfig symbol-renaming functionality to include gnulib
dnl components in a library, it may not be the case that every file is actually
dnl used by the library.  In this situation, applying the symbol renaming to
dnl every file unconditionally just makes the build take longer for no reason.
dnl
dnl Using this macro reduces the files processed for renaming to only those
dnl source files listed in the specified file, with one source file per line.
dnl This file should list every source file that could possibly be included
dnl into the library -- one way to generate such a list is by using gnulib-tool
dnl on a subset of modules.
dnl
dnl As the list depends on the results of configure, this works by installing a
dnl config.status hook to patch the gnulib_symfiles assignment in the Makefile
dnl based on the actually-enabled gnulib objects.
dnl
dnl The list of leftover objects which are not subject to symbol renaming is
dnl then placed in the gnulib_extra_objects make variable.  If shortname is
dnl nonempty, these object file names will be prefixed with the specified
dnl value followed by a hyphen (to match up with Automake's object file
dnl renaming system).
AC_DEFUN_ONCE([DX_GNULIB_SYMFILES],
[AC_REQUIRE([DX_PROG_JOIN])dnl
CONFIGURE_DEPENDENCIES=${CONFIGURE_DEPENDENCIES:+" "}'${top_srcdir}/$1'
AC_SUBST([CONFIGURE_DEPENDENCIES])dnl
AC_CONFIG_COMMANDS([gnulib-symfiles],
[for gl_f in $CONFIG_FILES
do
  # Restrict processing to only Automake-generated makefiles.
  gl_save_IFS=$IFS
  IFS=:; set x $gl_f
  gl_of=$[2]; gl_am=$gl_of.am # TODO fully handle user-specified input files
  IFS=$gl_save_IFS
  test -f "$gl_am" || test -f "$srcdir/$gl_am" || continue
  $MAKE -f - glconfig-objects GLCONFIG_OBJECTS="$ac_tmp/gl_objs.lst" \
    <"$gl_of" >/dev/null 2>&1 && test -f "$ac_tmp/gl_objs.lst" || continue
  sed 's|[[.][^/.]*$]| &|' "$ac_tmp/gl_objs.lst" | LC_ALL=C sort -u \
    >"$ac_tmp/gl_syms.1"
  LC_ALL=C sort -u >"$ac_tmp/gl_syms.2" <<'EOF'
dnl hide this include from traces so that aclocal doesn't find it.
dnl The required details for rebuild rules are handled by substituting
dnl CONFIGURE_DEPENDENCIES above.
m4_bpatsubst(m4_indir([m4_include], [$1]), [[.][^/.]*$], [ x])
EOF
  $JOIN -a1 "$ac_tmp/gl_syms.1" "$ac_tmp/gl_syms.2" >"$ac_tmp/gl_syms.3"
  # Now replace default gnulib_symfiles assignment with the computed list
  $AWK '$[1] "/" $[2] "/" $[3] == "gnulib_symfiles/=/$(gnulib_all_symfiles)" {
    objlst = symlst = "";
    while ((rc = getline < f) > 0) {
      if ($[3] == "x") {
        symlst = symlst " " $[1] ".glsym";
      } else {
        m4_ifnblank([$2], [sub(/[[^\/]]*$/, "$2-&", $[1]);
        ])objlst = objlst " " $[1] $[2];
      }
    }

    if (rc < 0)
      exit 1;

    print "gnulib_symfiles =" symlst;
    print "gnulib_extra_objects =" objlst;
    next;
  }
  # add ourself to Automake-generated rebuild rules
  $[2] == "=" && $[1] ~ /^am__(depfiles_maybe|maybe_remake_depfiles)$/ {
    for (i = 3; i <= NF; i++)
      if ($i == "gnulib-symfiles")
        break;
    $i = "gnulib-symfiles";
  }
  { print; }' f="$ac_tmp/gl_syms.3" "$gl_of" >"$ac_tmp/gl_tmp.mk"
  mv -f "$ac_tmp/gl_tmp.mk" "$gl_of"
  break # should be max one makefile needing patching
done], [: "\${MAKE=${MAKE-make}}" "\${JOIN=$JOIN}"])])
