dnl Copyright © 2012, 2021, 2023-2024 Nick Bowler
dnl
dnl Definitions to help maintain compatibility with multiple Automake versions.
dnl
dnl License WTFPL2: Do What The Fuck You Want To Public License, version 2.
dnl This is free software: you are free to do what the fuck you want to.
dnl There is NO WARRANTY, to the extent permitted by law.

dnl DX_AUTOMAKE_COMPAT
dnl
dnl Provides definitions to help maintain compatibility with older versions of
dnl Automake.  Currently, this means:
dnl
dnl  - Automake 1.15 includes an install-sh that depends on a "better" shell
dnl    environment.  However, even if such a shell is available and found by
dnl    configure, the script includes #!/bin/sh so it won't work if /bin/sh
dnl    is deficient.  This problem is worked around by patching MKDIR_P
dnl    and INSTALL to include $SHELL.
dnl
dnl  - AM_V and AM_DEFAULT_V (new in Automake 1.12) are defined and AC_SUBSTed
dnl    if Automake did not already, in a manner compatible with silent-rules
dnl    support from previous versions.
dnl
dnl  - A make variable DX_ALIGN_V is defined which contain printf alignment
dnl    flags for silent-rules output, since the alignment changed between
dnl    Automake 1.11 and 1.12.  It is intended to be used like this:
dnl
dnl      MYGEN_V   = $(MYGEN_V@AM_V@)
dnl      MYGEN_V_  = $(MYGEN_V_@AM_DEFAULT_V)
dnl      MYGEN_V_0 = @printf '  %$(DX_ALIGN_V)s %s\n' 'MYGEN   ' $@
dnl
dnl    The spacing after MYGEN should be defined so that the silent-rules
dnl    alignment is correct for the latest version of Automake if DX_ALIGN_V
dnl    is empty (that way, this definition provides only backwards
dnl    compatibility).  However, it is irrelevant when DX_ALIGN_V is defined
dnl    by this macro, as the spacing will always be correct.  This feature
dnl    maintains alignment by truncating the argument, so it will only work
dnl    properly if your string can actually be aligned in all versions of
dnl    Automake being targeted (this means you are limited to 6 characters
dnl    in Automake 1.11).

AC_DEFUN_ONCE([DX_AUTOMAKE_COMPAT],
[AC_REQUIRE([DX_INIT])dnl
AC_REQUIRE([_DX_AUTOMAKE_COMPAT_PATCH_CC])dnl
AC_REQUIRE([_DX_AUTOMAKE_COMPAT_PATCH_AR])dnl
AC_CONFIG_COMMANDS_PRE([# Patch install-sh references to include $SHELL
AS_CASE([$MKDIR_P], ["$ac_install_sh"*], [MKDIR_P="$SHELL $MKDIR_P"])
AS_CASE([$INSTALL], ["$ac_install_sh"*], [INSTALL="$SHELL $INSTALL"])

# Substitute AM_V/AM_DEFAULT_V for compatibility with <automake-1.12
AS_VAR_SET_IF([AM_V], [], [AC_SUBST([AM_V], ['$(V)'])])
AS_VAR_SET_IF([AM_DEFAULT_V], [],
  [AC_SUBST([AM_DEFAULT_V], ['$(AM_DEFAULT_VERBOSITY)'])])])dnl

# This will only work if there is a Makefile.in generated at the top srcdir.
AC_CACHE_CHECK([for alignment of silent-rules output], [dx_cv_align_v], [dnl
cat >conftest.sed <<'EOF'
/^am__v_GEN_0 *=/{
  s/@<:@^"@:>@*"  \(GEN *\).*/\1/
  s/ /x/g
  p
}
EOF
if sed -nf conftest.sed "$srcdir/Makefile.in" >conftest.out 2>/dev/null; then
  read dx_genline <conftest.out
  dx_cv_align_v=`expr "$dx_genline" : '.*'`
  test $dx_cv_align_v -eq 0 && dx_cv_align_v=unknown
fi
: "${dx_cv_align_v:=unknown}"
rm -f conftest*
])

AS_IF([test x"$dx_cv_align_v" != x"unknown"],
  [AC_SUBST([DX_ALIGN_V], [-$dx_cv_align_v.$dx_cv_align_v])])

AC_CACHE_CHECK([for depfiles generation method], [dx_cv_am_depfiles],
  [AS_IF([grep '^am--depfiles:' "$srcdir/Makefile.in" >/dev/null ||
          grep '[[$]](am__maybe_remake_depfiles)' "$srcdir/Makefile.in" >/dev/null],
    [dx_cv_am_depfiles='make am--depfiles'],
    [dx_cv_am_depfiles='config.status'])])
AS_CASE([$dx_cv_am_depfiles],
  ['make am--depfiles'], [dx_include_marker=' @%:@ am--include-marker'],
  [dx_include_marker=''])
AC_SUBST([dx_depfiles_target], [am--depfiles])
AC_SUBST([dx_include_marker])
AM_SUBST_NOTMAKE([dx_depfiles_target])
AM_SUBST_NOTMAKE([dx_include_marker])])

AC_DEFUN_ONCE([_DX_AUTOMAKE_COMPAT_PATCH_CC],
[AC_BEFORE([$0], [_AM_PROG_CC_C_O])dnl
DX_PATCH_MACRO([_AM_PROG_CC_C_O],
  ["[$]am_aux_dir/compile],
  ["$SHELL $am_aux_dir/compile])])

AC_DEFUN_ONCE([_DX_AUTOMAKE_COMPAT_PATCH_AR],
[AC_BEFORE([$0], [AM_PROG_AR])dnl
DX_PATCH_MACRO([AM_PROG_AR],
  ["[$]am_aux_dir/ar-lib],
  ["$SHELL $am_aux_dir/ar-lib])])
