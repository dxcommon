dnl Copyright © 2020-2021, 2024 Nick Bowler
dnl
dnl License WTFPL2: Do What the Fuck You Want To Public License, version 2.
dnl This is free software: you are free to do what the fuck you want to.
dnl There is NO WARRANTY, to the extent permitted by law.

dnl DX_PROG_FLEX([min-version], [action-if-found], [action-if-not-found])
dnl
dnl Search PATH for a working flex utility with the given minimum version,
dnl which may be empty to accept any version.  If found, the FLEX
dnl variable (which is substituted by AC_SUBST) is set with the result,
dnl and action-if-found is executed.  Otherwise, action-if-not-found
dnl is executed if nonempty, else configure will exit with a fatal error.
AC_DEFUN([DX_PROG_FLEX], [AC_PREREQ([2.62])dnl
AC_ARG_VAR([FLEX], [Fast Lexical Analyzer command])dnl
AC_ARG_VAR([FLEXFLAGS], [Additional options to pass to flex])dnl
cat >conftest.l <<'EOF'
m4_apply([_DX_FLEX_TEST_PROGRAM], m4_split([$1], [\.]))
EOF
AC_CACHE_CHECK([for flex], [ac_cv_path_FLEX],
[dx_cv_flex_found=:
AC_PATH_PROGS_FEATURE_CHECK([FLEX], [flex lex],
[_DX_FLEX_DO_TEST([$ac_path_FLEX],
[DX_BASENAME([flex_relcmd], ["$ac_path_FLEX"])
flex_bypath=`{ command -v "$flex_relcmd"; } 2>/dev/null` #''
ac_cv_path_FLEX=$ac_path_FLEX
test x"$flex_bypath" = x"$ac_path_FLEX" && ac_cv_path_FLEX=$flex_relcmd
ac_path_FLEX_found=:
dx_cv_flex_works=yes])], [ac_cv_path_FLEX=no dx_cv_flex_found=false])])
AS_IF([$dx_cv_flex_found],
[FLEX=$ac_cv_path_FLEX
AC_CACHE_CHECK([whether $FLEX works], [dx_cv_flex_works],
[_DX_FLEX_DO_TEST([$FLEX], [dx_cv_flex_works=yes], [dx_cv_flex_works=no])])])
AS_IF([test x"$dx_cv_flex_works" = x"yes"],
  [$2], [m4_default([$3], [AC_MSG_FAILURE(
[flex m4_ifnblank([$1], [version at least $1 ])is required.

The latest version may be found at <https://github.com/westes/flex/>.
])])])
rm -f conftest.c conftest.inc conftest.l
])

m4_define([_DX_FLEX_TEST_PROGRAM],
[%option noyywrap
%{
#ifndef FLEX_SCANNER
DX_LIB_COMPILE_ERROR([FLEX_SCANNER not defined])
#endif
m4_ifnblank([$1], [dnl
#if YY_FLEX_MAJOR_VERSION < $1
DX_LIB_COMPILE_ERROR([major version insufficient])
m4_ifnblank([$2], [dnl
#elif YY_FLEX_MAJOR_VERSION == $1
#  if YY_FLEX_MINOR_VERSION < $2
DX_LIB_COMPILE_ERROR([minor version insufficient])
m4_ifnblank([$3], [dnl
#  elif YY_FLEX_MINOR_VERSION == $2
#    if YY_FLEX_SUBMINOR_VERSION < $3
DX_LIB_COMPILE_ERROR([subminor version insufficient])
#    endif
])dnl
#  endif
])dnl
#endif
])dnl
%}
%%
%%
int main(void)
{
  return yylex();
}])

AC_DEFUN([_DX_FLEX_DO_TEST], [rm -f conftest.c
AS_IF([DX_RUN_LOG([$1 -o conftest.inc conftest.l])],
  [AC_LINK_IFELSE([AC_LANG_SOURCE([[#include "conftest.inc"]])], [$2], [$3])],
  [$3])])
