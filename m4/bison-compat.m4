dnl Copyright © 2021 Nick Bowler
dnl License WTFPL2: Do What The Fuck You Want To Public License, version 2.
dnl This is free software: you are free to do what the fuck you want to.
dnl There is NO WARRANTY, to the extent permitted by law.

dnl DX_BISON_COMPAT
dnl
dnl Checks $BISON for various options that may be required for compatibility
dnl with multiple versions.  Currently tested features:
dnl
dnl If bison supports --define (as an alias for %define in the source file),
dnl then dx_cv_bison_define_cmdline will be set to "yes".  Otherwise, it will
dnl be set to "no".
dnl
dnl If bison supports the --define=api.header.include feature (and thus
dnl #includes the header in the parser rather than embedding its contents),
dnl then dx_cv_bison_api_header_include will be set to "yes".  Otherwise, it
dnl will be set to "no".
dnl
dnl If bison gives spews warnings for %error-verbose and -Wno-deprecated
dnl suppresses those warnings, then dx_cv_bison_warn_no_deprecated will be
dnl set to "yes".  Otherwise, it will be set to "no".

AC_DEFUN([DX_BISON_COMPAT],
[AC_REQUIRE([DX_PROG_BISON])dnl
AS_IF([test x"$dx_cv_bison_works" = x"yes"],
[# Older versions of bison accept --define as an abbreviation for --defines.
# This gives very bad results in the following configure test.
AC_CACHE_CHECK([whether $BISON supports --define],
  [dx_cv_bison_define_cmdline], [dx_cv_bison_define_cmdline=no
cat >conftest.y <<'EOF'
%{
int yylex(void) { return 0; }
void yyerror(const char *msg) { }
%}
%%
input:
%%
int main(void) { return yylval; }
EOF

rm -f conftest.tab.c conftest.x
$BISON --define=conftest.x conftest.y >&AS_MESSAGE_LOG_FD 2>&1
AS_IF([test ! -f conftest.x],
[AS_IF([$BISON conftest.y >&AS_MESSAGE_LOG_FD 2>&1],
  [AC_COMPILE_IFELSE([AC_LANG_SOURCE([[#include "conftest.tab.c"]])],
    [AS_IF([$BISON --define=api.pure conftest.y >&AS_MESSAGE_LOG_FD 2>&1],
      [AC_COMPILE_IFELSE([AC_LANG_SOURCE([[#include "conftest.tab.c"]])],
        [:], [dx_cv_bison_define_cmdline=yes])])])])])
])

AS_IF([test x"$dx_cv_bison_define_cmdline" != x"yes"],
  [dx_cv_bison_api_header_include=no])

AC_CACHE_CHECK([whether $BISON supports api.header.include],
  [dx_cv_bison_api_header_include], [dx_cv_bison_api_header_include=no
cat >conftest.y <<'EOF'
%{
int yylex(void) { return 0; }
void yyerror(const char *msg) { }
%}
%%
input:
EOF
rm -f conftest.[[123].[ch]]
for dx_pass in 1 2 3
do
  AS_CASE([$dx_pass],
    [1], [$BISON -o conftest.1.c --defines=conftest.1.h conftest.y >&AS_MESSAGE_LOG_FD 2>&1|| break],
    [2], [$BISON -o conftest.2.c --define=api.header.include='{"conftest.3.h"}' --defines=conftest.2.h conftest.y >&AS_MESSAGE_LOG_FD 2>&1 || break],
    [3], [mv conftest.2.c conftest.3.c; mv conftest.2.h conftest.3.h])
  AC_COMPILE_IFELSE([AC_LANG_SOURCE([[#include "conftest.$dx_pass.c"]])],
    [test $dx_pass -ne 2 || break], [test $dx_pass -eq 2 || break])
  test $dx_pass -ne 3 || dx_cv_bison_api_header_include=yes
done
])

AC_CACHE_CHECK([whether $BISON wants -Wno-deprecated],
  [dx_cv_bison_warn_no_deprecated], [dx_cv_bison_warn_no_deprecated=no
cat >conftest.y <<'EOF'
%error-verbose
%{
int yylex(void) { return 0; }
void yyerror(const char *msg) { }
%}
%%
input:
EOF
AS_IF([$BISON -Werror conftest.y >&AS_MESSAGE_LOG_FD 2>&1], [],
  [AS_IF([$BISON -Werror -Wno-deprecated conftest.y >&AS_MESSAGE_LOG_FD 2>&1],
    [dx_cv_bison_warn_no_deprecated=yes])])
])], [dx_cv_bison_define_cmdline=no
dx_cv_bison_api_header_include=no
dx_cv_bison_warn_no_deprecated=no])])
