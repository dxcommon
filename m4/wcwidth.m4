# Copyright © 2024 Nick Bowler
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# DX_CHECK_MRTOWC_WCWIDTH
#
# Check whether or not the mbrtowc and wcwidth functions are both usable.
# If they are, the macros HAVE_WCWIDTH and HAVE_MBRTOWC are both defined
# to 1 and the cache variable dx_cv_have_mbrtowc_wcwidth is set to "yes".
# Otherwise, dx_cv_have_mbrtowc_wcwidth is set to "no".
AC_DEFUN([DX_CHECK_MBRTOWC_WCWIDTH],
[AC_REQUIRE([AC_USE_SYSTEM_EXTENSIONS])dnl
AC_CACHE_CHECK([for mbrtowc / wcwidth], [dx_cv_have_mbrtowc_wcwidth],
  [AC_LINK_IFELSE([_DX_CHECK_MBRTOWC_WCWIDTH_TEST],
    [dx_cv_have_mbrtowc_wcwidth=yes],
    [dx_cv_have_mbrtowc_wcwidth=no])])
AS_CASE([$dx_cv_have_mbrtowc_wcwidth], [yes],
[AC_DEFINE([HAVE_MBRTOWC], [1],
  [Define to 1 if the mbrtowc function is available.])
AC_DEFINE([HAVE_WCWIDTH], [1],
  [Define to 1 if the wcwidth function is available.])])])

m4_define([_DX_CHECK_MBRTOWC_WCWIDTH_TEST],
[AC_LANG_PROGRAM([#include <wchar.h>
],
[mbstate_t ps;
wchar_t wc;
mbrtowc(&wc, "x", 1, &ps);
return wcwidth(wc);
])])
