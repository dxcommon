# Copyright © 2024 Nick Bowler
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# DX_CHECK_GETLINE
#
# Check whether or not the getline function is available.  If it is, the macro
# HAVE_GETLINE is defined to 1 and the cache variable dx_cv_have_getline is set
# to "yes".  Otherwise, dx_cv_have_getline is set to "no".
AC_DEFUN([DX_CHECK_GETLINE],
[AC_CACHE_CHECK([for getline], [dx_cv_have_getline],
  [AC_LINK_IFELSE([AC_LANG_PROGRAM([#include <stdio.h>
], [ssize_t (*x)() = getline;
char *p = 0;
size_t n = 0;
return getline(&p, &n, stdin);
])], [dx_cv_have_getline=yes], [dx_cv_have_getline=no])])
AS_CASE([$dx_cv_have_getline], [yes],
  [AC_DEFINE([HAVE_GETLINE], [1],
    [Define to 1 if the getline function is available.])])])
