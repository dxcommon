# Copyright © 2024 Nick Bowler
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# DX_COPYRIGHT([license-id], [blurb])
#
# Helper for license notices in configure scripts and test suites.  This works
# similar to AC_COPYRIGHT and AT_COPYRIGHT and can be used in place of either.
#
# Unlike AC/AT_COPYRIGHT, the complete blurb is copied into the script comments
# but the --version output is a simplified form, constructed as follows:
#
#   - Lines of the blurb containing the word "Copyright" are printed, with all
#     years but the last deleted.
#
#   - An abbreviated form of the license name is printed, selected by the
#     license-id (GPLv2+ or GPLv3+).
#
#   - A brief note that the program is free software and is provided without
#     any warranty.
#
# Neither argument is subject to any macro expansion.  This macro is only
# recognized with parameters.
#
# For syntactic convenience, both leading and trailing blank lines are stripped
# from the blurb, and each line may optionally begin with a # character.
m4_define([DX_COPYRIGHT], [m4_if([$#], [0], [[$0]], [m4_wrap(
[_DX_COPYRIGHT([$1], m4_bpatsubst([$2], [\`[
]*\(\([^x]\|x\)*\)[
]*\'], [[
\1]]))])])])

m4_define([_DX_COPYRIGHT],
[m4_divert_text(m4_ifdef([AC_INIT], [VERSION_USER], [VERSION_NOTICES]), [
_DX_COPYRIGHT_BRIEF(_DX_COPYRIGHT_SPLIT([$2]))_DX_LICENSE_BRIEF([GPLv3+])])dnl
m4_divert_text([HEADER-COPYRIGHT],
  [m4_bpatsubst([$2], [^\([^#]\|$\)], [# \1])])])

m4_define([_DX_COPYRIGHT_SPLIT], [m4_unquote(
  m4_changequote(<~~,~~>)[m4_bpatsubst(
    <~~<~~$1~~>~~>,<~~Copyright~~>,<~~],[Copyright~~>)]m4_changequote([,]))])

m4_define([_DX_COPYRIGHT_BRIEF], [m4_if([$#], [1],
  [m4_bregexp([$1],
    [\(Copyright[^0-9]*\).*\([0-9][0-9][0-9][0-9].*\)], [[\1\2
]])], [m4_map_args([_DX_COPYRIGHT_BRIEF], $@)])])

m4_define([_DX_LICENSE_NAME],
  [m4_if([$#], [2], [m4_define([$0($1)], [: $2])], [m4_defn([$0($1)])])])

m4_define([_DX_LICENSE_BRIEF], [[License $1]_DX_LICENSE_NAME([$1])[.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.]])

_DX_LICENSE_NAME([GPLv2+],
  [GNU General Public License version 2 or any later version])
_DX_LICENSE_NAME([GPLv3+],
  [GNU General Public License version 3 or any later version])

# Redefine with AC_DEFUN so aclocal will pich this up, while
# allowing this file to be included by autotest as well where
# this line will have no effect.
AC_DEFUN([DX_COPYRIGHT], m4_defn([DX_COPYRIGHT]))
