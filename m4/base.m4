# Copyright © 2012, 2014, 2021, 2023-2024 Nick Bowler
#
# Basic macros for dxcommon.
#
# License WTFPL2: Do What The Fuck You Want To Public License, version 2.
# This is free software: you are free to do what the fuck you want to.
# There is NO WARRANTY, to the extent permitted by law.

AC_PREREQ([2.64])

m4_pattern_forbid([^_?DX_])

dnl _DX_BASE_FILE
dnl
dnl This macro identifies the filename of this file, used to automatically
dnl determine the dxcommon base directory.  It is a literal string, thus it
dnl should not be expanded directly; use m4_defn([_DX_BASE_FILE]) instead.
m4_define([_DX_BASE_FILE], __file__)

dnl _DX_SET_BASEDIR([directory])
dnl
dnl Defines the macro _DX_BASEDIR to the dxcommon base directory.  If the
dnl argument is empty, the base directory will be determined automatically.
dnl Otherwise, the base directory is set to the argument (which is not
dnl subject to macro expansion).
dnl
dnl Quoting here is tricky as we must avoid macro expansion of _DX_BASE_FILE,
dnl but m4_bpatsubst does not quote its result.  The resulting _DX_BASEDIR
dnl macro is a literal string, thus it should not be expanded directly; use
dnl m4_defn([_DX_BASEDIR]) instead.
m4_define([_DX_SET_BASEDIR], [m4_do(
  [m4_define([_DX_BASEDIR], m4_ifval([$1], [[$1]],
    [m4_bpatsubst(m4_dquote(m4_defn([_DX_BASE_FILE])),
      [/m4/[^/]*\.m4\(.\)$], [\1])]))],
  [_DX_STAMP])])

dnl Include the stamp file to force failure at autoconf time if it does
dnl not exist in the checkout.  We circumvent m4_include to avoid warnings
dnl about multiple inclusions.
m4_define([_DX_STAMP], [m4_do(
  [m4_pushdef([m4_include], [m4_builtin([include], $][@)])],
  [m4_include(m4_defn([_DX_BASEDIR])[/m4/dx-stamp.m4])],
  [m4_popdef([m4_include])])])

dnl DX_INIT(directory)
dnl
dnl Sets the dxcommon base directory, which is used by other macros to
dnl find any source files that they may need.  It is not normally necessary
dnl to use this macro.  If it used, it must be expanded prior to any other
dnl dxcommon macro.  Otherwise, the base directory will be determined
dnl automatically.
dnl
dnl The directory argument is a literal string, not subject to macro expansion.
dnl
dnl Autoconf versions <= 2.69 have a bug which causes the first argument of
dnl AC_SUBST to be expanded as a macro when using the two-argument form.  This
dnl bug has been fixed upstream but as of September, 2014 there is no release
dnl which includes the fix.
AC_DEFUN_ONCE([DX_INIT], [m4_do(
  [_DX_SET_BASEDIR([$1])],
  [AC_SUBST([DX_BASEDIR])],
  [AC_CONFIG_COMMANDS_PRE(
    [[DX_BASEDIR]="\${top_srcdir}/AS_ESCAPE(
      m4_dquote(m4_defn([_DX_BASEDIR])))"])])])

dnl DX_BASEDIR
dnl
dnl This macro expands to the dxcommon base directory, as a quoted string.
AC_DEFUN([DX_BASEDIR], [m4_ignore(DX_INIT())m4_defn([_DX_BASEDIR])])

# DX_RUN_LOG(command)
#
# Run a command, logging all of the command, its output, and overall
# exit status to config.log.  The expansion of this macro is a single
# complex shell command suitable for use in shell conditionals.
AC_DEFUN([DX_RUN_LOG],
[{ _dx_shopt=$-
( set -x
$1
) >&AS_MESSAGE_LOG_FD 2>&1
_dx_status=$?
test x"$-" = x"$_dx_shopt" || set +x # work around DJGPP subshell bug
AS_ECHO(["\$? = $_dx_status"]) >&AS_MESSAGE_LOG_FD
test $_dx_status = 0; } 2>/dev/null])

# DX_PATCH_MACRO([macro-name], [regexp], [replacement])
#
# Patches the definition of macro-name by replacing substrings that match
# the given regexp (a la m4_bpatsubst).
AC_DEFUN([DX_PATCH_MACRO], [m4_ifdef([$1],
  [m4_define([$1], m4_bpatsubst(m4_dquote(m4_defn([$1])), [$2], [$3]))])])
