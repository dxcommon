dnl Copyright © 2018 Nick Bowler
dnl
dnl License WTFPL2: Do What The Fuck You Want To Public License, version 2.
dnl This is free software: you are free to do what the fuck you want to.
dnl There is NO WARRANTY, to the extent permitted by law.

dnl DX_LIB_CNEARTREE([min-version], [action-if-ok], [acton-if-failed])
dnl
dnl Search for a working CNearTree installation.  If such a library is found,
dnl appropriate compiler and linker flags are stored in CNEARTREE_CFLAGS and
dnl CNEARTREE_LIBS, respectively, and the action-if-ok is performed (may be
dnl empty).  If no suitable library is found, then action-if-failed is
dnl performed if non-empty, otherwise configure will stop with a fatal error.
AC_DEFUN([DX_LIB_CNEARTREE],
[DX_LIB_SETUP([cneartree])AC_MSG_CHECKING([for libCNearTree])
DX_LIB_SEARCH_LINK([cneartree],
  [m4_apply([_DX_LIB_CNEARTREE_TEST], m4_split([$1], [\.]))],
  [ [[$CNEARTREE_CFLAGS], [$CNEARTREE_LIBS]],
    [[], [-lCNearTree]],
    [[], [-lCNearTree -lCVector]] ])
AS_IF([test x"$dx_cv_cneartree_lib_found" = x"yes"], [$2],
  [m4_default([$3], [AC_MSG_FAILURE([dnl
libCNearTree is required.  The latest
version may be found at <http://neartree.sourceforge.net/>.
m4_newline([DX_LIB_USERFLAG_BLURB([cneartree])])
])])])])

dnl Internal test program for CNearTree.  The version test is currently
dnl unimplemented.
m4_define([_DX_LIB_CNEARTREE_TEST], [AC_LANG_PROGRAM([dnl
#include <CNearTree.h>
], [dnl
CNearTreeHandle t;
CVectorHandle v;
void *p;
CNearTreeCreate(&t, 2, CNEARTREE_TYPE_DOUBLE|CNEARTREE_NORM_L2);
CNearTreeObjects(t, &v);
CVectorGetElement(v, &p, 0);
CNearTreeFree(&t);
])])
