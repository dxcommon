# Monkey patches for gnulib macros.
#
# Copyright © 2023-2024 Nick Bowler
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# DX_PATCH_GNULIB([extra-patches])
#
# Apply the default set of patches, plus any optional extra named patches.
AC_DEFUN([DX_PATCH_GNULIB],
[m4_foreach_w([patch],
m4_defn([_DX_GL_PATCHES])m4_default_nblank_quoted([ $1]),
[m4_indir([_DX_GL_PATCH(]m4_defn([patch])[)])])])

m4_define([_DX_GL_PATCHES])
m4_define([_DX_GL_PATCH],
  [m4_define([_DX_GL_PATCH($1)], [$2])m4_define([_DX_GL_PATCHES],
    m4_defn([_DX_GL_PATCHES])[ $1])])

m4_define([_DX_GL_OPTIONAL_PATCH], [m4_define([_DX_GL_PATCH($1)], [$2])])

# Patch if ! ... constructs produced by gnulib conditional dependencies
# as these fail in Solaris 10 /bin/sh.
_DX_GL_PATCH([gl-cond-deps],
  [DX_PATCH_MACRO([gl_INIT],
    [if ! *\(.*gnulib_enabled[^;]*\); then],
    [if \1; then :; else])])

# Patch old versions of threadlib.m4, which have a quoting bug preventing
# correct determination of PTHREAD_IN_USE_DETECTION_HARD on Solaris 9 and
# earlier.  If this is set incorrectly, libcdecl will crash on error.
_DX_GL_PATCH([gl-threadlib-solaris-fix],
  [DX_PATCH_MACRO([gl_PTHREADLIB_BODY],
    [solaris2\.\[1-9\]], [solaris2.@<:@1-9@:>@])])

# Patch old versions of threadlib to default to native Windows threads.
# This is the default behaviour in current upstream gnulib, and avoids
# pulling in extra libraries by default on MinGW.
_DX_GL_PATCH([gl-threadlib-mingw-default],
  [DX_PATCH_MACRO([gl_THREADLIB_EARLY_BODY],
    [\[gl_use_winpthreads_default=\]],
    [[gl_use_winpthreads_default=no]])])

# Remove silly warning flag probes that are included unconditionally in newer
# gnulib and bloat downstream configure scripts for no real reason.
_DX_GL_PATCH([gl-no-warnings],
  [DX_PATCH_MACRO([gl_INIT], [AC_REQUIRE(\[gl_CC_GNULIB_WARNINGS\])], [dnl])])

# Avoid gnulib stdint.m4 from forcing HAVE_(UNSIGNED_)LONG_LONG_INT as this
# breaks anything which actually tries to use AC_TYPE_LONG_LONG_INT to work
# with missing long long, and is otherwise pointless.
#
# Gnulib itself doesn't care about these definitions and unconditionally uses
# long long.  So by itself, this won't fix any failures in Gnulib due to an
# actually missing 'long long' but also should not create new problems.
_DX_GL_PATCH([stdint-no-fake-longlong],
  [DX_PATCH_MACRO([gl_STDINT_H],
    [AC_DEFINE(\[HAVE[_UNSIGNED]*_LONG_LONG_INT\][^)]*.], [dnl])])

# Don't expand the gnulib "common" definitions.  This removes a bunch of
# configure probes and a large amount of config.h noise, but other parts
# of Gnulib may implicitly depend on these so your mileage may vary.
#
# In particular:
#
#  - Removes Amsterdam Compiler Kit probe which seems very incomplete and
#    broken.  It should be possible to support this compiler by setting
#    the appropriate environment variables or the config.site mechanism.
#
#  - Removes clang probes which seem to be something related to probing
#    with the C compiler then using the results with the C++ compiler.
#
#  - Removes a bunch of really noisy config.h definitions.
#
# Note that if you use this, you should also delete zzgnulib.m4 after
# running gnulib-tool, as its mere presense will confuse aclocal and
# cause extra files to be distributed (even though this patch will
# disable all its effectS).
_DX_GL_OPTIONAL_PATCH([no-gnulib-common],
[DX_PATCH_MACRO([gl_EARLY], [AC_REQUIRE(\[gl_PROG_AR_RANLIB\])], [dnl])dnl
DX_PATCH_MACRO([gl_INIT], [gl_COMMON], [dnl])])
