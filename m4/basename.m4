dnl Copyright © 2021 Nick Bowler
dnl
dnl License WTFPL2: Do What The Fuck You Want To Public License, version 2.
dnl This is free software: you are free to do what the fuck you want to.
dnl There is NO WARRANTY, to the extent permitted by law.

dnl DX_BASENAME(variable, string)
dnl
dnl If string, which must be a single shell word, contains a / character,
dnl variable is assigned to the portion of string that follows the last
dnl such character.  Otherwise, variable is set to string.
AC_DEFUN([DX_BASENAME],
[AC_REQUIRE([_DX_BASENAME_SETUP])dnl
dx_fn_basename $1 $2])

AC_DEFUN_ONCE([_DX_BASENAME_SETUP], [m4_divert_push([INIT_PREPARE])dnl
dx_fn_basename () {
  _dx_tmp=; { _dx_tmp=${2##*/}; } 2>/dev/null
  AS_CASE([$_dx_tmp], [""], [_dx_save_IFS=$IFS; IFS=/
    for _dx_tmp in $][2; do :; done
    IFS=$_dx_save_IFS])
  AS_VAR_SET([$][1], [$_dx_tmp])
}
m4_divert_pop()])
