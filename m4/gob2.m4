dnl Copyright © 2020-2021 Nick Bowler
dnl
dnl License WTFPL2: Do What the Fuck You Want To Public License, version 2.
dnl This is free software: you are free to do what the fuck you want to.
dnl There is NO WARRANTY, to the extent permitted by law.

dnl DX_PROG_GOB2([min-version], [action-if-found], [action-if-not-found])
dnl
dnl Search PATH for a working gob2 utility with the given minimum version,
dnl which may be empty to accept any version.  If found, the GOB2 variable
dnl (which is substituted with AC_SUBST) is set with the result, and
dnl action-if-found is executed.  Otherwise, action-if-not-found is
dnl executed if nonempty, else configure will exit with a fatal error.

AC_DEFUN([DX_PROG_GOB2], [AC_ARG_VAR([GOB2], [GOBject Builder command])dnl
AC_PREREQ([2.62])dnl
cat >conftest.gob <<'EOF'
m4_ifnblank([$1], [requires $1
])dnl
class :Conftest from G:Object
{
}
EOF
AC_CACHE_CHECK([for GObject Builder], [ac_cv_path_GOB2],
[dx_cv_gob_found=:
AC_PATH_PROGS_FEATURE_CHECK([GOB2], [gob2],
[rm -f conftest.c
$ac_path_GOB2 conftest.gob >&AS_MESSAGE_LOG_FD 2>&1 && test -f conftest.c && {
  DX_BASENAME([gob_relcmd], ["$ac_path_GOB2"])
  gob_bypath=`{ command -v "$gob_relcmd"; } 2>/dev/null` # ''
  ac_cv_path_GOB2=$ac_path_GOB2
  test x"$gob_bypath" = x"$ac_path_GOB2" && ac_cv_path_GOB2=$gob_relcmd
  ac_path_GOB2_found=:
  dx_cv_gob_works=yes
}], [ac_cv_path_GOB2=no dx_cv_gob_found=false])
])
AS_IF([$dx_cv_gob_found],
[GOB2=$ac_cv_path_GOB2
AC_CACHE_CHECK([whether $GOB2 works], [dx_cv_gob_works],
[rm -f conftest.c
if $GOB2 conftest.gob >&AS_MESSAGE_LOG_FD 2>&1 && test -f conftest.c; then
  dx_cv_gob_works=yes
else
  dx_cv_gob_works=no
fi])])
AS_IF([test x"$dx_cv_gob_works" = x"yes"],
  [$2], [m4_default([$3], [AC_MSG_FAILURE(
[GObject Builder m4_ifnblank([$1], [version at least $1 ])is required.

The latest version may be found at <https://draconx.ca/projects/gob-dx/>.
])])])
rm -f conftest.c conftest.gob
])
