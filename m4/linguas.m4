dnl Copyright © 2011, 2021-2023 Nick Bowler
dnl
dnl Computes the set of .po and .mo files based on the LINGUAS environment
dnl variable.  The variable POFILES is set to the complete list of .po files,
dnl according to the LINGUAS file in the po directory.  The variable MOFILES
dnl is set to the list of .mo files that the user has requested be installed.
dnl Both POFILES and MOFILES are AC_SUBSTed.
dnl
dnl License WTFPL2: Do What The Fuck You Want To Public License, version 2.
dnl This is free software: you are free to do what the fuck you want to.
dnl There is NO WARRANTY, to the extent permitted by law.

AC_DEFUN([DX_LINGUAS],
[AC_REQUIRE([AM_GNU_GETTEXT])dnl
AC_REQUIRE([DX_PROG_JOIN])dnl
AC_REQUIRE([AC_PROG_AWK])dnl

POFILES=
MOFILES=

if test -f "$srcdir/po/LINGUAS"; then
  $AWK '{ sub(/#.*$/, "")
    for (i = 1; i <= NF; i++) {
      print $(i)
    }
  }' "$srcdir/po/LINGUAS" | LC_ALL=C sort -u >conftest.all

  : "${LINGUAS=all}"
  if test x"$LINGUAS" = x"all"; then
    cp conftest.all conftest.ena
  else
    :; {
      for arg in $LINGUAS; do
        # Ensure that if "la_CC"-style code is requested then so is plain "la".
        printf '%s\n' "$arg"
        case $arg in
        *_*)
          save_IFS=$IFS; IFS=_
          set x $arg
          IFS=$save_IFS
          AS_ECHO(["${2}"])
          ;;
        esac
      done
    } | LC_ALL=C sort -u >conftest.ena
  fi

  exec 3<conftest.all
  while read x <&3; do
    AS_VAR_APPEND([POFILES], ["${POFILES:+ }po/$x.po"])
  done

  if test x"$USE_NLS" = x"yes"; then
    LC_ALL=C $JOIN conftest.ena conftest.all >conftest.out
    exec 3<conftest.out
    while read x <&3; do
      AS_VAR_APPEND([MOFILES], ["${MOFILES:+ }po/$x.mo"])
    done
  fi

  exec 3<&-
  rm -f conftest.all conftest.ena conftest.out
fi

AC_SUBST([POFILES])
AC_SUBST([MOFILES])
])
