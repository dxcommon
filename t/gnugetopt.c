/*
 * Wrapper to run the getopt_long tests from Gnulib.
 *
 * Copyright © 2024 Nick Bowler
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * Can be run with an optional "posix" or "gnu" argument to select between
 * different test modes.
 *
 * If run in the default gnu mode, POSIXLY_CORRECT must be unset in the
 * environment.  Otherwise, in posix mode, POSIXLY_CORRECT must be set.
 *
 */

#undef HAVE_GETOPT_LONG
#undef HAVE_GETOPT_LONG_ONLY

#include "gnu_getopt.h"

#include <stdlib.h>
#include <string.h>
#include "tap.h"

#undef no_argument
#define no_argument 0

#undef required_argument
#define required_argument 1

#undef optional_argument
#define optional_argument 2

#define FALLTHROUGH /**/

#if HAVE_STRINGIZE
#  define ASSERT(x) tap_result(x, "%s:%d: %s", __FILE__, __LINE__, #x);
#else
#  define ASSERT(x) tap_result(x, "%s:%d", __FILE__, __LINE__);
#endif

#include "gnugetopt_test.h"

static void check_mode(int posix_mode, int posixly_correct)
{
	if (posix_mode == posixly_correct)
		return;

	if (!posix_mode)
		tap_diag("Please remove POSIXLY_CORRECT from the environment");
	else
		tap_diag("Please set POSIXLY_CORRECT in the environment");

	tap_skip_all("POSIXLY_CORRECT environment mismatch");
}

static void usage(void)
{
	tap_bail_out("Usage: t/gnu_getopt [posix|gnu]");
}

int main(int argc, char **argv)
{
	int posixly_correct = !!getenv("POSIXLY_CORRECT");

	if (argc == 2) {
		if (!strcmp(argv[1], "posix"))
			check_mode(1, posixly_correct);
		else if (!strcmp(argv[1], "gnu"))
			check_mode(0, posixly_correct);
		else
			usage();
	} else if (argc > 2) {
		usage();
	} else {
		check_mode(0, posixly_correct);
	}

	if (getenv("POSIXLY_CORRECT"))
		test_getopt_long_posix();
	else
		test_getopt_long();

	test_getopt_long_only();

	tap_done();
}
