/*
 * Copyright © 2015, 2023 Nick Bowler
 *
 * Test application to verify 64-bit signed unpacking functions.
 *
 * License WTFPL2: Do What The Fuck You Want To Public License, version 2.
 * This is free software: you are free to do what the fuck you want to.
 * There is NO WARRANTY, to the extent permitted by law.
 */

#include "pack.h"
#include "tap.h"

static const unsigned char zero[8];
static const unsigned char minus_one[8] = {
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff
};

static const unsigned char min[9] = {
	0x80, 0, 0, 0, 0, 0, 0, 0, 0x80
};

static const unsigned char test_pattern[8] = {
	0xde, 0xad, 0xbe, 0xef, 0xf0, 0x0d, 0xca, 0xfe
};

#define test(func, pattern, expected) do { \
	long long result__ = (func)(pattern); \
	if (!tap_result(result__ == (expected), "%s(%s)", #func, #expected)) { \
		tap_diag("  expected: %lld", (long long)(expected)); \
		tap_diag("  actual:   %lld", result__); \
	} \
} while (0)

int main(void)
{
#if PACK_HAVE_64BIT
	tap_plan(8);

	test(unpack_s64_be, zero,          0);
	test(unpack_s64_be, minus_one,    -1);
	test(unpack_s64_be, test_pattern, -2401053088584709378ll);
	test(unpack_s64_be, min,          -9223372036854775807ll-1);

	test(unpack_s64_le, zero,          0);
	test(unpack_s64_le, minus_one,    -1);
	test(unpack_s64_le, test_pattern, -87241914314740258ll);
	test(unpack_s64_le, min+1,        -9223372036854775807ll-1);
#else
	tap_skip_all("no 64-bit support");
#endif

	tap_done();
}
