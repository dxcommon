#!/bin/sh
#
# Copyright © 2023 Nick Bowler
#
# Fake msgfmt program for testing.
#
# License WTFPL2: Do What The Fuck You Want To Public License, version 2.
# This is free software: you are free to do what the fuck you want to.
# There is NO WARRANTY, to the extent permitted by law.

outfile=messages.mo
for arg
do
  arg=${lastarg:+"$lastarg="}$arg
  lastarg=

  save_IFS=$IFS; IFS='='
  set x $arg; shift; shift; optarg=$*
  IFS=$save_IFS

  case $arg in
  -o|--output-file) lastarg=--output-file ;;
  --output-file=*) outfile=$optarg ;;
  esac
done

exec 1>"$outfile"
