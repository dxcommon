/*
 * Stub gettext bits for test purposes.
 *
 * Copyright © 2023-2024 Nick Bowler
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef TEST_NLS_LIBINTL_H_
#define TEST_NLS_LIBINTL_H_

#include <stdio.h>
#include <stdarg.h>

/*
 * We don't need a real snprintf for testing help functionality; substitute
 * using vsprintf to ease testing on C89 implementations.
 */
static int test_do_snprintf(char *buf, size_t n, const char *fmt, ...)
{
	va_list ap;
	int ret;

	va_start(ap, fmt);
	ret = vsprintf(buf, fmt, ap);
	va_end(ap);

	return ret;
}
#define help_do_snprintf(x) test_do_snprintf x

/*
 * Test gettext wrappers which return the string unmodified, but also
 * initialize the locale as a side effect (not thread safe).
 *
 * Try to tickle a crash if a bogus string argument is passed as the real
 * functions dereference the provided pointers.
 */

#define gettext(s) test_gettext(s)
const char *gettext(const char *s);

#endif
