/*
 * Copyright © 2015, 2023 Nick Bowler
 *
 * Test application to verify 64-bit unsigned unpacking functions.
 *
 * License WTFPL2: Do What The Fuck You Want To Public License, version 2.
 * This is free software: you are free to do what the fuck you want to.
 * There is NO WARRANTY, to the extent permitted by law.
 */

#include "pack.h"
#include "tap.h"

static const unsigned char zero[8];
static const unsigned char minus_one[8] = {
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff
};

static const unsigned char test_pattern[8] = {
	0xde, 0xad, 0xbe, 0xef, 0xf0, 0x0d, 0xca, 0xfe
};

#define test(func, pattern, expected) do { \
	unsigned long long result__ = (func)(pattern); \
	if (!tap_result(result__ == (expected), "%s(%s)", #func, #expected)) { \
		tap_diag("  expected: %llu", (unsigned long long)(expected)); \
		tap_diag("  actual:   %llu", result__); \
	} \
} while (0)

int main(void)
{
#if PACK_HAVE_64BIT
	tap_plan(6);

	test(unpack_64_be, zero,         0);
	test(unpack_64_be, minus_one,    0xffffffffffffffffll);
	test(unpack_64_be, test_pattern, 0xdeadbeeff00dcafell);
	test(unpack_64_le, zero,         0);
	test(unpack_64_le, minus_one,    0xffffffffffffffffll);
	test(unpack_64_le, test_pattern, 0xfeca0df0efbeaddell);
#else
	tap_skip_all("no 64-bit support");
#endif

	tap_done();
}
