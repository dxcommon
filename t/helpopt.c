/*
 * Test helper application to verify help_print_optstring operation.
 *
 * Copyright © 2021-2022, 2024 Nick Bowler
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * Command-line arguments are collected to create the options to be formatted.
 * There are six basic forms:
 *
 *   --long-option-only
 *   --long-option-with-argument arg
 *   --long-option-with-optional-argument [arg]
 *   --short-option -s arg
 *   --short-option-with-argument -s arg
 *   --short-option-with-optional-argument -s [arg]
 *
 * Adding an argument of '&' to any form will set the "flag" member of
 * the option structure to a non-null value.
 *
 * Based on these arguments, a suitable 'struct option' is constructed and
 * passed to help_print_optstring.  Then a tab character is printed, followed
 * by the return value of help_print_optsring (via printf %d).  Then further
 * arguments are considered to output more options.
 *
 * Initially, the "l" value passed to help_print_optstring is 20.  This can be
 * changed at any point by a numeric command-line argument, which will set a
 * new value that applies to all subsequent calls until it is changed again.
 */

#include "help.h"
#include "tap.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <errno.h>

#include "gnu_getopt.h"

int arg_to_int(const char *s)
{
	char *end;
	long val;

	errno = 0;
	val = strtol(s, &end, 0);
	if (*end != 0)
		tap_bail_out("%s: numeric argument expected", s);
	else if (val < INT_MIN || val > INT_MAX || errno == ERANGE)
		tap_bail_out("%s: %s", s, strerror(ERANGE));
	else if (errno)
		tap_bail_out("%s: %s", s, strerror(errno));

	return val;
}

void print_opt(struct option *opt, const char *argname, int w)
{
	w = help_print_optstring(opt, argname, w);
	printf("\t%d\n", w);
}

int main(int argc, char **argv)
{
	struct option opt = {0};
	const char *argname = 0;
	int i, w = 20;

	for (i = 1; i < argc; i++) {
		if (argv[i][0] == '-' && argv[i][1] == '-') {
			if (opt.name)
				print_opt(&opt, argname, w);
			opt.val = UCHAR_MAX+1;
			opt.has_arg = 0;
			opt.name = argv[i]+2;
			opt.flag = NULL;
		} else if (argv[i][0] == '-') {
			opt.val = argv[i][1];
		} else if (argv[i][0] == '[') {
			char *c;

			argname = argv[i]+1;
			if ((c = strchr(argname, ']')))
				*c = 0;
			opt.has_arg = 2;
		} else if (argv[i][0] == '&') {
			static int dummy;
			opt.flag = &dummy;
		} else if (argv[i][0] >= '0' && argv[i][0] <= '9') {
			w = arg_to_int(argv[i]);
		} else {
			argname = argv[i];
			opt.has_arg = 1;
		}
	}

	if (opt.name)
		print_opt(&opt, argname, w);

	return 0;
}
