#!/bin/sh
#
# Copyright © 2020 Nick Bowler
#
# Fake flex program for testing the flex detection macro.
#
# License WTFPL2: Do What The Fuck You Want To Public License, version 2.
# This is free software: you are free to do what the fuck you want to.
# There is NO WARRANTY, to the extent permitted by law.

: "${FAKE_FLEX_VERSION=2.0.0}"

argv0=$0

infile=
outfile=lex.yy.c
for arg
do
  shift

  case $arg in
  -o) outfile=$1; shift ;;
  *) set x "$@" "$arg"; shift ;;
  esac
done

for arg
do
  case $arg in
  *.l) infile=$arg ;;
  esac
done

if test x"$infile" = x""; then
  printf '%s: error: no input file\n' "$argv0" 1>&2
  exit 1
fi

copyout=false
exec 3<"$infile" 4>"$outfile"

save_IFS=$IFS IFS=.
set x $FAKE_FLEX_VERSION 0 0 0; shift
IFS=$save_IFS

cat >&4 <<EOF
#define FLEX_SCANNER
#define YY_FLEX_MAJOR_VERSION $1
#define YY_FLEX_MINOR_VERSION $2
#define YY_FLEX_SUBMINOR_VERSION $3
EOF

while read line <&3; do
  case $line in
  %{) copyout=true ;;
  %}) copyout=false ;;
  %%) break ;;
  *) $copyout && printf '%s\n' "$line" >&4 ;;
  esac
done

cat >&4 <<EOF
int yylex(void)
{
  return 0;
}
EOF

while read line <&3; do
  case $line in
  %%) break ;;
  esac
done

cat <&3 >&4
