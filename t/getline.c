/*
 * Copyright © 2024 Nick Bowler
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#undef HAVE_GETLINE
#define DX_GETLINE_INITIAL_ALLOC 2
#include "getline.h"
#include "tap.h"

#include <errno.h>

#define LONGLINE \
"this line is pretty long and may require scaling the buffer multiple times"

static const char testdata[] = "\n\
this line is pretty long and may require scaling the buffer multiple times\n\
this line is pretty long and may require scaling the buffer multiple times\n\
this line is pretty long and may require scaling the buffer multiple times\n\
hellorld\n";

int main(void)
{
	char *line = NULL;
	size_t long_size;
	size_t n = 0;
	FILE *f;
	int rc;

	if (!(f = tmpfile()))
		tap_bail_out("tmpfile failed: %s\n", strerror(errno));

	if (fwrite(testdata, sizeof testdata-1, 1, f) != 1)
		tap_bail_out("fwrite failed: %s\n", strerror(errno));

	if (fflush(f) != 0)
		tap_bail_out("fflush failed: %s\n", strerror(errno));

	if (fseek(f, 0, SEEK_SET) != 0)
		tap_bail_out("fseek failed: %s\n", strerror(errno));

	/* First line: empty */
	rc = dx_getline(&line, &n, f);
	if (!tap_result(rc == DX_GETLINE_OK, "dx_getline")) {
		tap_diag("Failed, unexpected result");
		tap_diag("   Received: %d", rc);
		tap_diag("   Expected: %d (DX_GETLINE_OK)", DX_GETLINE_OK);
	}
	if (!tap_result(n == 2, "alloc size")) {
		tap_diag("Failed, unexpected result");
		tap_diag("   Received: %lu", (unsigned long)n);
		tap_diag("   Expected: 2");
	}
	if (!tap_result(line[0] == 0, "returned string")) {
		tap_diag("Failed, unexpected result");
		tap_diag("   Received: \"%.*s\"", (int)n, line);
		tap_diag("   Expected: \"\"");
	}

	/* Next line: long line (buffer needs expanding) */
	rc = dx_getline(&line, &n, f);
	if (!tap_result(rc == DX_GETLINE_OK, "dx_getline")) {
		tap_diag("Failed, unexpected result");
		tap_diag("   Received: %d", rc);
		tap_diag("   Expected: %d (DX_GETLINE_OK)", DX_GETLINE_OK);
	}
	if (!tap_result(n > sizeof LONGLINE, "alloc size")) {
		tap_diag("Failed, unexpected result");
		tap_diag("   Received: %lu", (unsigned long)n);
		tap_diag("   Expected: >%u", (unsigned)(sizeof LONGLINE));
	}
	if (!tap_result(!strcmp(line, LONGLINE), "returned string")) {
		tap_diag("Failed, unexpected result");
		tap_diag("   Received: \"%.*s\"", (int)n, line);
		tap_diag("   Expected: \"%s\"", LONGLINE);
	}

	/* Next line: long line (buffer does not need expanding) */
	long_size = n;
	rc = dx_getline(&line, &n, f);
	if (!tap_result(rc == DX_GETLINE_OK, "dx_getline")) {
		tap_diag("Failed, unexpected result");
		tap_diag("   Received: %d", rc);
		tap_diag("   Expected: %d (DX_GETLINE_OK)", DX_GETLINE_OK);
	}
	if (!tap_result(n == long_size, "alloc size")) {
		tap_diag("Failed, unexpected result");
		tap_diag("   Received: %lu", (unsigned long)n);
		tap_diag("   Expected: %lu", (unsigned long)long_size);
	}
	if (!tap_result(!strcmp(line, LONGLINE), "returned string")) {
		tap_diag("Failed, unexpected result");
		tap_diag("   Received: \"%.*s\"", (int)n, line);
		tap_diag("   Expected: \"%s\"", LONGLINE);
	}

	/* Next line: long line (new buffer allocation) */
	free(line); line = NULL; n = 0;
	rc = dx_getline(&line, &n, f);
	if (!tap_result(rc == DX_GETLINE_OK, "dx_getline")) {
		tap_diag("Failed, unexpected result");
		tap_diag("   Received: %d", rc);
		tap_diag("   Expected: %d (DX_GETLINE_OK)", DX_GETLINE_OK);
	}
	if (!tap_result(n == long_size, "alloc size")) {
		tap_diag("Failed, unexpected result");
		tap_diag("   Received: %lu", (unsigned long)n);
		tap_diag("   Expected: %lu", (unsigned long)long_size);
	}
	if (!tap_result(!strcmp(line, LONGLINE), "returned string")) {
		tap_diag("Failed, unexpected result");
		tap_diag("   Received: \"%.*s\"", (int)n, line);
		tap_diag("   Expected: \"%s\"", LONGLINE);
	}

	/* Next line: short line */
	rc = dx_getline(&line, &n, f);
	if (!tap_result(rc == DX_GETLINE_OK, "dx_getline")) {
		tap_diag("Failed, unexpected result");
		tap_diag("   Received: %d", rc);
		tap_diag("   Expected: %d (DX_GETLINE_OK)", DX_GETLINE_OK);
	}
	if (!tap_result(n == long_size, "alloc size")) {
		tap_diag("Failed, unexpected result");
		tap_diag("   Received: %lu", (unsigned long)n);
		tap_diag("   Expected: %lu", (unsigned long)long_size);
	}
	if (!tap_result(!strcmp(line, "hellorld"), "returned string")) {
		tap_diag("Failed, unexpected result");
		tap_diag("   Received: \"%.*s\"", (int)n, line);
		tap_diag("   Expected: \"hellorld\"");
	}

	/* Next line: end of file */
	rc = dx_getline(&line, &n, f);
	if (!tap_result(rc == DX_GETLINE_EOF, "dx_getline (end of file)")) {
		tap_diag("Failed, unexpected result");
		tap_diag("   Received: %d", rc);
		tap_diag("   Expected: %d (DX_GETLINE_EOF)", DX_GETLINE_EOF);
	}
	if (!tap_result(n == long_size, "alloc size")) {
		tap_diag("Failed, unexpected result");
		tap_diag("   Received: %lu", (unsigned long)n);
		tap_diag("   Expected: %lu", (unsigned long)long_size);
	}

	tap_done();
}
