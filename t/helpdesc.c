/*
 * Read some text from standard input and format it with help_print_desc,
 * for testing.  Each pair of program arguments is converted to an int and
 * passed as the two integer arguments to help_print_desc.
 */
#include "help.h"
#include "tap.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <errno.h>

static char buf[1000];

int arg_to_int(const char *s)
{
	char *end;
	long val;

	errno = 0;
	val = strtol(s, &end, 0);
	if (*end != 0)
		tap_bail_out("%s: numeric argument expected", s);
	else if (val < INT_MIN || val > INT_MAX || errno == ERANGE)
		tap_bail_out("%s: %s", s, strerror(ERANGE));
	else if (errno)
		tap_bail_out("%s: %s", s, strerror(errno));

	return val;
}

int main(int argc, char **argv)
{
	long a, b;
	size_t len;
	int i;

	len = fread(buf, 1, sizeof buf - 1, stdin);
	if (len == sizeof buf - 1)
		tap_bail_out("too much input text");
	if (ferror(stdin))
		tap_bail_out("error reading from stdin: %s", strerror(errno));

	for (i = 1; i < argc; i += 2) {
		int indent = arg_to_int(argv[i]);
		int sub = i+1 < argc ? arg_to_int(argv[i+1]) : 0;

		help_print_desc(NULL, buf, indent, sub);
	}

	return 0;
}
