/*
 * Copyright © 2015 Nick Bowler
 *
 * Test application to verify 16 and 32-bit unsigned unpacking functions.
 *
 * License WTFPL2: Do What The Fuck You Want To Public License, version 2.
 * This is free software: you are free to do what the fuck you want to.
 * There is NO WARRANTY, to the extent permitted by law.
 */

#include "pack.h"
#include "tap.h"

static const unsigned char zero[8];
static const unsigned char minus_one[8] = {
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff
};

static const unsigned char test_pattern[8] = {
	0xde, 0xad, 0xbe, 0xef, 0xf0, 0x0d, 0xca, 0xfe
};

#define test(func, pattern, expected) do { \
	unsigned long result__ = (func)(pattern); \
	if (!tap_result(result__ == (expected), "%s(%s)", #func, #expected)) { \
		tap_diag("  expected: %lu", (unsigned long)(expected)); \
		tap_diag("  actual:   %lu", result__); \
	} \
} while (0)

int main(void)
{
	tap_plan(12);

	test(unpack_16_be, zero,         0);
	test(unpack_16_be, minus_one,    0xffff);
	test(unpack_16_be, test_pattern, 0xdead);
	test(unpack_32_be, zero,         0);
	test(unpack_32_be, minus_one,    0xffffffff);
	test(unpack_32_be, test_pattern, 0xdeadbeef);

	test(unpack_16_le, zero,         0);
	test(unpack_16_le, minus_one,    0xffff);
	test(unpack_16_le, test_pattern, 0xadde);
	test(unpack_32_le, zero,         0);
	test(unpack_32_le, minus_one,    0xffffffff);
	test(unpack_32_le, test_pattern, 0xefbeadde);

	tap_done();
}
