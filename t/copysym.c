/*
 * Copyright © 2023-2024 Nick Bowler
 *
 * Tests for the copyright_symbol function.
 *
 * License WTFPL2: Do What The Fuck You Want To Public License, version 2.
 * This is free software: you are free to do what the fuck you want to.
 * There is NO WARRANTY, to the extent permitted by law.
 */

#include <string.h>
#include "tap.h"

#define ENABLE_NLS 1
#include "copysym.h"

static const char *format_str(const char *s)
{
	static char buf[100];
	size_t pos = 0;

	for (; *s && pos < sizeof buf - 4; s++) {
		if (*s == '(' || *s == 'C' || *s == ')') {
			buf[pos++] = *s;
		} else {
			static const char xdigits[16] = "0123456789abcdef";
			unsigned char c = *s;

			buf[pos++] = '\\';
			buf[pos++] = 'x';
			buf[pos++] = xdigits[(c >> 4) & 0xf];
			buf[pos++] = xdigits[c & 0xf];
		}
	}

	buf[pos] = 0;
	return buf;
}

static void do_test(const char *charset, char *expected)
{
	const char *sym = copyright_symbol(charset);
	char *quote = charset ? "\"" : "";

	if (!tap_result(!strcmp(sym, expected), "copyright_symbol(%s%s%s)",
	                quote, charset ? charset : "NULL", quote))
	{
		tap_diag("Failed, unexpected result");
		tap_diag("   Received:  %s", format_str(sym));
		tap_diag("   Expected:  %s", format_str(expected));
	}
}

int main(void)
{
	do_test(NULL, "(C)");
	do_test("ANSI_X3.4-1968", "(C)");
	do_test("ARMSCII-8", "(C)");
	do_test("ASCII", "(C)");
	do_test("BIG5", "(C)");
	do_test("BIG5-HKSCS", "(C)");
	do_test("CP1046", "(C)");
	do_test("CP1124", "(C)");
	do_test("CP1125", "(C)");
	do_test("CP1129", "\xa9");
	do_test("CP1131", "(C)");
	do_test("CP1250", "\xa9");
	do_test("CP1251", "\xa9");
	do_test("CP1252", "\xa9");
	do_test("CP1253", "\xa9");
	do_test("CP1254", "\xa9");
	do_test("CP1255", "\xa9");
	do_test("CP1256", "\xa9");
	do_test("CP1257", "\xa9");
	do_test("CP437", "(C)");
	do_test("CP775", "\xa8");
	do_test("CP850", "\xb8");
	do_test("CP852", "(C)");
	do_test("CP855", "(C)");
	do_test("CP856", "\xb8");
	do_test("CP857", "\xb8");
	do_test("CP861", "(C)");
	do_test("CP862", "(C)");
	do_test("CP864", "(C)");
	do_test("CP865", "(C)");
	do_test("CP866", "(C)");
	do_test("CP869", "\x97");
	do_test("CP874", "(C)");
	do_test("CP922", "\xa9");
	do_test("CP932", "(C)");
	do_test("CP943", "(C)");
	do_test("CP949", "(C)");
	do_test("CP950", "(C)");
	do_test("DEC-HANYU", "(C)");
	do_test("DEC-KANJI", "(C)");
	do_test("EUC-JP", "\x8f\xa2\xed");
	do_test("EUC-KR", "(C)");
	do_test("EUC-TW", "(C)");
	do_test("GB18030", "\x81\x30\x84\x38");
	do_test("GB2312", "(C)");
	do_test("GBK", "(C)");
	do_test("GEORGIAN-PS", "\xa9");
	do_test("HP-ARABIC8", "(C)");
	do_test("HP-GREEK8", "(C)");
	do_test("HP-HEBREW8", "(C)");
	do_test("HP-KANA8", "(C)");
	do_test("HP-ROMAN8", "(C)");
	do_test("HP-TURKISH8", "(C)");
	do_test("ISO-8859-1", "\xa9");
	do_test("ISO-8859-10", "(C)");
	do_test("ISO-8859-11", "(C)");
	do_test("ISO-8859-13", "\xa9");
	do_test("ISO-8859-14", "\xa9");
	do_test("ISO-8859-15", "\xa9");
	do_test("ISO-8859-2", "(C)");
	do_test("ISO-8859-3", "(C)");
	do_test("ISO-8859-4", "(C)");
	do_test("ISO-8859-5", "(C)");
	do_test("ISO-8859-6", "(C)");
	do_test("ISO-8859-7", "\xa9");
	do_test("ISO-8859-8", "\xa9");
	do_test("ISO-8859-9", "\xa9");
	do_test("JOHAB", "(C)");
	do_test("KOI8-R", "\xbf");
	do_test("KOI8-T", "\xbf");
	do_test("KOI8-U", "\xbf");
	do_test("PT154", "\xa9");
	do_test("SHIFT_JIS", "(C)");
	do_test("TCVN5712-1", "(C)");
	do_test("TIS-620", "(C)");
	do_test("UTF-8", "\xc2\xa9");
	do_test("VISCII", "(C)");

	do_test("CP1026", "(C)"); /* EBCDIC B4 */
	do_test("CP1047", "(C)"); /* EBCDIC B4 */
	do_test("CP1112", "(C)"); /* EBCDIC B4 */
	do_test("CP1122", "(C)"); /* EBCDIC B4 */
	do_test("CP1130", "(C)"); /* EBCDIC B4 */
	do_test("CP1140", "(C)"); /* EBCDIC B4 */
	do_test("CP1141", "(C)"); /* EBCDIC B4 */
	do_test("CP1142", "(C)"); /* EBCDIC B4 */
	do_test("CP1143", "(C)"); /* EBCDIC B4 */
	do_test("CP1144", "(C)"); /* EBCDIC B4 */
	do_test("CP1145", "(C)"); /* EBCDIC B4 */
	do_test("CP1146", "(C)"); /* EBCDIC B4 */
	do_test("CP1147", "(C)"); /* EBCDIC B4 */
	do_test("CP1148", "(C)"); /* EBCDIC B4 */
	do_test("CP1149", "(C)"); /* EBCDIC B4 */
	do_test("CP1155", "(C)"); /* EBCDIC B4 */
	do_test("CP1156", "(C)"); /* EBCDIC B4 */
	do_test("CP1157", "(C)"); /* EBCDIC B4 */
	do_test("CP1164", "(C)"); /* EBCDIC B4 */
	do_test("CP273",  "(C)"); /* EBCDIC B4 */
	do_test("CP277",  "(C)"); /* EBCDIC B4 */
	do_test("CP278",  "(C)"); /* EBCDIC B4 */
	do_test("CP280",  "(C)"); /* EBCDIC B4 */
	do_test("CP284",  "(C)"); /* EBCDIC B4 */
	do_test("CP285",  "(C)"); /* EBCDIC B4 */
	do_test("CP297",  "(C)"); /* EBCDIC B4 */
	do_test("CP424",  "(C)"); /* EBCDIC B4 */
	do_test("CP500",  "(C)"); /* EBCDIC B4 */
	do_test("CP871",  "(C)"); /* EBCDIC B4 */
	do_test("CP875",  "(C)"); /* EBCDIC FB */

	tap_done();
}
