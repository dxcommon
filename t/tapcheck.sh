#!/bin/sh
#
# Fake TAP test program for testing the autotest snippets.
#
# Copyright © 2024 Nick Bowler
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

total=$1; shift
pass=0
seq=0

echo "1..$total"
for arg
do
  seq=`expr 1 + $seq`
  if { eval "$arg"; } >/dev/null 2>&1; then
    echo "ok $seq"
    pass=`expr 1 + $pass`
  else
    echo "not ok $seq"
  fi
done

test $pass -eq $total
