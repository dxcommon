#!/bin/sh
#
# Copyright © 2022-2023 Nick Bowler
#
# Fake autotest program for testing the autotest macros and snippets.
#
# License WTFPL2: Do What The Fuck You Want To Public License, version 2.
# This is free software: you are free to do what the fuck you want to.
# There is NO WARRANTY, to the extent permitted by law.

argv0=$0

outfile=
for arg
do
  arg=${lastarg:+"$lastarg="}$arg
  lastarg=

  save_IFS=$IFS; IFS='='
  set x $arg; shift; shift; optarg=$*
  IFS=$save_IFS

  case $arg in
  -o|--output) lastarg=--output ;;
  --output=*) outfile=$optarg ;;
  esac
done

test x"$outfile" = x || exec 1>"$outfile"
echo "echo 'testsuite (mypackage)'"
