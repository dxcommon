/*
 * Copyright © 2015 Nick Bowler
 *
 * Test application to verify 16 and 32-bit signed unpacking functions.
 *
 * License WTFPL2: Do What The Fuck You Want To Public License, version 2.
 * This is free software: you are free to do what the fuck you want to.
 * There is NO WARRANTY, to the extent permitted by law.
 */

#include "pack.h"
#include "tap.h"

static const unsigned char zero[8];
static const unsigned char minus_one[8] = {
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff
};

static const unsigned char min[9] = {
	0x80, 0, 0, 0, 0, 0, 0, 0, 0x80
};

static const unsigned char test_pattern[8] = {
	0xde, 0xad, 0xbe, 0xef, 0xf0, 0x0d, 0xca, 0xfe
};

#define test(func, pattern, expected) do { \
	long result__ = (func)(pattern); \
	if (!tap_result(result__ == (expected), "%s(%s)", #func, #expected)) { \
		tap_diag("  expected: %ld", (long)(expected)); \
		tap_diag("  actual:   %ld", result__); \
	} \
} while (0)

int main(void)
{
	tap_plan(16);

	test(unpack_s16_be, zero,          0);
	test(unpack_s16_be, minus_one,    -1);
	test(unpack_s16_be, test_pattern, -8531);
	test(unpack_s16_be, min,          -32767-1);
	test(unpack_s32_be, zero,          0);
	test(unpack_s32_be, minus_one,    -1);
	test(unpack_s32_be, test_pattern, -559038737);
	test(unpack_s32_be, min,          -2147483647-1);

	test(unpack_s16_le, zero,          0);
	test(unpack_s16_le, minus_one,    -1);
	test(unpack_s16_le, test_pattern, -21026);
	test(unpack_s16_le, min+7,        -32767-1);
	test(unpack_s32_le, zero,          0);
	test(unpack_s32_le, minus_one,    -1);
	test(unpack_s32_le, test_pattern, -272716322);
	test(unpack_s32_le, min+5,        -2147483647-1);

	tap_done();
}
