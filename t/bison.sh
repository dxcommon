#!/bin/sh
#
# Copyright © 2021 Nick Bowler
#
# Fake bison program for testing the bison detection macro.
#
# License WTFPL2: Do What The Fuck You Want To Public License, version 2.
# This is free software: you are free to do what the fuck you want to.
# There is NO WARRANTY, to the extent permitted by law.

: "${FAKE_BISON_VERSION=2.4.0}"

argv0=$0

infile=
outheader=
use_header=false

for arg
do
  case $arg in
  --defines) use_header=true ;;
  --defines=*) use_header=true outheader=${arg%*=} ;;
  *.y) infile=$arg ;;
  esac
done

if test x"$infile" = x""; then
  printf '%s: error: no input file\n' "$argv0" 1>&2
  exit 1
fi

outfile=${infile%.y}.tab.c
if $use_header; then
  test ${outheader:+y} || outheader=${outfile%.c}.h
  exec 5>"$outheader"
fi
exec 3<"$infile" 4>"$outfile"

copy_header=false copy_impl=false
while read line <&3; do
  case $line in
  %require*)
    save_IFS=$IFS IFS=$IFS\".
    set x $FAKE_BISON_VERSION 0 0 0; shift
    fake_major=$1 fake_minor=$2 fake_revision=$3
    set x ${line#*\"} 0 0 0; shift
    req_major=$1 req_minor=$2 req_revision=$3
    IFS=$save_IFS
    set -x
    test $fake_major -ge $req_major || exit 1
    if test $fake_major -eq $req_major; then
      test $fake_minor -ge $req_minor || exit 1
      if test $fake_minor -eq $req_minor; then
        test $fake_revision -ge $req_revision || exit 1
      fi
    fi
    set +x
    ;;
  %code*)
    set x $line; shift
    case $2 in
    requires|provides) copy_header=true copy_impl=true ;;
    top) copy_impl=true ;;
    esac
    ;;
  [}])
    copy_header=false copy_impl=false
    ;;
  %%)
    break
    ;;
  *)
    $copy_header && printf '%s\n' "$line" >&5
    $copy_impl && printf '%s\n' "$line" >&4
    ;;
  esac
done

cat >&4 <<EOF
int yyparse(void)
{
  return 0;
}
EOF

while read line <&3; do
  case $line in
  %%) break;
  esac
done

cat <&3 >&4
