/*
 * Copyright © 2023 Nick Bowler
 *
 * Runtime check of printf behaviour for the test suite.  Some very old printf
 * implementations return 0 on success instead of the number of bytes written.
 *
 * License WTFPL2: Do What The Fuck You Want To Public License, version 2.
 * This is free software: you are free to do what the fuck you want to.
 * There is NO WARRANTY, to the extent permitted by law.
 */
#include <stdio.h>

int main(void)
{
	return printf("hello\n") == 0;
}
