#!/bin/sh
#
# Copyright © 2021 Nick Bowler
#
# Fake md5sum-like program for testing the md5 detection macro.
#
# License WTFPL2: Do What The Fuck You Want To Public License, version 2.
# This is free software: you are free to do what the fuck you want to.
# There is NO WARRANTY, to the extent permitted by law.

: "${MD5_NO_R=}"
: "${MD5_HASH=d41d8cd98f00b204e9800998ecf8427e}"
: "${MD5_DIRECTION=forward}"

stdin=true

print_hash () {
  case $MD5_DIRECTION in
  forward) printf '%s  %s\n' "$MD5_HASH" "$1" ;;
  reverse) printf '%s = %s\n' "$1" "$MD5_HASH" ;;
  *) printf 'invalid direction %s\n' "$MD5_DIRECTION" 1>&2; exit 1 ;;
  esac
}

for arg
do
  case $arg in
  -r)
    test x"$MD5_NO_R" = x"" || exit
    MD5_DIRECTION=forward
    continue ;;
  md5)
    continue ;;
  esac

  stdin=false
  print_hash $arg
done

if $stdin; then
  cat >/dev/null
  print_hash stdin
fi
