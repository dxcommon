#!/bin/sh
#
# Copyright © 2020 Nick Bowler
#
# Fake gob2 program for testing the gob2 detection macro.
#
# License WTFPL2: Do What The Fuck You Want To Public License, version 2.
# This is free software: you are free to do what the fuck you want to.
# There is NO WARRANTY, to the extent permitted by law.

: "${FAKE_GOB2_VERSION=2.0.0}"

argv0=$0

infile=
for arg
do
  case $arg in
  *.gob)
    infile=$arg
  esac
done

if test x"$infile" = x""; then
  printf '%s: error: no input file\n' "$argv0" 1>&2
  exit 1
fi

exec 3<"$infile"
save_IFS=$IFS
while IFS=$IFS. read a b c d e <&3; do
  case $a in
  requires)
    IFS=$IFS.
    set x $FAKE_GOB2_VERSION; shift
    IFS=$save_IFS

    { test "${1:-0}" -gt "${b:-0}" ||
      { test "${1:-0}" -eq "${b:-0}" &&
        { test "${2:-0}" -gt "${c:-0}" ||
          { test "${2:-0}" -eq "${c:-0}" && test "${3:-0}" -ge "${d:-}"
            }; }; }; } ||
    { printf '%s: error: minimum version not met\n' "$argv0"; exit; }
    ;;
  esac
done

cat >conftest.c <<'EOF'
int conftest_get_type(void) { }
EOF
