/* Stub function definitions to satisfy link tests, as required. */
void discid_new(void) {}
void discid_free(void) {}

void curs_set(int x) {}

void g_get_prgname(void) {}

int mouse_set(unsigned long mask) { return 0; }
int request_mouse_pos(void) { return 0; }
unsigned getmouse(void) { return 0; }
unsigned long mousemask(unsigned long a, unsigned long *b) { return 0; }

struct MEVENT;
int getmouse_nc_(struct MEVENT *mev) { return 0; }
int getmouse_bogus_(void *p) { return 0; }

void dx_link_stub(void) { }
