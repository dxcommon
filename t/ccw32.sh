#!/bin/sh
#
# Copyright © 2022 Nick Bowler
#
# Fake C compiler for testing PE subsystem probes.
#
# License WTFPL2: Do What The Fuck You Want To Public License, version 2.
# This is free software: you are free to do what the fuck you want to.
# There is NO WARRANTY, to the extent permitted by law.

mode=link
outfile=
infile=

: "${TEST_SUBSYS=3}"
: "${TEST_GUI_FLAG=-mwindows}"

for arg; do
  shift

  case $arg in
  -o) outfile=$1; ;;
  -c) mode=compile ;;
  *.c) infile=$arg ;;
  $TEST_GUI_FLAG) TEST_SUBSYS=2 ;;
  esac
done

test x"$infile" != x || exit
filebase=`expr "$infile" : '\(.*\).c$'`

case $mode in
link)
  : "${outfile:=a.exe}"
  ;;
compile)
  : "${outfile:=$filebase.obj}"
  ;;
esac

:; {
  printf '%-60s' MZ
  printf '\101\1\0\0%257s' ''
  printf 'PE\0\0%20s' ''
  printf '\13\1%66s' ''
  printf "\\$TEST_SUBSYS\\0"
} >"$outfile"
